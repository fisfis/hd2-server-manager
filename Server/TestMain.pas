unit TestMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HD2ServerService, StdCtrls, Menus, ExtCtrls, ImgList, TrIconSi;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Images: TImageList;
    Menu: TPopupMenu;
    Startservice1: TMenuItem;
    Stopservice1: TMenuItem;
    N1: TMenuItem;
    Showconsoles1: TMenuItem;
    Hideconsoles1: TMenuItem;
    N2: TMenuItem;
    Exit1: TMenuItem;
    Timer1: TTimer;
    Icon: TTrayIconSinea;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Startservice1Click(Sender: TObject);
    procedure Stopservice1Click(Sender: TObject);
    procedure Showconsoles1Click(Sender: TObject);
    procedure Hideconsoles1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    Icn:       TIcon;
  public
    { Public  }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Icn:=TIcon.Create;
   ServerService:=nil;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   Icn.Free;
   if Assigned (ServerService) then ServerService.Done;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   Close;
end;

procedure TForm1.Startservice1Click(Sender: TObject);
begin
   Images.GetIcon(0,Icn);
   Icon.Icon:=Icn;

   Startservice1.Enabled:=false;
   Stopservice1.Enabled:=false;
   Showconsoles1.Enabled:=false;
   Hideconsoles1.Enabled:=false;

   if Assigned (ServerService) then ServerService.Done;
   ServerService:=nil;

   ServerService:=THD2ServerService.Create (Application.ExeName);

   if not ServerService.ServiceInitialized then
   begin
      ServerService.Done;
      ServerService:=nil;

      Startservice1.Enabled:=true;
      Stopservice1.Enabled:=false;
   end else
      begin
         Images.GetIcon(1,Icn);
         Icon.Icon:=Icn;

         Startservice1.Enabled:=false;
         Stopservice1.Enabled:=true;
         Showconsoles1.Enabled:=true;
         Hideconsoles1.Enabled:=true;
      end;
end;

procedure TForm1.Stopservice1Click(Sender: TObject);
begin
   Images.GetIcon(0,Icn);
   Icon.Icon:=Icn;

   Startservice1.Enabled:=false;
   Stopservice1.Enabled:=false;
   Showconsoles1.Enabled:=false;
   Hideconsoles1.Enabled:=false;

   if Assigned (ServerService) then ServerService.Done;
   ServerService:=nil;

   Startservice1.Enabled:=true;
   Stopservice1.Enabled:=false;
end;

procedure TForm1.Showconsoles1Click(Sender: TObject);
begin
   if Assigned (ServerService) then ServerService.ShowWindows;
end;

procedure TForm1.Hideconsoles1Click(Sender: TObject);
begin
   if Assigned (ServerService) then ServerService.HideWindows;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
   Images.GetIcon(0,Icn);
   Icon.Icon:=Icn;

   Startservice1.Enabled:=false;
   Stopservice1.Enabled:=false;
   Showconsoles1.Enabled:=false;
   Hideconsoles1.Enabled:=false;

   if Assigned (ServerService) then ServerService.ShowWindows;

   Close;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
   Timer1.Enabled:=False;
   ShowWindow(Application.Handle,sw_hide);
   Hide;
   StartService1Click(Sender);
end;

end.
