unit HD2ServerService;

interface

uses Windows,StdCtrls,Sysutils,Messages,
     Classes,Scktcomp,Common,DSDataReader,SyncObjs,dialogs;

const
   MAX_CLIENT_INACTIVITY  = 300;
   SERVER_STARTUP_TIMEOUT = 300;
   SERVER_80USAGE_TIMEOUT = 30000;

type
   TTCPServerThread = class (TServerClientThread)
      public
         destructor Destroy; override;
         procedure Initialize;
         procedure Execute; override;
         procedure SendCommand (Command: byte; Data: string);
      private
         Fucked:        boolean;

         Stream:        TWinSocketStream;
         EncryptionKey: array [0..15] of byte;

         Username:      string;
         Permissions:   Byte;                  // 1 - user, 2 - admin
         ServerState:   Byte;                  // 0 - wait for login, 1 - ready for command

         RecvBuffer:    Pointer;
         RecvBufferPtr: ^Byte;
         SendBuffer:    Pointer;
         SendBufferPtr: ^Byte;

         RecvDataSize:  Word;
         ReqDataSize:   Word;

         overwrite:     boolean;
         _PServer:      PServer;
         Command:       string;

         procedure ProcedReceivedRequest;

         procedure SaveConfiguration;
         procedure LoadAviabileMaps;
         procedure StartServer;
         procedure StopServer;
         procedure SendCommandToServer;
         procedure RemoveThread;
   end;

   THD2ServerService = class
      public
         ServiceInitialized:   boolean;

         constructor Create (ExePath: string);
         procedure Done;

         procedure StartTCPServer (Port: Word);
         procedure StopTCPServer;

         procedure AddToLogFile (AddDateTimeInfo: Boolean; Text: string);
         procedure MemoryDumpToLogFile (DumpData: Pointer; DumpDataSize: Cardinal);

         procedure HideWindows;
         procedure ShowWindows;
      private
         ServicePath:           string;

         ProcessingConfigFile:  boolean;

         ServerInfo:            TServerInfo;
         ServerUsers:           array of TUserInfo;
         Servers:               array of TServer;
         Maps:                  array of string;

         TCPServer:             TServerSocket;
         Threads:               array of TServerClientThread;

         Timer:                 Integer;
         HiddenWindows:         boolean;

         procedure CreateLogfile;

         procedure DefaultConfiguration;
         function  LoadConfiguration: boolean;
         function  SaveConfiguration (overwrite: boolean): boolean;

         procedure LoadAviabileMaps;

         procedure ClientGetThread (Sender: TObject; ClientSocket: TServerClientWinSocket;
                                    var SocketThread: TServerClientThread);
         procedure ClientDisconnect (Sender: TObject; Socket: TCustomWinSocket);
         procedure ClientError (Sender: TObject; Socket: TCustomWinSocket;
                                ErrorEvent: TErrorEvent; var ErrorCode: Integer);

         procedure BuildScriptForServer (Server: PServer);
         function  RebuildScriptForServer (Server: PServer): boolean;

         procedure StartServer (Server: PServer);
         procedure StopServer (Server: PServer);

         procedure ServerInfoReceived (ServerName: string; ServerInfo: PHD2DS);
         procedure SendCommandToServer (Server: PServer; Command: string);

         procedure AddThread (Thread: TServerClientThread);
         procedure RemoveThread (Thread: TServerClientThread);
   end;

var
   ServerService:    THD2ServerService;

implementation

procedure ExecuteTimer (Handle: HWND; uMsg: Cardinal; idEvent: Pointer; dwTime: DWord); stdcall; forward; 

function FindUserIPAddress(Server: TServer; Username: string): string;
var
   a:   Integer;
begin
   Result:='0.0.0.0';

   for a:=0 to 31 do
   begin
      if (Server.playerSlots[a] = Username) then
      begin
         Result:=Server.playerIPs[a];
         break;
      end;
   end;
end;

constructor THD2ServerService.Create (ExePath: string);
var
   a:    Integer;
begin
   Randomize;

   ServiceInitialized:=false;
   ServicePath:=ExtractFilePath (ExePath);

   ServerUsers:=nil;
   Servers:=nil;
   Maps:=nil;
   HiddenWindows:=false;

   CreateLogFile;
   AddToLogFile (false,'Hidden and dangerous dedicated server configuration service 2.8');
   AddToLogFile (false,'Copyright (c)1992-2011 Atom Software Studios');
   AddToLogFile (false,'Supported by Mirco from Panicos clan');
   AddToLogFile (false,'');
   AddToLogFile (false,'Copyright (c)2003 Illusion Softworks');
   AddToLogFile (false,'Illusion Softworks, the Illusion Softworks logo and Hidden and Dangerous are trademarks of Illusion Softworks.');
   AddToLogFile (false,'Gathering, the Gathering logo, Take 2 Interactive Software and the Take 2 logo are all trademarks of Take 2 Interactive Software.');
   AddToLogFile (false,'All other trademarks are properties of their respective owners.');
   AddToLogFile (false,'Developed by Illusion Softworks. Published by Gathering.');
   AddToLogFile (false,'');
   AddToLogFile (true,'HD2Server configuration service starting');
   AddToLogFile (false,'');

   ProcessingConfigFile:=false;
   if not LoadConfiguration then
   begin
      DefaultConfiguration;
      if not SaveConfiguration (false) then
      begin
         AddToLogFile (true,'Can not start configuration service');
         AddToLogFile (true,'Repair or delete configuration file');
         AddToLogFile (false,'');
         Exit;
       end;
   end;

   AddToLogFile (false,'');

   AddToLogFile (true,'Starting and configuring HD2DS servers:');
   AddToLogFile (false,'');
   for a:=0 to length (Servers)-1 do
      if Servers [a].running then
      begin
         AddToLogFile (true,'   '+Servers [a].name);
         Servers [a].ProcessStarting:=false;
         Servers [a].ProcessStarted:=false;
         StartServer (@Servers [a]);
      end;

   AddToLogFile (false,'');
   AddToLogFile (true,'Starting timer...');
   Timer:=SetTimer (0,0,1000,@ExecuteTimer);

   AddToLogFile (false,'');
   AddToLogFile (true,'Starting and configuring TCP service...');
   AddToLogFile (false,'');

   TCPServer:=TServerSocket.Create (nil);
   TCPServer.ThreadCacheSize:=0;
   TCPServer.ServerType:=stThreadBlocking;
   TCPServer.OnGetThread:=ClientGetThread;
   TCPServer.OnClientDisconnect:=ClientDisconnect;
   TCPServer.OnClientError:=ClientError;
   StartTCPServer (ServerInfo.ServerPort);

   ServiceInitialized:=true;

   AddToLogFile (true,'HD2Server configuration service started');
   AddToLogFile (false,'');
end;

procedure THD2ServerService.Done;
var
   a:   Integer;
begin
   KillTimer (0,Timer);

   AddToLogFile (true,'Stopping TCP service');
   StopTCPServer;
   TCPServer.Free;

   AddToLogFile (true,'Shutdowning HD2DS servers');

   for a:=0 to length (Servers)-1 do
      if (Servers [a].running and Servers [a].ProcessStarted) or
            (Servers [a].running and Servers [a].ProcessStarting) then
         StopServer (@Servers [a]);

   AddToLogFile (true,'Saving configuration');
   SaveConfiguration (true);

   AddToLogFile (true,'HD2Server configurator service terminated');
end;

// *****************************************************************************
// * Log file
// *****************************************************************************

procedure THD2ServerService.CreateLogfile;
var
   LogFile:   TextFile;
begin
   {$I-}
   AssignFile (LogFile,ServicePath+'hd2serverlog.txt');
   Rewrite (LogFile);
   CloseFile (LogFile);
   {$I+}
end;

procedure THD2ServerService.AddToLogFile (AddDateTimeInfo: boolean; Text: string);
var
   LogFile: TextFile;
begin
   if FileExists (ServicePath+'hd2serverlog.txt') then
   begin
      AssignFile (LogFile,ServicePath+'hd2serverlog.txt');
      Append (LogFile);

      if AddDateTimeInfo then Write (LogFile,DateTimeToStr (Now)+' - ');

      WriteLn (LogFile,Text);

      CloseFile (LogFile);
   end;
end;

procedure THD2ServerService.MemoryDumpToLogFile (DumpData: Pointer; DumpDataSize: Cardinal);
var
   a:     Integer;
   b:     ^Byte;
   tmp:   string;
   tmp1:  string;
begin
   AddToLogFile (false,'');
   AddToLogFile (false,'Dumping memory from address: $'+IntToHex (Integer (DumpData),8));
   AddToLogFile (false,'');

   tmp:='';
   tmp1:='';
   b:=DumpData;
   for a:=1 to DumpDataSize do
   begin
      tmp:=tmp+IntToHex (b^,2)+' ';
      if b^>31 then tmp1:=tmp1+Chr (b^) else tmp1:=tmp1+'.';
      if Length (Tmp1)=16 then
      begin
         AddToLogFile (false,tmp+'   '+tmp1);
         tmp:='';
         tmp1:='';
      end;
      Inc (b);
   end;

   if Tmp<>'' then
   begin
      while length (tmp)<48 do tmp:=tmp+' ';
      AddToLogFile (false,tmp+'   '+tmp1);
   end;

   AddToLogFile (false,'');
end;

// *****************************************************************************
// * Inf file
// *****************************************************************************

procedure THD2ServerService.DefaultConfiguration;
begin
   AddToLogFile (true,'Creating default configuration');

   ServerInfo.ServerIP:='127.0.0.1';
   ServerInfo.ServerPort:=2332;
   ServerInfo.DedicatedServerPath:='';
   ServerInfo.WatchDogEnable:=true;
   ServerInfo.WatchDogInterval:=15;
   ServerInfo.MessagingEnable:=true;
   ServerInfo.MessagingInterval:=180;
   ServerInfo.RebootEnable:=false;
   ServerInfo.RebootInterval:=48;
   ServerInfo.ForcedMessagesEnable:=false;
   ServerInfo.ForcedBanListEnable:=false;
   ServerInfo.ForcedMessages:=nil;
   ServerInfo.ForcedBanList:=nil;

   ServerUsers:=nil;
   Servers:=nil;

   SetLength (ServerUsers,1);
   ServerUsers [0].Username:='Admin';
   ServerUsers [0].Password:='';
   ServerUsers [0].PrivilegeLevel:=2;
end;

function THD2ServerService.LoadConfiguration: boolean;
var
   F:             textfile;

   linenum:       integer;
   
   line:          String;
   section:       String;
   cmd:           String;
   params:        array of AnsiString;

   CurrentServer: ^TServer;
   CurrentConfig: ^TServerConfig;

   a:       integer;
begin
   Result:=false;

   ServerInfo.ServerPort:=2332;
   ServerInfo.DedicatedServerPath:='';
   ServerInfo.WatchDogEnable:=true;
   ServerInfo.WatchDogInterval:=15;
   ServerInfo.MessagingEnable:=true;
   ServerInfo.MessagingInterval:=180;
   ServerInfo.RebootEnable:=false;
   ServerInfo.RebootInterval:=48;
   ServerInfo.ForcedMessagesEnable:=false;
   ServerInfo.ForcedBanListEnable:=false;
   ServerInfo.ForcedMessages:=nil;
   ServerInfo.ForcedBanList:=nil;

   ServerUsers:=nil;
   Servers:=nil;

   if ProcessingConfigFile then Exit;
   ProcessingConfigFile:=true;

   AddToLogFile (true,'Loading configuration from file');

   if FileExists (ServicePath+'config.txt') then
   begin
      AssignFile (F,ServicePath+'config.txt');
      Reset (f);

      CurrentServer:=nil;
      CurrentConfig:=nil;

      linenum:=0;
      section:='';

      try

         while not EOF (F) do
         begin
            ReadLn (F,Line);
            inc (linenum);
            Line:=Trim (Line);

            if (Line<>'') and (Copy (Line,1,2)<>'//') then
            begin

               if (LowerCase (Copy (Line,1,15))='<servermanager>') and
                  (section='') then section:='<servermanager>' else

               if (LowerCase (Copy (Line,1,16))='</servermanager>') and
                  (section='<servermanager>') then section:='' else

               if (LowerCase (Copy (Line,1,7))='<users>') and
                  (section='') then section:='<users>' else

               if (LowerCase (Copy (Line,1,8))='</users>') and
                  (section='<users>') then section:='' else

               if (LowerCase (Copy (Line,1,9))='<servers>') and
                  (section='') then section:='<servers>' else

               if (LowerCase (Copy (Line,1,10))='</servers>') and
                  (section='<servers>') then section:='' else

               if (LowerCase (Copy (Line,1,8))='<server>') and
                  (section='<servers>') then
               begin
                  section:='<server>';
                  SetLength (Servers,length (Servers)+1);
                  CurrentServer:=@Servers [length (Servers)-1];
                  CurrentServer.ProcessStarting:=false;
                  CurrentServer.StartTime:=0;
                  CurrentServer.StartTries:=0;
                  CurrentServer.ProcessStarted:=false;
                  CurrentServer.PermanentError:=false;
                  CurrentServer.ConfigChanged:=true;
                  CurrentServer.ProcessDown:=false;
                  CurrentServer.InfoReceiveTime:=0;
                  CurrentServer.LastProcessedInfo:=0;
               end else

               if (LowerCase (Copy (Line,1,9))='</server>') and
                  (section='<server>') then
               begin
                  CurrentServer:=nil;
                  CurrentConfig:=nil;
                  section:='<servers>'
               end else

               if (LowerCase (Copy (Line,1,8))='<config>') and
                  (section='<server>') then
               begin
                  section:='<config>';
                  SetLength (CurrentServer.Configurations,length (CurrentServer.Configurations)+1);
                  CurrentConfig:=@CurrentServer.Configurations [length (CurrentServer.Configurations)-1];
               end else

               if (LowerCase (Copy (Line,1,9))='</config>') and
                  (section='<config>') then
               begin
                  CurrentConfig:=nil;
                  section:='<server>'
               end else

               if Pos ('=',Line)<>0 then
               begin
                   Cmd:=LowerCase (Trim (Copy (Line,1,Pos ('=',Line)-1)));
                   Delete (Line,1,Pos ('=',Line));
                   Line:=Trim (Line);

                   Params:=nil;
                   if Pos (',',Line)<>0 then
                   begin
                      Line:=Line+',';
                      while Pos (',',Line)<>0 do
                      begin
                         SetLength (Params,length (Params)+1);
                         Params [length (Params)-1]:=Trim (Copy (Line,1,Pos (',',Line)-1));
                         Delete (Line,1,Pos (',',Line));
                      end;
                   end else
                      if Line<>'' then
                      begin
                         SetLength (Params,1);
                         Params [0]:=Line;
                      end;

                   for a:=0 to length (Params)-1 do
                      if (Params [a][1]='"') and (Params [a][length (Params [a])]='"') then
                      begin
                         Delete (Params [a],1,1);
                         Delete (Params [a],length (Params [a]),1);
                      end;

                   if (Cmd<>'') then
                   begin
                      if section='<servermanager>' then
                      begin

                         if (Cmd='serverip') and (length (Params)=1) then
                            ServerInfo.ServerIP:=Params [0] else

                         if (Cmd='serverport') and (length (Params)=1) then
                            ServerInfo.ServerPort:=StrToInt (Params [0]) else

                         if (Cmd='hd2dspath') and (length (Params)=1) then
                            ServerInfo.DedicatedServerPath:=Params [0] else

                         if (Cmd='enablewatchdog') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               ServerInfo.WatchDogEnable:=true
                            else
                               ServerInfo.WatchDogEnable:=false
                         end else

                         if (Cmd='watchdoginterval') and (length (Params)=1) then
                            ServerInfo.WatchDogInterval:=StrToInt (Params [0]) else

                         if (Cmd='enablemessaging') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               ServerInfo.MessagingEnable:=true
                            else
                               ServerInfo.MessagingEnable:=false
                         end else

                         if (Cmd='messaginginterval') and (length (Params)=1) then
                            ServerInfo.MessagingInterval:=StrToInt (Params [0]) else

                         if (Cmd='enablereboot') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               ServerInfo.RebootEnable:=true
                            else
                               ServerInfo.RebootEnable:=false
                         end else

                         if (Cmd='rebootinterval') and (length (Params)=1) then
                            ServerInfo.RebootInterval:=StrToInt (Params [0]) else

                         if (Cmd='enableforcedmessages') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               ServerInfo.ForcedMessagesEnable:=true
                            else
                               ServerInfo.ForcedMessagesEnable:=false
                         end else

                         if (Cmd='enableforcedbanlist') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               ServerInfo.ForcedBanlistEnable:=true
                            else
                               ServerInfo.ForcedBanlistEnable:=false
                         end else

                         if (Cmd='forcedmessages') then
                         begin
                            SetLength (ServerInfo.ForcedMessages,length (Params));
                            for a:=0 to length (Params)-1 do
                               ServerInfo.ForcedMessages [a]:=Params [a];
                         end else

                         if (Cmd='forcedbanlist') then
                         begin
                            SetLength (ServerInfo.ForcedBanList,length (Params));
                            for a:=0 to length (Params)-1 do
                               ServerInfo.ForcedBanList [a]:=Params [a];
                         end else

                         begin
                            AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
                            AddToLogFile (true,'Unknown comand for section <servermanager>: '+Cmd);
                            AddToLogFile (false,'');
                         end;

                      end else

                      if section='<users>' then
                      begin

                         if (Cmd='user') and (length (Params)=3) then
                         begin
                            SetLength (ServerUsers,length (ServerUsers)+1);
                            ServerUsers [length (ServerUsers)-1].Username:=Params [0];
                            ServerUsers [length (ServerUsers)-1].Password:=Params [1];
                            ServerUsers [length (ServerUsers)-1].PrivilegeLevel:=StrToInt (Params [2]);
                         end else

                         begin
                            AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
                            AddToLogFile (true,'Unknown comand for section <users>: '+Cmd);
                            AddToLogFile (false,'');
                         end;

                      end else

                      if section='<servers>' then
                      begin
                      end else

                      if (section='<server>') and (CurrentServer<>nil) then
                      begin

                         if (Cmd='name') and (length (Params)=1) then
                            CurrentServer.name:=Params [0] else

                         if (Cmd='running') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentServer.running:=true
                            else
                               CurrentServer.running:=false;
                         end else

                         if (Cmd='watchdog') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentServer.watchdogenable:=true
                            else
                               CurrentServer.watchdogenable:=false
                         end else

                         if (Cmd='messages') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentServer.messagesenable:=true
                            else
                               CurrentServer.messagesenable:=false
                         end else

                         if (Cmd='currentconfig') and (length (Params)=1) then
                            CurrentServer.currentconfig:=Params [0] else

                         if (Cmd='port') and (length (Params)=1) then
                            CurrentServer.port:=StrToInt (Params [0]) else

                         if (Cmd='users') and (length (Params)>0) then
                         begin
                            SetLength (CurrentServer.grantedusers,length (Params));
                            for a:=0 to length (Params)-1 do
                               CurrentServer.grantedusers [a]:=Params [a];
                         end else

                         begin
                            AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
                            AddToLogFile (true,'Unknown comand for section <server>: '+Cmd);
                            AddToLogFile (false,'');
                         end;

                      end else

                      if (section='<config>') and (CurrentConfig<>nil) then
                      begin
                         if (Cmd='name') and (length (Params)=1) then
                            CurrentConfig.name:=Params [0] else

                         if (Cmd='domain') and (length (Params)=1) then
                            CurrentConfig.domain:=Params [0] else

                         if (Cmd='style') and (length (Params)=1) then
                            CurrentConfig.style:=Params [0] else

                         if (Cmd='sessionname') and (length (Params)=1) then
                            CurrentConfig.sessionname:=Params [0] else

                         if (Cmd='maxclients') and (length (Params)=1) then
                            CurrentConfig.maxclients:=StrToInt (Params [0]) else

                         if (Cmd='pointlimit') and (length (Params)=1) then
                            CurrentConfig.pointlimit:=StrToInt (Params [0]) else

                         if (Cmd='roundlimit') and (length (Params)=1) then
                            CurrentConfig.roundlimit:=StrToInt (Params [0]) else

                         if (Cmd='roundcount') and (length (Params)=1) then
                            CurrentConfig.roundcount:=StrToInt (Params [0]) else

                         if (Cmd='respawntime') and (length (Params)=1) then
                            CurrentConfig.respawntime:=StrToInt (Params [0]) else

                         if (Cmd='spawnprotection') and (length (Params)=1) then
                            CurrentConfig.spawnprotection:=StrToInt (Params [0]) else

                         if (Cmd='warmup') and (length (Params)=1) then
                            CurrentConfig.warmup:=StrToInt (Params [0]) else

                         if (Cmd='inversedamage') and (length (Params)=1) then
                            CurrentConfig.inversedamage:=StrToInt (Params [0]) else

                         if (Cmd='friendlyfire') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.friendlyfire:=true
                            else
                               CurrentConfig.friendlyfire:=false;
                         end else

                         if (Cmd='autoteambalance') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.autoteambalance:=true
                            else
                               CurrentConfig.autoteambalance:=false
                         end else

                         if (Cmd='3rdpersonview') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig._3rdpersonview:=true
                            else
                               CurrentConfig._3rdpersonview:=false
                         end else

                         if (Cmd='allowcrosshair') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.allowcrosshair:=true
                            else
                               CurrentConfig.allowcrosshair:=false
                         end else

                         if (Cmd='fallingdmg') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.fallingdmg:=true
                            else
                               CurrentConfig.fallingdmg:=false
                         end else

                         if (Cmd='allowrespawn') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.allowrespawn:=true
                            else
                               CurrentConfig.allowrespawn:=false
                         end else

                         if (Cmd='allowvehicles') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.allowvehicles:=true
                            else
                               CurrentConfig.allowvehicles:=false
                         end else

                         if (Cmd='dificulty') and (length (Params)=1) then
                            CurrentConfig.dificulty:=Params [0] else

                         if (Cmd='respawnnumber') and (length (Params)=1) then
                            CurrentConfig.respawnnumber:=StrToInt (Params [0]) else

                         if (Cmd='teamrespawn') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.teamrespawn:=true
                            else
                               CurrentConfig.teamrespawn:=false
                         end else

                         if (Cmd='password') and (length (Params)=1) then
                            CurrentConfig.password:=Params [0] else

                         if (Cmd='adminpass') and (length (Params)=1) then
                            CurrentConfig.adminpass:=Params [0] else

                         if (Cmd='maxping') and (length (Params)=1) then
                            CurrentConfig.maxping:=StrToInt (Params [0]) else

                         if (Cmd='maxfreq') and (length (Params)=1) then
                            CurrentConfig.maxfreq:=StrToInt (Params [0]) else

                         if (Cmd='maxinactivity') and (length (Params)=1) then
                            CurrentConfig.maxinactivity:=StrToInt (Params [0]) else

                         if (Cmd='voicechat') and (length (Params)=1) then
                            CurrentConfig.voicechat:=StrToInt (Params [0]) else

                         if (Cmd='enableautokick') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='true' then
                               CurrentConfig.enableautokick:=true
                            else
                               CurrentConfig.enableautokick:=false
                         end else

                         if (Cmd='clantag') and (length (Params)=1) then
                            CurrentConfig.clantag:=Params [0] else

                         if (Cmd='clanside') and (length (Params)=1) then
                         begin
                            if LowerCase (Params [0])='allied' then
                               CurrentConfig.clanside:=1
                            else
                               CurrentConfig.clanside:=0;
                         end else

                         if (Cmd='clanreserve') and (length (Params)=1) then
                            CurrentConfig.clanreserve:=StrToInt (Params [0]) else

                         if (Cmd='maps') then
                         begin
                            SetLength (CurrentConfig.maps,length (Params));
                            for a:=0 to length (Params)-1 do
                               CurrentConfig.maps [a]:=Params [a]
                         end else

                         if (Cmd='messages') then
                         begin
                            SetLength (CurrentConfig.messages,length (Params));
                            for a:=0 to length (Params)-1 do
                               CurrentConfig.messages [a]:=Params [a]
                         end else

                         if (Cmd='banlist') then
                         begin
                            SetLength (CurrentConfig.banlist,length (Params));
                            for a:=0 to length (Params)-1 do
                               CurrentConfig.banlist [a]:=Params [a]
                         end else

                         begin
                            AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
                            AddToLogFile (true,'Unknown comand for section <config>: '+Cmd);
                            AddToLogFile (false,'');
                         end;

                      end;
                   end;

               end else
                  begin
                     AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
                     AddToLogFile (true,'Unknown section name'+Cmd);
                     AddToLogFile (false,'');
                  end;

            end;
         end;

      except
         CloseFile (F);
         AddToLogFile (true,'Config file parse error at line '+IntToStr (linenum));
         AddToLogFile (true,'Error loading configuration file');
         AddToLogFile (false,'');
         Result:=false;
         ProcessingConfigFile:=false;
         Exit;
      end;

      CloseFile (F);

      AddToLogFile (true,'Configuration succesfully loaded');
      AddToLogFile (false,'');
      Result:=true;
   end;

   if length (ServerUsers)=0 then
   begin
      SetLength (ServerUsers,1);
      ServerUsers [0].Username:='Admin';
      ServerUsers [0].Password:='';
      ServerUsers [0].PrivilegeLevel:=2;
   end;

   ProcessingConfigFile:=false;
end;

function THD2ServerService.SaveConfiguration (overwrite: boolean): boolean;
var
   F:     textfile;
   a,b,c: integer;
   tmp:   string;
begin
   Result:=false;
   if ProcessingConfigFile then Exit;
   ProcessingConfigFile:=true;

   AddToLogFile (true,'Saving configuration to file');

   if not FileExists (ServicePath+'config.txt') or overwrite then
   begin
      AssignFile (F,ServicePath+'config.txt');
      Rewrite (F);

      WriteLn (F,'// HD2Server configurator service configuration file');
      WriteLn (F,'// Please do not modify it manually (add or remove lines)');
      WriteLn (F,'// Value changes allowed on your own risk');
      WriteLn (F,'// Value changes may cause service disfunction');
      WriteLn (F,'// Use remote configurator to config service parameters');
      WriteLn (F,'');
      WriteLn (F,'// Part of service configuration');
      WriteLn (F,'');

      WriteLn (F,'<ServerManager>');
      WriteLn (F,'');
      WriteLn (F,'   ServerIP             = '+ServerInfo.ServerIP);
      WriteLn (F,'   ServerPort           = '+IntToStr (ServerInfo.ServerPort));
      WriteLn (F,'   HD2DSPath            = "'+ServerInfo.DedicatedServerPath+'"');
      WriteLn (F,'');
      WriteLn (F,'   EnableWatchDog       = '+BoolToStr (ServerInfo.WatchDogEnable));
      WriteLn (F,'   WatchdogInterval     = '+IntToStr (ServerInfo.WatchDogInterval));
      WriteLn (F,'');
      WriteLn (F,'   EnableMessaging      = '+BoolToStr (ServerInfo.MessagingEnable));
      WriteLn (F,'   MessagingInterval    = '+IntToStr (ServerInfo.MessagingInterval));
      WriteLn (F,'');
      WriteLn (F,'   EnableReboot         = '+BoolToStr (ServerInfo.RebootEnable));
      WriteLn (F,'   RebootInterval       = '+IntToStr (ServerInfo.RebootInterval));
      WriteLn (F,'');
      WriteLn (F,'   EnableForcedMessages = '+BoolToStr (ServerInfo.ForcedMessagesEnable));
      tmp:='';
      for a:=0 to length (ServerInfo.ForcedMessages)-1 do
         tmp:=tmp+'"'+ServerInfo.ForcedMessages [a]+'",';
      Delete (tmp,length (tmp),1);
      WriteLn (F,'   ForcedMessages       = '+tmp);
      WriteLn (F,'');
      WriteLn (F,'   EnableForcedBanList  = '+BoolToStr (ServerInfo.ForcedBanListEnable));
      tmp:='';
      for a:=0 to length (ServerInfo.ForcedBanList)-1 do
         tmp:=tmp+'"'+ServerInfo.ForcedBanList [a]+'",';
      Delete (tmp,length (tmp),1);
      WriteLn (F,'   ForcedBanList        = '+tmp);
      WriteLn (F,'');
      WriteLn (F,'</ServerManager>');
      WriteLn (F,'');

      WriteLn (F,'// Part of users configuration');
      WriteLn (F,'');
      WriteLn (F,'<Users>');
      WriteLn (F,'');

      for a:=0 to length (ServerUsers)-1 do
      begin
         WriteLn (F,'   user = "'+ServerUsers [a].Username+'","'+ServerUsers [a].Password+'",'+IntToStr (ServerUsers [a].PrivilegeLevel));
      end;

      WriteLn (F,'');
      WriteLn (F,'</Users>');
      WriteLn (F,'');

      WriteLn (F,'// Dedicated servers configurations');
      WriteLn (F,'');
      WriteLn (F,'<Servers>');
      WriteLn (F,'');

      for a:=0 to length (Servers)-1 do
      begin
         WriteLn (F,'   <Server>');
         WriteLn (F,'');
         WriteLn (F,'      name          = "'+Servers [a].name+'"');
         WriteLn (F,'      running       = '+BoolToStr (Servers [a].running));
         WriteLn (F,'      watchdog      = '+BoolToStr (Servers [a].watchdogenable));
         WriteLn (F,'      messages      = '+BoolToStr (Servers [a].messagesenable));
         WriteLn (F,'');
         tmp:='';
         for b:=0 to length (Servers [a].GrantedUsers)-1 do
            tmp:=tmp+'"'+Servers [a].GrantedUsers [b]+'",';
         Delete (tmp,length (tmp),1);
         WriteLn (F,'      users         = '+tmp);
         WriteLn (F,'      port          = '+IntToStr (Servers [a].port));
         WriteLn (F,'');
         WriteLn (F,'      currentconfig = "'+Servers [a].CurrentConfig+'"');
         WriteLn (F,'');

         for b:=0 to length (Servers [a].Configurations)-1 do
         begin
            WriteLn (F,'      <config>');
            WriteLn (F,'');
            WriteLn (F,'         name            = "'+Servers [a].Configurations [b].name+'"');
            WriteLn (F,'');
            WriteLn (F,'         domain          = '+Servers [a].Configurations [b].domain);
            WriteLn (F,'         style           = '+Servers [a].Configurations [b].style);
            WriteLn (F,'         sessionname     = "'+Servers [a].Configurations [b].sessionname+'"');
            WriteLn (F,'         maxclients      = '+IntToStr (Servers [a].Configurations [b].maxclients));
            WriteLn (F,'         pointlimit      = '+IntToStr (Servers [a].Configurations [b].pointlimit));
            WriteLn (F,'         roundlimit      = '+IntToStr (Servers [a].Configurations [b].roundlimit));
            WriteLn (F,'         roundcount      = '+IntToStr (Servers [a].Configurations [b].roundcount));
            WriteLn (F,'         respawntime     = '+IntToStr (Servers [a].Configurations [b].respawntime));
            WriteLn (F,'         spawnprotection = '+IntToStr (Servers [a].Configurations [b].spawnprotection));
            WriteLn (F,'         warmup          = '+IntToStr (Servers [a].Configurations [b].warmup));
            WriteLn (F,'         inversedamage   = '+IntToStr (Servers [a].Configurations [b].inversedamage));
            WriteLn (F,'         friendlyfire    = '+BoolToStr (Servers [a].Configurations [b].friendlyfire));
            WriteLn (F,'         autoteambalance = '+BoolToStr (Servers [a].Configurations [b].autoteambalance));
            WriteLn (F,'         3rdpersonview   = '+BoolToStr (Servers [a].Configurations [b]._3rdpersonview));
            WriteLn (F,'         allowcrosshair  = '+BoolToStr (Servers [a].Configurations [b].allowcrosshair));
            WriteLn (F,'         fallingdmg      = '+BoolToStr (Servers [a].Configurations [b].fallingdmg));
            WriteLn (F,'         allowrespawn    = '+BoolToStr (Servers [a].Configurations [b].allowrespawn));
            WriteLn (F,'         allowvehicles   = '+BoolToStr (Servers [a].Configurations [b].allowvehicles));
            WriteLn (F,'         dificulty       = '+Servers [a].Configurations [b].dificulty);
            WriteLn (F,'         respawnnumber   = '+IntToStr (Servers [a].Configurations [b].respawnnumber));
            WriteLn (F,'         teamrespawn     = '+BoolToStr (Servers [a].Configurations [b].teamrespawn));
            WriteLn (F,'');
            WriteLn (F,'         password        = "'+Servers [a].Configurations [b].password+'"');
            WriteLn (F,'         adminpass       = "'+Servers [a].Configurations [b].adminpass+'"');
            WriteLn (F,'');
            WriteLn (F,'         maxping         = '+IntToStr (Servers [a].Configurations [b].maxping));
            WriteLn (F,'         maxfreq         = '+IntToStr (Servers [a].Configurations [b].maxfreq));
            WriteLn (F,'         maxinactivity   = '+IntToStr (Servers [a].Configurations [b].maxinactivity));
            WriteLn (F,'         voicechat       = '+IntToStr (Servers [a].Configurations [b].voicechat));
            WriteLn (F,'');
            tmp:='';
            for c:=0 to length (Servers [a].Configurations [b].maps)-1 do
               tmp:=tmp+'"'+Servers [a].Configurations [b].maps [c]+'",';
            Delete (tmp,length (tmp),1);
            WriteLn (F,'         maps            = '+tmp);
            WriteLn (F,'');
            tmp:='';
            for c:=0 to length (Servers [a].Configurations [b].messages)-1 do
               tmp:=tmp+'"'+Servers [a].Configurations [b].messages [c]+'",';
            Delete (tmp,length (tmp),1);
            WriteLn (F,'         messages        = '+tmp);
            tmp:='';
            for c:=0 to length (Servers [a].Configurations [b].banlist)-1 do
               tmp:=tmp+'"'+Servers [a].Configurations [b].banlist [c]+'",';
            Delete (tmp,length (tmp),1);
            WriteLn (F,'         banlist         = '+tmp);
            WriteLn (F,'');
            WriteLn (F,'         enableautokick  = '+BoolToStr (Servers [a].Configurations [b].enableautokick));
            WriteLn (F,'         clantag         = "'+Servers [a].Configurations [b].clantag+'"');

            if Servers [a].Configurations [b].clanside=0 then
               WriteLn (F,'         clanside        = axis')
            else
               WriteLn (F,'         clanside        = allied');

            WriteLn (F,'         clanreserve    = '+IntToStr (Servers [a].Configurations [b].ClanReserve));

            WriteLn (F,'');
            WriteLn (F,'      </config>');
            WriteLn (F,'');
         end;

         WriteLn (F,'   </Server>');
         WriteLn (F,'');
      end;

      WriteLn (F,'</Servers>');
      WriteLn (F,'');
      CloseFile (F);

      AddToLogFile (true,'Configuration succesfully saved');
      AddToLogFile (false,'');
      Result:=true;
   end else
      begin
         AddToLogFile (true,'Error while saving configuration file');
         AddToLogFile (true,'Configuration file allready exists');
         AddToLogFile (false,'');
      end;

   ProcessingConfigFile:=false;
end;

procedure THD2ServerService.LoadAviabileMaps;
var
   Path:        string;
   F:           textfile;
   Line,Ln:     string;
begin
   Maps:=nil;

   Path:=ExtractFilePath (ServerInfo.DedicatedServerPath);
   ServerService.AddToLogFile (true,'Loading mpmaplist.txt: '+Path+'mpmaplist.txt');

   if FileExists (Path+'mpmaplist.txt') then
   begin
      AssignFile (F,Path+'mpmaplist.txt');
      Reset (F);

      while not EOF (F) do
      begin
         ReadLn (F,Line);
         Ln:=LowerCase (Line);

         if Pos ('<gamestyle',Ln)<>0 then
         begin
            if Pos ('type="',Ln)<>0 then
            begin
               Delete (Ln,1,Pos ('type="',Ln)+5);
               Delete (Ln,Pos ('"',Ln),length (Ln));
               SetLength (Maps,length (Maps)+1);
               Maps [length (Maps)-1]:='<'+Ln+'>';
            end;
         end;

         if Pos ('<map',Ln)<>0 then
            if Pos ('name="',Ln)<>0 then
            begin
               Delete (Line,1,Pos ('name="',Ln)+5);
               Delete (Line,Pos ('"',Line),length (Line));
               SetLength (Maps,length (Maps)+1);
               Maps [length (Maps)-1]:=Line;
            end;
      end;

      CloseFile (F);

      ServerService.AddToLogFile (true,'Total aviabile map configurations: '+IntToStr(length(Maps)));

   end else
      ServerService.AddToLogFile (true,'Map list not found!');
end;

// *****************************************************************************
// *****************************************************************************
// * Part of TCP server service with data processing in separate thread
// *****************************************************************************
// *****************************************************************************

procedure TTCPServerThread.SaveConfiguration;
begin
   ServerService.SaveConfiguration (overwrite);
end;

procedure TTCPServerThread.LoadAviabileMaps;
begin
   ServerService.LoadAviabileMaps;
end;

procedure TTCPServerThread.StartServer;
begin
   ServerService.StartServer (_PServer);
end;

procedure TTCPServerThread.StopServer;
begin
   ServerService.StopServer (_PServer);
end;

procedure TTCPServerThread.SendCommandToServer;
begin
   ServerService.SendCommandToServer (_PServer,Command);
end;

procedure TTCPServerThread.RemoveThread;
begin
   ServerService.RemoveThread (Self);
end;

destructor TTCPServerThread.Destroy;
begin
   if SendBuffer<>nil then FreeMem (SendBuffer);
   if RecvBuffer<>nil then FreeMem (RecvBuffer);
   if Stream<>nil then Stream.Free;

   Synchronize (RemoveThread);
   inherited Destroy;
end;

procedure TTCPServerThread.Initialize;
begin
   Fucked:=false;

   RecvBuffer:=nil;
   SendBuffer:=nil;
   Stream:=nil;
   RecvDataSize:=0;
   ReqDataSize:=0;

   Username:='';
   Permissions:=0;
   ServerState:=0;
end;

procedure TTCPServerThread.Execute;
var
   CurrentReceivedSize: LongInt;
   Tmp:                 ^Word;

   a:                   Integer;
   Buffer:              Pointer;

   stmp1, stmp2:        string;
begin
   ServerService.AddToLogFile (true,'New user connection established from '+ClientSocket.RemoteAddress);

   Stream:=TWinSocketStream.Create (ClientSocket,0);
   GetMem (RecvBuffer,65535);
   GetMem (SendBuffer,65535);

   Buffer:=RecvBuffer;

   try
      for a:=0 to 15 do
         EncryptionKey [a]:=Random (255);

      Tmp:=SendBuffer;
      Tmp^:=18;
      Inc (Tmp);
      CopyMemory (Tmp,@EncryptionKey,16);
      Stream.Write (SendBuffer^,18);

      while not Terminated and ClientSocket.Connected do
      try
         if Stream.WaitForData (MAX_CLIENT_INACTIVITY*1000) then
         begin
            CurrentReceivedSize:=0;

            if not Terminated and ClientSocket.Connected then
            begin
               CurrentReceivedSize:=Stream.Read (Buffer^,65535-RecvDataSize);
               Buffer := Pointer(Integer(Buffer)+CurrentReceivedSize);
            end;

            if ClientSocket.Connected then stmp1 := 'connected' else stmp1 := 'disconnected';
            if Terminated then stmp2 := 'terminated' else stmp1 := 'running';

            if not Terminated and (ClientSocket.Connected) and (CurrentReceivedSize=0) then ClientSocket.Close
            else
               begin
                  Tmp:=RecvBuffer;
                  if RecvDataSize=0 then ReqDataSize:=Tmp^;

                  Inc (RecvDataSize,CurrentReceivedSize);

                  if RecvDataSize=ReqDataSize then
                  begin
                     SendBufferPtr:=SendBuffer;
                     Inc (SendBufferPtr,2);

                     RecvBufferPtr:=RecvBuffer;
                     ProcedReceivedRequest;

                     Buffer:=RecvBuffer;
                     RecvDataSize:=0;
                  end;

               end;
         end else
            begin
               ServerService.AddToLogFile (true, 'Error: Connection timeout');
               ClientSocket.Close;
            end;
      except
         Fucked:=true;
         ClientSocket.Close;
         Terminate;
      end;

   finally
      FreeMem (SendBuffer);
      SendBuffer:=nil;
      FreeMem (RecvBuffer);
      RecvBuffer:=nil;
      Stream.Free;
      Stream:=nil;
   end;
end;

procedure TTCPServerThread.SendCommand (Command: byte; Data: string);
var
   a:            Integer;
   SendDataSize: Integer;
   SB:           Pointer;
   SBP:          ^Byte;
begin
   SB:=nil;

   if not Fucked and ClientSocket.Connected and (SendBuffer<>nil) then
   try
      GetMem (SB,256);

      SBP:=SB;
      Inc (SBP,2);

      case Command of
         svcmd_PutServerStatus:
         begin
            for a:=0 to length (ServerService.Servers)-1 do
               if ServerService.Servers [a].name=Data then
               begin
                  SBP^:=0;
                  Inc (SBP);
                  SBP^:=svcmd_PutServerStatus;
                  Inc (SBP);

                  StrPCopy (PChar (SBP),ServerService.Servers [a].name);
                  Inc (SBP,StrLen (PChar (SBP))+1);

                  if ServerService.Servers [a].ProcessDown and not
                        ServerService.Servers [a].ProcessStarting then SBP^:=4 else
                     if ServerService.Servers [a].PermanentError and not
                        ServerService.Servers [a].ProcessStarting then SBP^:=3 else
                        if not (ServerService.Servers [a].ProcessStarting or
                                ServerService.Servers [a].ProcessStarted) then SBP^:=0 else
                           if ServerService.Servers [a].ProcessStarting then SBP^:=1 else
                              if ServerService.Servers [a].ProcessStarted then SBP^:=2;


                  Inc (SBP);

                  Break;
               end;
         end;
      end;

      SendDataSize:=Integer (SBP)-Integer (SB);
      SBP:=SB;
      SBP^:=Lo (SendDataSize); Inc (SBP);
      SBP^:=Hi (SendDataSize);

      if SendDataSize>2 then
      try
         Sleep (250);
         if not Fucked and ClientSocket.Connected then Stream.Write (SB^,SendDataSize);
         Sleep (250);
      except
         Fucked:=true;
         ClientSocket.Close;
         Terminate;
      end;

   finally;
      if SB<>nil then FreeMem (SB);
   end;
end;

procedure TTCPServerThread.ProcedReceivedRequest;
var
   a,b,c:        Integer;

   Server:       TServer;
   tmp:          string;

   SendDataSize: Integer;

   userIp:       string;
begin
   Inc (RecvBufferPtr,2);

   // DecryptBuffer (RecvData,RecvDataSize);

   // Login to server
   if RecvBufferPtr^=svcmd_Login then
   begin
      Inc (RecvBufferPtr);

      SendBufferPtr^:=svcmd_Login;
      Inc (SendBufferPtr);

      SendBufferPtr^:=0;

      for a:=0 to length (ServerService.ServerUsers)-1 do
         if LowerCase (ServerService.ServerUsers [a].Username)=LowerCase (string (PChar (RecvBufferPtr))) then
         begin
            Username:=LowerCase (ServerService.ServerUsers [a].Username);

            Inc (RecvBufferPtr,strlen (PChar (RecvBufferPtr))+1);
            if ServerService.ServerUsers [a].Password=string (PChar (RecvBufferPtr)) then
            begin
               SendBufferPtr^:=ServerService.ServerUsers [a].PrivilegeLevel;
               Permissions:=ServerService.ServerUsers [a].PrivilegeLevel;

               ServerService.AddToLogFile (true,'Connected user name: '+Username);
               Synchronize (LoadAviabileMaps);
               ServerService.AddToLogFile (false,'');
            end else
               ServerService.AddToLogFile (true,'Login failed: Userame="'+Username+'", Password="'+string (PChar (RecvBufferPtr))+'"'+#13#10);

            break;
         end;

      Inc (SendBufferPtr);

   end else

      case RecvBufferPtr^ of
      // *****************************************************************
      svcmd_GetConfig:
            if Permissions>1 then
            begin
               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_GetConfig;
               Inc (SendBufferPtr);

               CopyMemory (SendBufferPtr,@ServerService.ServerInfo,19);
               Inc (SendBufferPtr,19);
               StrPCopy (PChar (SendBufferPtr),ServerService.ServerInfo.DedicatedServerPath);
               Inc (SendBufferPtr,length (ServerService.ServerInfo.DedicatedServerPath)+1);
               StrPCopy (PChar (SendBufferPtr),ServerService.ServerInfo.ServerIP);
               Inc (SendBufferPtr,length (ServerService.ServerInfo.ServerIP)+1);
               SendBufferPtr^:=length (ServerService.ServerInfo.ForcedMessages);
               Inc (SendBufferPtr);
               for a:=0 to length (ServerService.ServerInfo.ForcedMessages)-1 do
               begin
                  StrPCopy (PChar (SendBufferPtr),ServerService.ServerInfo.ForcedMessages [a]);
                  Inc (SendBufferPtr,length (ServerService.ServerInfo.ForcedMessages [a])+1);
               end;
               SendBufferPtr^:=length (ServerService.ServerInfo.ForcedBanList);
               Inc (SendBufferPtr);
               for a:=0 to length (ServerService.ServerInfo.ForcedBanList)-1 do
               begin
                  StrPCopy (PChar (SendBufferPtr),ServerService.ServerInfo.ForcedBanList [a]);
                  Inc (SendBufferPtr,length (ServerService.ServerInfo.ForcedBanList [a])+1);
               end;
            end else
               begin
                  SendBufferPtr^:=1;;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_PutConfig:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_PutConfig;
               Inc (SendBufferPtr);

               CopyMemory (@ServerService.ServerInfo.WatchDogEnable,RecvBufferPtr,17);
               Inc (RecvBufferPtr,17);

               SetLength (ServerService.ServerInfo.ForcedMessages,RecvBufferPtr^);
               Inc (RecvBufferPtr);
               for a:=0 to length (ServerService.ServerInfo.ForcedMessages)-1 do
               begin
                  ServerService.ServerInfo.ForcedMessages [a]:=StrPas (PChar (RecvBufferPtr));
                  Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
               end;

               Setlength (ServerService.ServerInfo.ForcedBanList,RecvBufferPtr^);
               Inc (RecvBufferPtr);
               for a:=0 to length (ServerService.ServerInfo.ForcedBanList)-1 do
               begin
                  ServerService.ServerInfo.ForcedBanList [a]:=StrPas (PChar (RecvBufferPtr));
                  Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
               end;

               Overwrite:=true;
               Synchronize (SaveConfiguration);
            end;

      // *****************************************************************
      svcmd_ListUsers:
            if Permissions>1 then
            begin
               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_ListUsers;
               Inc (SendBufferPtr);

               SendBufferPtr^:=length (ServerService.ServerUsers);
               Inc (SendBufferPtr);
               for a:=0 to length (ServerService.ServerUsers)-1 do
               begin
                  SendBufferPtr^:=ServerService.ServerUsers [a].PrivilegeLevel;
                  Inc (SendBufferPtr);
                  StrPCopy (PChar (SendBufferPtr),ServerService.ServerUsers [a].Username);
                  Inc (SendBufferPtr,length (ServerService.ServerUsers [a].Username)+1);
                  StrPCopy (PChar (SendBufferPtr),ServerService.ServerUsers [a].Password);
                  Inc (SendBufferPtr,length (ServerService.ServerUsers [a].Password)+1);
               end;
            end else
               begin
                  SendBufferPtr^:=1;;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_ListServers:
            if Permissions>0 then
            begin
               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_ListServers;
               Inc (SendBufferPtr);

               SendBufferPtr^:=length (ServerService.Servers);
               Inc (SendBufferPtr);
               for a:=0 to length (ServerService.Servers)-1 do
               begin
                  CopyMemory (SendBufferPtr,@ServerService.Servers [a],3);
                  Inc (SendBufferPtr,3);
                  StrPCopy (PChar (SendBufferPtr),ServerService.Servers [a].name);
                  Inc (SendBufferPtr,length (ServerService.Servers [a].name)+1);
                  SendBufferPtr^:=length (ServerService.Servers [a].GrantedUsers);
                  Inc (SendBufferPtr);
                  for b:=0 to length (ServerService.Servers [a].GrantedUsers)-1 do
                  begin
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [a].GrantedUsers [b]);
                     Inc (SendBufferPtr,length (ServerService.Servers [a].GrantedUsers [b])+1);
                  end;
               end;
            end else
               begin
                  SendBufferPtr^:=1;;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_GetServerConfig:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_GetServerConfig;
               Inc (SendBufferPtr);

               SendBufferPtr^:=0;
               c:=-1;
               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=StrPas (PChar (RecvBufferPtr)) then
                  begin
                     for b:=0 to length (ServerService.Servers [a].GrantedUsers)-1 do
                        if LowerCase (ServerService.Servers [a].GrantedUsers [b])=Username then
                        begin
                           SendBufferPtr^:=1;
                           c:=a;
                           break
                        end;
                     break;
                  end;

               if (SendBufferPtr^<>0) and (c>-1) then
               begin
                  Inc (SendBufferPtr);
                  CopyMemory (SendBufferPtr,@ServerService.Servers [c].watchdogenable,2);
                  Inc (SendBufferPtr,2);
                  StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].currentconfig);
                  Inc (SendBufferPtr,length (ServerService.Servers [c].currentconfig)+1);
                  SendBufferPtr^:=length (ServerService.Servers [c].configurations);
                  Inc (SendBufferPtr);
                  for a:=0 to length (ServerService.Servers [c].configurations)-1 do
                  begin
                     CopyMemory (SendBufferPtr,@ServerService.Servers [c].configurations [a],31);
                     Inc (SendBufferPtr,31);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].name);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].name)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].domain);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].domain)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].style);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].style)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].sessionname);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].sessionname)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].dificulty);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].dificulty)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].password);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].password)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].adminpass);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].adminpass)+1);
                     StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].clantag);
                     Inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].clantag)+1);

                     SendBufferPtr^:=length (ServerService.Servers [c].configurations [a].maps);
                     Inc (SendBufferPtr);
                     for b:=0 to length (ServerService.Servers [c].configurations [a].maps)-1 do
                     begin
                        StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].maps [b]);
                        inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].maps [b])+1);
                     end;

                     SendBufferPtr^:=length (ServerService.Servers [c].configurations [a].messages);
                     Inc (SendBufferPtr);
                     for b:=0 to length (ServerService.Servers [c].configurations [a].messages)-1 do
                     begin
                        StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].messages [b]);
                        inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].messages [b])+1);
                     end;

                     SendBufferPtr^:=length (ServerService.Servers [c].configurations [a].banlist);
                     Inc (SendBufferPtr);

                     for b:=0 to length (ServerService.Servers [c].configurations [a].banlist)-1 do
                     begin
                        StrPCopy (PChar (SendBufferPtr),ServerService.Servers [c].configurations [a].banlist [b]);
                        inc (SendBufferPtr,length (ServerService.Servers [c].configurations [a].banlist [b])+1);
                     end;
                  end;

               end else Inc (SendBufferPtr);

               SendBufferPtr^:=length (ServerService.Maps);
               Inc (SendBufferPtr);

               if length (ServerService.Maps)>0 then
                  for a:=0 to length (ServerService.Maps)-1 do
                  begin
                     StrPCopy (PChar (SendBufferPtr),ServerService.Maps [a]);
                     Inc (SendBufferPtr,length (ServerService.Maps [a])+1);
                  end;

//               ServerService.MemoryDumpToLogFile (SendData,SendDataSize);
               end else
                  begin
                     SendBufferPtr^:=1;;
                     Inc (SendBufferPtr);
                  end;


      // 0 - OK, 1 - Not found, 2 - Access denied, 3 - Server needs restart
      // *****************************************************************
      svcmd_PutServerConfig:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_PutServerConfig;
               Inc (SendBufferPtr);

               StrCopy (PChar (SendBufferPtr),PChar (RecvBufferPtr));
               Inc (SendBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               tmp:=StrPas (PChar (RecvBufferPtr));
               Inc (RecvBufferPtr,length (tmp)+1);

               SendBufferPtr^:=1;
               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=tmp then
                  begin
                     SendBufferPtr^:=2;
                     for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                        if LowerCase (Username)=LowerCase (ServerService.Servers [a].grantedusers [b]) then
                        begin
                           Server.name:=tmp;
                           SendBufferPtr^:=0;
                           Break;
                        end;
                  end else if SendBufferPtr^=0 then break;

               if SendBufferPtr^=0 then
               begin
                  CopyMemory (@Server.watchdogenable,RecvBufferPtr,2);
                  Inc (RecvBufferPtr,2);

                  Server.currentconfig:=StrPas (PChar (RecvBufferPtr));
                  Inc (RecvBufferPtr,length (Server.currentconfig)+1);

                  SetLength (Server.configurations,RecvBufferPtr^);
                  Inc (RecvBufferPtr);

                  for a:=0 to length (Server.configurations)-1 do
                  begin
                     CopyMemory (@Server.configurations [a],RecvBufferPtr,31);
                     Inc (RecvBufferPtr,31);

                     Server.configurations [a].name:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].name)+1);
                     Server.configurations [a].domain:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].domain)+1);
                     Server.configurations [a].style:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].style)+1);
                     Server.configurations [a].sessionname:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].sessionname)+1);
                     Server.configurations [a].dificulty:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].dificulty)+1);
                     Server.configurations [a].password:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].password)+1);
                     Server.configurations [a].adminpass:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].adminpass)+1);
                     Server.configurations [a].clantag:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (Server.configurations [a].clantag)+1);

                     SetLength (Server.configurations [a].maps,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for b:=0 to length (Server.configurations [a].maps)-1 do
                     begin
                        Server.configurations [a].maps [b]:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (Server.configurations [a].maps [b])+1);
                     end;

                     SetLength (Server.configurations [a].messages,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for b:=0 to length (Server.configurations [a].messages)-1 do
                     begin
                        Server.configurations [a].messages [b]:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (Server.configurations [a].messages [b])+1);
                     end;

                     SetLength (Server.configurations [a].banlist,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for b:=0 to length (Server.configurations [a].banlist)-1 do
                     begin
                        Server.configurations [a].banlist [b]:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (Server.configurations [a].banlist [b])+1);
                     end;

                  end;

                  SendBufferPtr^:=0;
                  if (not ServerService.RebuildScriptForServer (@Server) or Server.PermanentError) then
                  begin
                     if Server.running or Server.ProcessStarted or Server.ProcessStarting then
                        for a:=0 to length (Server.Script)-1 do
                        begin
                           Command:=Server.Script [a];
                           _PServer:=@Server;
                           Synchronize (SendCommandToServer);
                        end;

                  end else
                     if Server.running or Server.ProcessStarted or Server.ProcessStarting then SendBufferPtr^:=255;

                  Inc (SendBufferPtr);

                  Overwrite:=true;
                  Synchronize (SaveConfiguration);
               end;

            end else
               begin
                  SendBufferPtr^:=1;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_AddServer:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_AddServer;
               Inc (SendBufferPtr);

               a:=length (ServerService.Servers);
               SetLength (ServerService.Servers,a+1);

               CopyMemory (@ServerService.Servers [a],RecvBufferPtr,5);
               Inc (RecvBufferPtr,5);

               ServerService.Servers [a].name:=StrPas (PChar (RecvBufferPtr));
               Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               ServerService.Servers [a].currentconfig:=StrPas (PChar (RecvBufferPtr));
               Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               SetLength (ServerService.Servers [a].grantedusers,RecvBufferPtr^);
               Inc (RecvBufferPtr);
               for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
               begin
                  ServerService.Servers [a].grantedusers [b]:=StrPas (PChar (RecvBufferPtr));
                  Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
               end;

               ServerService.Servers [a].currentconfig:='';
               ServerService.Servers [a].configurations:=nil;

               Overwrite:=true;
               Synchronize (SaveConfiguration);
            end;

      // *****************************************************************
      svcmd_UpdateServer:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_UpdateServer;
               Inc (SendBufferPtr);

               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=StrPas (PChar (RecvBufferPtr)) then
                  begin
                     Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

                     CopyMemory (@ServerService.Servers [a],RecvBufferPtr,2);
                     Inc (RecvBufferPtr,2);

                     ServerService.Servers [a].name:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

                     SetLength (ServerService.Servers [a].grantedusers,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                     begin
                        ServerService.Servers [a].grantedusers [b]:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
                     end;

                     break;
                  end;

                  Overwrite:=true;
               Synchronize (SaveConfiguration);
            end;

      // *****************************************************************
      svcmd_DeleteServer:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_DeleteServer;
               Inc (SendBufferPtr);

               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=StrPas (PChar (RecvBufferPtr)) then
                  begin
                     if ServerService.Servers [a].running and
                        (ServerService.Servers [a].ProcessStarted or
                        ServerService.Servers [a].ProcessStarting) then
                        begin
                           _PServer:=@ServerService.Servers [a]; 
                           Synchronize (StopServer);
                        end;

                     for b:=a to length (ServerService.Servers)-2 do
                        ServerService.Servers [a]:=ServerService.Servers [a+1];

                     SetLength (ServerService.Servers,length (ServerService.Servers)-1);
                     break;
                  end;
            end;

      // *****************************************************************
      svcmd_AddUser:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_AddUser;
               Inc (SendBufferPtr);

               SetLength (ServerService.ServerUsers,length (ServerService.ServerUsers)+1);

               ServerService.ServerUsers [length (ServerService.ServerUsers)-1].PrivilegeLevel:=RecvBufferPtr^;
               Inc (RecvBufferPtr);
               ServerService.ServerUsers [length (ServerService.ServerUsers)-1].Username:=StrPas (PChar (RecvBufferPtr));
               Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
               ServerService.ServerUsers [length (ServerService.ServerUsers)-1].Password:=StrPas (PChar (RecvBufferPtr));

               Overwrite:=true;
               Synchronize (SaveConfiguration);
            end else
               begin
                  SendBufferPtr^:=1;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_UpdateUser:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_UpdateUser;
               Inc (SendBufferPtr);

               for a:=0 to length (ServerService.ServerUsers)-1 do
                  if ServerService.ServerUsers [a].Username=StrPas (PChar (RecvBufferPtr)) then
                  begin
                     Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
                     ServerService.ServerUsers [a].PrivilegeLevel:=RecvBufferPtr^;
                     Inc (RecvBufferPtr);
                     ServerService.ServerUsers [a].Username:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
                     ServerService.ServerUsers [a].Password:=StrPas (PChar (RecvBufferPtr));
                     break;
                  end;

               Overwrite:=true;
               Synchronize (SaveConfiguration);
            end else
               begin
                  SendBufferPtr^:=1;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_DeleteUser:
            if Permissions>1 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_DeleteUser;
               Inc (SendBufferPtr);

               for a:=0 to length (ServerService.ServerUsers)-1 do
                  if ServerService.ServerUsers [a].Username=StrPas (PChar (RecvBufferPtr)) then
                  begin
                     for b:=a to length (ServerService.ServerUsers)-2 do
                        ServerService.ServerUsers [b]:=ServerService.ServerUsers [b+1];

                     SetLength (ServerService.ServerUsers,length (ServerService.ServerUsers)-1);
                     break;
                  end;

               Overwrite:=true;
               Synchronize (SaveConfiguration);
            end else
               begin
                  SendBufferPtr^:=1;
                  Inc (SendBufferPtr);
               end;

      // *****************************************************************
      svcmd_StartServer:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_StartServer;
               Inc (SendBufferPtr);

               StrCopy (PChar (SendBufferPtr),PChar (RecvBufferPtr));
               Inc (SendBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               tmp:=StrPas (PChar (RecvBufferPtr));

               SendBufferPtr^:=1;
               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=tmp then
                  begin
                     SendBufferPtr^:=2;
                     for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                        if LowerCase (Username)=LowerCase (ServerService.Servers [a].grantedusers [b]) then
                        begin
                           ServerService.Servers [a].running:=true;
                           if (not ServerService.Servers [a].ProcessStarted) and
                              (not ServerService.Servers [a].ProcessStarting) then
                           begin
                              _PServer:=@ServerService.Servers [a];
                              Synchronize (StartServer);
                           end;

                           SendBufferPtr^:=0;
                           Overwrite:=true;
                           Synchronize (SaveConfiguration);
                           Break;
                        end;

                     Break;
                  end;
            end;

      // *****************************************************************
      svcmd_StopServer:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_StopServer;
               Inc (SendBufferPtr);

               StrCopy (PChar (SendBufferPtr),PChar (RecvBufferPtr));
               Inc (SendBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               tmp:=StrPas (PChar (RecvBufferPtr));

               SendBufferPtr^:=1;
               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=tmp then
                  begin
                     SendBufferPtr^:=2;
                     for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                        if LowerCase (Username)=LowerCase (ServerService.Servers [a].grantedusers [b]) then
                        begin
                           ServerService.Servers [a].running:=false;
                           if ServerService.Servers [a].ProcessStarted or
                              ServerService.Servers [a].ProcessStarting then
                              begin
                                 _PServer:=@ServerService.Servers [a];
                                 Synchronize (StopServer);
                              end;

                           SendBufferPtr^:=0;
                           Overwrite:=true;
                           Synchronize (SaveConfiguration);
                           Break;
                        end;

                     Break;
                  end;
            end;

      // *****************************************************************
      svcmd_RestartServer:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_RestartServer;
               Inc (SendBufferPtr);

               StrCopy (PChar (SendBufferPtr),PChar (RecvBufferPtr));
               Inc (SendBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

               tmp:=StrPas (PChar (RecvBufferPtr));

               SendBufferPtr^:=1;
               for a:=0 to length (ServerService.Servers)-1 do
                  if ServerService.Servers [a].name=tmp then
                  begin
                     SendBufferPtr^:=2;
                     for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                        if LowerCase (Username)=LowerCase (ServerService.Servers [a].grantedusers [b]) then
                        begin
                           ServerService.Servers [a].running:=true;
                           if ServerService.Servers [a].ProcessStarted or
                           ServerService.Servers [a].ProcessStarting then
                           begin
                              _PServer:=@ServerService.Servers [a];
                              Synchronize (StopServer);
                              Synchronize (StartServer);
                           end;

                           SendBufferPtr^:=0;
                           Overwrite:=true;
                           Synchronize (SaveConfiguration);
                           Break;
                        end;

                     Break;
                  end;
            end;

      // *****************************************************************
      svcmd_ListServerPlayers:
            if Permissions>0 then
            begin
               Inc (RecvBufferPtr);

               SendBufferPtr^:=0;
               Inc (SendBufferPtr);
               SendBufferPtr^:=svcmd_ListServerPlayers;
               Inc (SendBufferPtr);

               tmp:=StrPas (PChar (RecvBufferPtr));

               SendBufferPtr^:=length (ServerService.Servers);
               Inc (SendBufferPtr);

               for a:=0 to length (ServerService.Servers)-1 do
               begin
                  StrCopy (PChar (SendBufferPtr),PChar (ServerService.Servers [a].name));
                  Inc (SendBufferPtr,StrLen (PChar (ServerService.Servers [a].name))+1);
                  SendBufferPtr^:=0;
                  Inc(SendBufferPtr);

                  if (ServerService.Servers [a].name=tmp) or (tmp='') then
                  begin
                       Dec (SendBufferPtr);
                       SendBufferPtr^:=255;
                       Inc (SendBufferPtr);

                       for b:=0 to length (ServerService.Servers [a].grantedusers)-1 do
                          if LowerCase (Username)=LowerCase (ServerService.Servers [a].grantedusers [b]) then
                          begin
                             Dec (SendBufferPtr);

                             if ServerService.Servers [a].DataReader <> nil then
                             begin

                                SendBufferPtr^:=ServerService.Servers [a].DataReader.NumPlayers;
                                Inc (SendBufferPtr);

                                for c:=0 to ServerService.Servers [a].DataReader.NumPlayers-1 do
                                begin
                                   userIp:=FindUserIPAddress(ServerService.Servers [a], ServerService.Servers [a].DataReader.Player [c].Name);
                                   userIp:=userIp+':>'+ServerService.Servers [a].DataReader.Player [c].Name;
                                   StrCopy (PChar (SendBufferPtr),PChar (userIp));
                                   Inc (SendBufferPtr,StrLen (PChar (userIp))+1);
                                end;
                             end
                                else
                                   begin
                                      SendBufferPtr^:=0;
                                      Inc (SendBufferPtr);
                                   end;

                             break;
                          end;
                    end;
               end;

            end;

      // *****************************************************************
      end;
      // *****************************************************************
      SendDataSize:=Integer (SendBufferPtr)-Integer (SendBuffer);
      SendBufferPtr:=SendBuffer;
      SendBufferPtr^:=Lo (SendDataSize); Inc (SendBufferPtr);
      SendBufferPtr^:=Hi (SendDataSize);

      if not Terminated and ClientSocket.Connected then Stream.Write (SendBuffer^,SendDataSize);

      RecvBufferPtr:=RecvBuffer;
      Inc (RecvBufferPtr,2);

      if RecvBufferPtr^=svcmd_ListServers then
      begin
         for a:=0 to length (ServerService.Servers)-1 do
            SendCommand (svcmd_PutServerStatus,ServerService.Servers [a].name);
      end;

end;

// *****************************************************************************
// *****************************************************************************
// * Part of Timer
// *****************************************************************************
// *****************************************************************************

var
   WatchDogTime:        Cardinal = 0;
   MessagingTime:       Cardinal = 0;

procedure ExecuteTimer (Handle: HWND; uMsg: Cardinal; idEvent: Pointer; dwTime: DWord); stdcall;
var
   a,b,c,d:             Integer;
   ExitCode:            Cardinal;

   Watchdog:            boolean;
   Messaging:           boolean;

   CurrentConfig:       Byte;

   CurrentTime:         Cardinal;

   CT,ET,KT,UT:         _FILETIME;
   CPUCurrentTime:      Cardinal;
   CPUUsage,CPUTimer:   Cardinal;

   Disconnected:        boolean;

   ipCurrent,ipBanned:  string;
   playerId:            string;

   playerBufferPointer:  Pointer;
   playerBuffer:         ^Byte;
   bufferData:           ^Byte;
   bytesReaded:          Cardinal;

   tlogFile:             TextFile;

begin
   CurrentTime:=GetTickCount;
   Watchdog:=false;
   Messaging:=false;

   if Assigned (ServerService) then
   begin

      if ServerService.ServerInfo.WatchDogEnable then
         if CurrentTime-WatchdogTime>ServerService.ServerInfo.WatchDogInterval*1000 then
         begin
            WatchdogTime:=CurrentTime;
            WatchDog:=true;
         end;

      if ServerService.ServerInfo.MessagingEnable then
         if CurrentTime-MessagingTime>ServerService.ServerInfo.MessagingInterval*1000 then
         begin
            MessagingTime:=CurrentTime;
            Messaging:=true;
         end;

      for a:=0 to length (ServerService.Servers)-1 do
      begin

         for b:=0 to length (ServerService.Servers [a].configurations)-1 do
            if ServerService.Servers [a].configurations [b].name=ServerService.Servers [a].currentconfig then
            begin
               CurrentConfig:=b;
               break;
            end;

         if ServerService.Servers [a].ProcessStarting and not ServerService.Servers [a].Initializing then
         begin

            GetExitCodeProcess (ServerService.Servers [a].ProcessInfo.hProcess,ExitCode);
            if (ServerService.Servers [a].InfoReceiveTime=0) or (ExitCode<>STILL_ACTIVE) then
            begin
               if (CurrentTime-ServerService.Servers [a].StartTime>(SERVER_STARTUP_TIMEOUT*1000))
                   or (ExitCode<>STILL_ACTIVE) then
               begin
                  ServerService.AddToLogFile (true,'Error starting '+ServerService.Servers [a].name+
                                                   ': try '+IntToStr (ServerService.Servers [a].StartTries)+'/3');
                  ServerService.StopServer (@ServerService.Servers [a]);
                  if ServerService.Servers [a].StartTries<3 then
                     ServerService.StartServer (@ServerService.Servers [a])
                  else
                     begin
                        ServerService.Servers [a].ProcessStarting:=false;
                        ServerService.Servers [a].ProcessStarted:=false;
                        ServerService.Servers [a].PermanentError:=true;
                        for b:=0 to length (ServerService.Threads)-1 do
                           TTCPServerThread (ServerService.Threads [a]).SendCommand (svcmd_PutServerStatus,ServerService.Servers [a].name);
                     end

               end else {ServerService.Servers [a].InfoReceiveTime:=1;}
                  if Assigned (ServerService.Servers [a].DataReader) then
                      ServerService.Servers [a].DataReader.ReceiveData;

            end else
               begin
                  ServerService.Servers [a].ProcessStarting:=false;
                  ServerService.Servers [a].ProcessStarted:=true;
                  ServerService.Servers [a].ProcessDown:=false;
                  ServerService.Servers [a].LastProcessedInfo:=ServerService.Servers [a].InfoReceiveTime;
                  for b:=0 to length (ServerService.Threads)-1 do
                     TTCPServerThread (ServerService.Threads [b]).SendCommand (svcmd_PutServerStatus,ServerService.Servers [a].name);
               end;

         end else

         if ServerService.Servers [a].ProcessStarted or ServerService.Servers [a].ProcessDown then
         begin
            if Assigned (ServerService.Servers [a].DataReader) then
                ServerService.Servers [a].DataReader.ReceiveData;

            // Check process CPU usage
            CPUCurrentTime:=GetTickCount;
            GetProcessTimes (ServerService.Servers [a].ProcessInfo.hProcess,CT,ET,KT,UT);

            CPUUsage:=((KT.dwLowDateTime+65536*KT.dwHighDateTime)+
                      (UT.dwLowDateTime+65536*UT.dwHighDateTime)) div 10000;

            CPUUsage:=CpuUsage-ServerService.Servers [a].CPULastUsage;
            CPUTimer:=CPUCurrentTime-ServerService.Servers [a].CPUCheckTime;

            if CpuTimer>0 then
            begin
               if (100/CPUTimer)*CpuUsage>80 then
                  Inc (ServerService.Servers [a].CPUUsageUpperTo80,CPUTimer)
               else
                  ServerService.Servers [a].CPUUsageUpperTo80:=0;

               ServerService.Servers [a].CPULastUsage:=((KT.dwLowDateTime+65536*KT.dwHighDateTime)+
                                                    (UT.dwLowDateTime+65536*KT.dwHighDateTime)) div 10000;
               ServerService.Servers [a].CPUCheckTime:=CPUCurrentTime;

               if ServerService.Servers [a].CPUUsageUpperTo80>SERVER_80USAGE_TIMEOUT then
               begin
                  ServerService.AddToLogFile (true,ServerService.Servers [a].name+' exceeds allowed CPU usage: '+
                                                   IntToStr (ServerService.Servers [a].CPUUsageUpperTo80));
                  TerminateProcess (ServerService.Servers [a].ProcessInfo.hProcess,0);
               end;
            end;

            // If is process down and may be up, start it
            if Watchdog and ServerService.ServerInfo.WatchDogEnable then
               if ServerService.Servers [a].ProcessDown then
                  ServerService.StartServer (@ServerService.Servers [a]);

            // Check if APP is still running
            GetExitCodeProcess (ServerService.Servers [a].ProcessInfo.hProcess,ExitCode);
            if (ExitCode<>STILL_ACTIVE) and ServerService.Servers [a].ProcessStarted then
            begin
               ServerService.Servers [a].ProcessDown:=true;
               ServerService.AddToLogFile (true,ServerService.Servers [a].name+' is down. Restarting...'); 
               ServerService.StopServer (@ServerService.Servers [a]);
            end;

            // Send messages to app
            if Messaging and not ServerService.Servers [a].ProcessDown then
            begin
               if ServerService.ServerInfo.ForcedMessagesEnable then
                  for b:=0 to length (ServerService.ServerInfo.ForcedMessages)-1 do
                     if Trim (ServerService.ServerInfo.ForcedMessages [b])<>'' then
                        ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                           Trim (ServerService.ServerInfo.ForcedMessages [b]));

                for b:=0 to length (ServerService.Servers [a].configurations [CurrentConfig].messages)-1 do
                  if Trim (ServerService.Servers [a].configurations [CurrentConfig].messages [b])<>'' then
                     ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                        Trim (ServerService.Servers [a].configurations [CurrentConfig].messages [b]));
            end;

            if ServerService.Servers [a].LastProcessedInfo<>ServerService.Servers [a].InfoReceiveTime then
            begin
               // Check kicked players list and delete disconnected from it
               b:=0;
               while b<ServerService.Servers [a].KickedPlayers.Count do
               begin
                  Disconnected:=true;
                  for c:=0 to ServerService.Servers [a].DataReader.NumPlayers-1 do
                     if ServerService.Servers [a].DataReader.Player [c].Name=
                           ServerService.Servers [a].KickedPlayers [b] then
                     begin
                        Disconnected:=false;
                        break;
                     end;

                  if Disconnected then
                     ServerService.Servers [a].KickedPlayers.Delete (b)
                  else inc (b);
               end;

               // Update current map user list

               // Read players from DS and store them in slots
               ReadProcessMemory(ServerService.Servers [a].ProcessInfo.hProcess, Pointer($009D6A4C + 4), @playerBufferPointer, 4, bytesReaded);

               GetMem(playerBuffer, 32 * 196);
               ReadProcessMemory(ServerService.Servers [a].ProcessInfo.hProcess, Pointer(Cardinal(playerBufferPointer)), playerBuffer, 32 * 196, bytesReaded);

               for b := 0 to 31 do
               begin
                  ServerService.Servers [a].playerSlots[b] := Pchar(Integer(@playerBuffer^) + (196 * b) + 8);

                  if ServerService.Servers [a].playerSlots[b] <> '' then
                  begin
                     bufferData := Pointer(Integer(@playerBuffer^) + (196 * b) + 4);
                     ServerService.Servers [a].playerIPs[b] := IntToStr(bufferData^) + '.';
                     Inc(bufferData);
                     ServerService.Servers [a].playerIPs[b] := ServerService.Servers [a].playerIPs[b] + IntToStr(bufferData^) + '.';
                     Inc(bufferData);
                     ServerService.Servers [a].playerIPs[b] := ServerService.Servers [a].playerIPs[b] + IntToStr(bufferData^) + '.';
                     Inc(bufferData);
                     ServerService.Servers [a].playerIPs[b] := ServerService.Servers [a].playerIPs[b] + IntToStr(bufferData^);
                  end else
                     ServerService.Servers [a].playerIPs[b] := '0.0.0.0';
               end;

               FreeMem(playerBuffer);

               // Check users with Global Ban List
               if ServerService.ServerInfo.ForcedBanListEnable then
                  for b:=0 to ServerService.Servers [a].DataReader.NumPlayers-1 do
                     if (ServerService.Servers [a].KickedPlayers.IndexOf (ServerService.Servers [a].DataReader.Player [b].Name)=-1) and
                        (ServerService.Servers [a].DataReader.Player [b].Side<2) then
                        for c:=0 to length(ServerService.ServerInfo.ForcedBanList)-1 do
                        begin
                           ipCurrent:=FindUserIPAddress(ServerService.Servers [a], ServerService.Servers [a].DataReader.Player [b].Name);
                           ipBanned:=ServerService.ServerInfo.ForcedBanList[c];
                           ipBanned:=Copy(ipBanned,1,pos(':>',ipBanned)-1);

                           playerId := '';
                           for d := 0 to 31 do
                              if ServerService.Servers [a].DataReader.Player [b].Name = ServerService.Servers [a].playerSlots[d] then
                                 playerId := IntToStr(d);

                           if (ipCurrent=ipBanned) And (playerId<>'') then
                           begin
                              ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                 ServerService.Servers [a].DataReader.Player [b].Name+'s ip '+ipCurrent+
                                                                 ' is banned!!!');
                              ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                 ServerService.Servers [a].DataReader.Player [b].Name+
                                                                 ' Contact server adminis to unban');
                              ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                 ServerService.Servers [a].DataReader.Player [b].Name+
                                                                 ' will be kicked now!');
                              ServerService.SendCommandToServer (@ServerService.Servers [a],'kickplayer ' + playerId);
                              ServerService.Servers [a].KickedPlayers.Add (ServerService.Servers [a].DataReader.Player [b].Name);
                              Break;
                           end;
                        end;

               // Check users with Ban Lists for current server
               for b:=0 to ServerService.Servers [a].DataReader.NumPlayers-1 do
                  if (ServerService.Servers [a].KickedPlayers.IndexOf (ServerService.Servers [a].DataReader.Player [b].Name)=-1) and
                    (ServerService.Servers [a].DataReader.Player [b].Side<2) then
                     for c:=0 to length(ServerService.Servers [a].configurations [CurrentConfig].banlist)-1 do
                     begin
                        ipCurrent:=FindUserIPAddress(ServerService.Servers [a], ServerService.Servers [a].DataReader.Player [b].Name);
                        ipBanned:=ServerService.Servers [a].configurations [CurrentConfig].banlist[c];
                        ipBanned:=Copy(ipBanned,1,pos(':>',ipBanned)-1);

                        playerId := '';
                        for d := 0 to 31 do
                           if ServerService.Servers [a].DataReader.Player [b].Name = ServerService.Servers [a].playerSlots[d] then
                              playerId := IntToStr(d);

                        if (ipCurrent=ipBanned) And (playerId<>'') then
                        begin
                           ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                              ServerService.Servers [a].DataReader.Player [b].Name+'s ip '+ipCurrent+
                                                              ' is banned!!!');
                           ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                              ServerService.Servers [a].DataReader.Player [b].Name+
                                                              ' Contact server adminis to unban');
                           ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                              ServerService.Servers [a].DataReader.Player [b].Name+
                                                              ' will be kicked now!');
                           ServerService.SendCommandToServer (@ServerService.Servers [a],'kickplayer ' + playerId);
                           ServerService.Servers [a].KickedPlayers.Add (ServerService.Servers [a].DataReader.Player [b].Name);
                           Break;
                        end;
                     end;

               // Check Autokick for non-clan side users
               if ServerService.Servers [a].configurations [CurrentConfig].enableautokick then
                  for b:=0 to ServerService.Servers [a].DataReader.NumPlayers-1 do
                  begin
                     if ServerService.Servers [a].KickedPlayers.IndexOf (ServerService.Servers [a].DataReader.Player [b].Name)=-1 then
                        if Pos (ServerService.Servers [a].configurations [CurrentConfig].clantag,ServerService.Servers [a].DataReader.Player [b].Name)=0 then
                           if (ServerService.Servers [a].DataReader.Player [b].Side<2) and
                              (ServerService.Servers [a].DataReader.Player [b].Side<>
                               ServerService.Servers [a].configurations [CurrentConfig].clanside) then
                           begin

                              playerId := '';
                              for d := 0 to 31 do
                                 if ServerService.Servers [a].DataReader.Player [b].Name = ServerService.Servers [a].playerSlots[d] then
                                    playerId := IntToStr(d);

                              if playerId <> '' then
                              begin
                                 ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                    ServerService.Servers [a].DataReader.Player [b].Name+
                                                                    ' is non clan side player');
                                 ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                    ServerService.Servers [a].DataReader.Player [b].Name+
                                                                    ' please select non-clan side');
                                 if ServerService.Servers [a].configurations [CurrentConfig].clanside=1 then
                                    ServerService.SendCommandToServer (@ServerService.Servers [a],'asay clan side is AXIS now')
                                 else
                                    ServerService.SendCommandToServer (@ServerService.Servers [a],'asay clan side is ALLIED now');

                                 ServerService.SendCommandToServer (@ServerService.Servers [a],'asay '+
                                                                    ServerService.Servers [a].DataReader.Player [b].Name+
                                                                    ' will be kicked now!');

                                 ServerService.SendCommandToServer (@ServerService.Servers [a],'kickplayer "'+
                                                                    ServerService.Servers [a].DataReader.Player [b].Name+'"');

                                 ServerService.Servers [a].KickedPlayers.Add (ServerService.Servers [a].DataReader.Player [b].Name);
                              end;
                           end;
                  end;

               // Check no of active users - reserve connections for clan-side

               ServerService.Servers [a].LastProcessedInfo:=ServerService.Servers [a].InfoReceiveTime
            end;

         end else

            if ServerService.Servers [a].running and
               ServerService.Servers [a].PermanentError and
               ServerService.Servers [a].ConfigChanged then
                  ServerService.StartServer (@ServerService.Servers [a]);
      end;
   end;
end;

// *****************************************************************************
// *****************************************************************************
// * Part of THD2Service
// *****************************************************************************
// *****************************************************************************

// START OF TCP SERVER
procedure THD2ServerService.StartTCPServer (Port: Word);
begin
   AddToLogFile (true,'Starting TCP server for remote managing on port '+IntToStr (Port));

   try
      TCPServer.Port:=Port;
      TCPServer.Active:=true;
   except
      on E: Exception do
      begin
         AddToLogFile (true,'Error while starting TCP server: ');
         AddToLogFile (true,E.Message);
         AddToLogFile (false,'');
         Exit;
      end;
   end;

   AddToLogFile (true,'TCP server succesfully started');
   AddToLogFile (false,'');
end;

// STOP OF TCP SERVER
procedure THD2ServerService.StopTCPServer;
begin
   if Assigned (TCPServer) and (TCPServer.Active) then
   begin
      TCPServer.Active:=false;

      AddToLogFile (true,'Stopping TCP server for remote managing');
      AddToLogFile (false,'');
   end;
end;

// CLIENT CONNECTED TO SERVER - NEED TO CREATE PROCESSING THREAD
procedure THD2ServerService.ClientGetThread (Sender: TObject; ClientSocket: TServerClientWinSocket;
                                             var SocketThread: TServerClientThread);
begin
   SocketThread:=TTCPServerThread.Create (true,ClientSocket);
   TTCPServerThread (SocketThread).Initialize;
   AddThread (SocketThread);
   SocketThread.Resume;
end;

procedure THD2ServerService.ClientDisconnect (Sender: TObject; Socket: TCustomWinSocket);
begin
end;

procedure THD2ServerService.ClientError (Sender: TObject; Socket: TCustomWinSocket;
                       ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
end;

procedure THD2ServerService.AddThread (Thread: TServerClientThread);
begin
   SetLength (Threads,length (Threads)+1);
   Threads [length (Threads)-1]:=Thread;
end;

procedure THD2ServerService.RemoveThread (Thread: TServerClientThread);
var
   a,b:   integer;
begin
   for a:=0 to length (Threads)-1 do
      if Threads [a]=Thread then
      begin
         for b:=a to length (Threads)-2 do Threads [b]:=Threads [b+1];
         SetLength (Threads,length (Threads)-1);
      end;
end;

procedure THD2ServerService.BuildScriptForServer (Server: PServer);

   procedure AddScriptLine (Line: string);
   begin
      SetLength (Server.Script,length (Server.Script)+1);
      Server.Script [length (Server.Script)-1]:=Line;
   end;

var
   Cfg:   PServerConfig;
   a,b:     Integer;

begin
   Server.Script:=nil;

   cfg:=nil;
   for a:=0 to length (Servers)-1 do
      if Servers [a].name=Server.name then
      begin
         for b:=0 to length (Servers [a].configurations)-1 do
            if Server.currentconfig=Servers [a].configurations [b].name then
            begin
               Cfg:=@Servers [a].configurations [b];
               break;
            end;
         break;
      end;

    if cfg<>nil then
    begin
       AddScriptLine ('sessionname "'+Cfg.sessionname+'"');
       AddScriptLine ('domain '+Cfg.domain);
       AddScriptLine ('dedicated 1');
       AddScriptLine ('port '+IntToStr (Server.port));
       AddScriptLine ('password "'+Cfg.password+'"');
       AddScriptLine ('adminpass "'+Cfg.adminpass+'"');
       AddScriptLine ('style '+Cfg.style);
       AddScriptLine ('maxclients '+IntToStr (Cfg.maxclients));
       AddScriptLine ('pointlimit '+IntToStr (Cfg.pointlimit));
       AddScriptLine ('roundlimit '+IntToStr (Cfg.roundlimit));
       AddScriptLine ('roundcount '+IntToStr (Cfg.roundcount));
       AddScriptLine ('respawntime '+IntToStr (Cfg.respawntime));
       AddScriptLine ('spawnprotection '+IntToStr (Cfg.spawnprotection));
       AddScriptLine ('warmup '+IntToStr (Cfg.warmup));
       AddScriptLine ('inversedamage '+IntToStr (Cfg.inversedamage));
       if Cfg.friendlyfire then AddScriptLine ('friendlyfire 1') else AddScriptLine ('friendlyfire 0');
       if Cfg.autoteambalance then AddScriptLine ('autoteambalance 1') else AddScriptLine ('autoteambalance 0');
       if Cfg._3rdpersonview then AddScriptLine ('3rdpersonview 1') else AddScriptLine ('3rdpersonview 0');
       if Cfg.allowcrosshair then AddScriptLine ('allowcrosshair 1') else AddScriptLine ('allowcrosshair 0');
       if Cfg.fallingdmg then AddScriptLine ('fallingdmg 1') else AddScriptLine ('fallingdmg 0');
       if Cfg.allowrespawn then AddScriptLine ('allowrespawn 1') else AddScriptLine ('allowrespawn 0');
       if Cfg.allowvehicles then AddScriptLine ('allowvehicles 1') else AddScriptLine ('allowvehicles 0');
       AddScriptLine ('cooplives '+IntToStr (cfg.respawnnumber));
       if Cfg.teamrespawn then AddScriptLine ('teamlives 1') else AddScriptLine ('teamlives 0');
       if LowerCase (Cfg.dificulty)='easy' then AddScriptLine ('coopdifficulty 1') else
          if LowerCase (Cfg.dificulty)='normal' then AddScriptLine ('coopdifficulty 2') else
             if LowerCase (Cfg.dificulty)='hard' then AddScriptLine ('coopdifficulty 3') else
                if LowerCase (Cfg.dificulty)='very hard' then AddScriptLine ('coopdifficulty 4');

       AddScriptLine ('maxping '+IntToStr (Cfg.maxping));
       AddScriptLine ('maxfreq '+IntToStr (Cfg.maxfreq));
       AddScriptLine ('maxinactivity '+IntToStr (Cfg.maxinactivity));

       case cfg.voicechat of
          0: AddScriptLine ('voicechat none');
          1: AddScriptLine ('voicechat vr12');
          2: AddScriptLine ('voicechat sc03');
          3: AddScriptLine ('voicechat sc06');
          4: AddScriptLine ('voicechat truespeech');
          5: AddScriptLine ('voicechat gsm');
          6: AddScriptLine ('voicechat adpcm');
          7: AddScriptLine ('voicechat pcm');
       end;

       for a:=0 to length (Cfg.maps)-1 do
          AddScriptLine ('mapname '+Cfg.maps [a]);
    end;
end;

function THD2ServerService.RebuildScriptForServer (Server: PServer): boolean;

   procedure AddScriptLine (Line: string);
   begin
      SetLength (Server.Script,length (Server.Script)+1);
      Server.Script [length (Server.Script)-1]:=Line;
   end;

var
   a,b,c:         Integer;
begin
   result:=false;
   
   Server.Script:=nil;

   for a:=0 to length (Servers)-1 do
      if Servers [a].name=Server.name then
      begin
         Servers [a].watchdogenable:=Server.watchdogenable;
         Servers [a].messagesenable:=Server.messagesenable;

         SetLength (Servers [a].configurations,length (Server.configurations));
         for b:=0 to length (Server.configurations)-1 do
         begin

            if Servers [a].configurations [b].maxclients<>Server.configurations [b].maxclients then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('maxclients '+IntToStr (Server.configurations [b].maxclients));
               Servers [a].configurations [b].maxclients:=Server.configurations [b].maxclients
            end;
            if Servers [a].configurations [b].pointlimit<>Server.configurations [b].pointlimit then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('pointlimit '+IntToStr (Server.configurations [b].pointlimit));
               Servers [a].configurations [b].pointlimit:=Server.configurations [b].pointlimit
            end;
            if Servers [a].configurations [b].roundlimit<>Server.configurations [b].roundlimit then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('roundlimit '+IntToStr (Server.configurations [b].roundlimit));
               Servers [a].configurations [b].roundlimit:=Server.configurations [b].roundlimit
            end;
            if Servers [a].configurations [b].roundcount<>Server.configurations [b].roundcount then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('roundcount '+IntToStr (Server.configurations [b].roundcount));
               Servers [a].configurations [b].roundcount:=Server.configurations [b].roundcount
            end;
            if Servers [a].configurations [b].respawntime<>Server.configurations [b].respawntime then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('respawntime '+IntToStr (Server.configurations [b].respawntime));
               Servers [a].configurations [b].respawntime:=Server.configurations [b].respawntime
            end;
            if Servers [a].configurations [b].spawnprotection<>Server.configurations [b].spawnprotection then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('spawnprotection '+IntToStr (Server.configurations [b].spawnprotection));
               Servers [a].configurations [b].spawnprotection:=Server.configurations [b].spawnprotection
            end;
            if Servers [a].configurations [b].warmup<>Server.configurations [b].warmup then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('warmup '+IntToStr (Server.configurations [b].warmup));
               Servers [a].configurations [b].warmup:=Server.configurations [b].warmup
            end;
            if Servers [a].configurations [b].inversedamage<>Server.configurations [b].inversedamage then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('inversedamage '+IntToStr (Server.configurations [b].inversedamage));
               Servers [a].configurations [b].inversedamage:=Server.configurations [b].inversedamage
            end;
            if Servers [a].configurations [b].friendlyfire<>Server.configurations [b].friendlyfire then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].friendlyfire then AddScriptLine ('friendlyfire 1') else AddScriptLine ('friendlyfire 0');
               Servers [a].configurations [b].friendlyfire:=Server.configurations [b].friendlyfire
            end;
            if Servers [a].configurations [b].autoteambalance<>Server.configurations [b].autoteambalance then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].autoteambalance then AddScriptLine ('autoteambalance 1') else AddScriptLine ('autoteambalance 0');
               Servers [a].configurations [b].autoteambalance:=Server.configurations [b].autoteambalance
            end;
            if Servers [a].configurations [b]._3rdpersonview<>Server.configurations [b]._3rdpersonview then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b]._3rdpersonview then AddScriptLine ('3rdpersonview 1') else AddScriptLine ('3rdpersonview 0');
               Servers [a].configurations [b]._3rdpersonview:=Server.configurations [b]._3rdpersonview
            end;
            if Servers [a].configurations [b].allowcrosshair<>Server.configurations [b].allowcrosshair then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].allowcrosshair then AddScriptLine ('allowcrosshair 1') else AddScriptLine ('allowcrosshair 0');
               Servers [a].configurations [b].allowcrosshair:=Server.configurations [b].allowcrosshair
            end;
            if Servers [a].configurations [b].fallingdmg<>Server.configurations [b].fallingdmg then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].fallingdmg then AddScriptLine ('fallingdmg 1') else AddScriptLine ('fallingdmg 0');
               Servers [a].configurations [b].fallingdmg:=Server.configurations [b].fallingdmg
            end;
            if Servers [a].configurations [b].allowrespawn<>Server.configurations [b].allowrespawn then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].allowrespawn then AddScriptLine ('allowrespawn 1') else AddScriptLine ('allowrespawn 0');
               Servers [a].configurations [b].allowrespawn:=Server.configurations [b].allowrespawn
            end;
            if Servers [a].configurations [b].allowvehicles<>Server.configurations [b].allowvehicles then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].allowvehicles then AddScriptLine ('allowvehicles 1') else AddScriptLine ('allowvehicles 0');
               Servers [a].configurations [b].allowvehicles:=Server.configurations [b].allowvehicles
            end;
            if Servers [a].configurations [b].respawnnumber<>Server.configurations [b].respawnnumber then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('cooplives '+IntToStr (Server.configurations [b].respawnnumber));
               Servers [a].configurations [b].respawnnumber:=Server.configurations [b].respawnnumber
            end;
            if Servers [a].configurations [b].teamrespawn<>Server.configurations [b].teamrespawn then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if Server.configurations [b].teamrespawn then AddScriptLine ('teamlives 1') else AddScriptLine ('teamlives 0');
               Servers [a].configurations [b].teamrespawn:=Server.configurations [b].teamrespawn
            end;
            if Servers [a].configurations [b].maxping<>Server.configurations [b].maxping then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('maxping '+IntToStr (Server.configurations [b].maxping));
               Servers [a].configurations [b].maxping:=Server.configurations [b].maxping
            end;
            if Servers [a].configurations [b].maxfreq<>Server.configurations [b].maxfreq then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('maxfreq '+IntToStr (Server.configurations [b].maxfreq));
               Servers [a].configurations [b].maxfreq:=Server.configurations [b].maxfreq
            end;
            if Servers [a].configurations [b].maxinactivity<>Server.configurations [b].maxinactivity then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('maxinactivity '+IntToStr (Server.configurations [b].maxinactivity));
               Servers [a].configurations [b].maxinactivity:=Server.configurations [b].maxinactivity
            end;
            if Servers [a].configurations [b].domain<>Server.configurations [b].domain then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('domain '+Server.configurations [b].domain);
               Servers [a].configurations [b].domain:=Server.configurations [b].domain
            end;
            if Servers [a].configurations [b].dificulty<>Server.configurations [b].dificulty then
            begin
               if Server.configurations [b].name=Server.currentconfig then
                  if LowerCase (Server.configurations [b].dificulty)='easy' then AddScriptLine ('coopdifficulty 1') else
                     if LowerCase (Server.configurations [b].dificulty)='normal' then AddScriptLine ('coopdifficulty 2') else
                        if LowerCase (Server.configurations [b].dificulty)='hard' then AddScriptLine ('coopdifficulty 3') else
                           if LowerCase (Server.configurations [b].dificulty)='very hard' then AddScriptLine ('coopdifficulty 4');
               Servers [a].configurations [b].dificulty:=Server.configurations [b].dificulty
            end;
            if Servers [a].configurations [b].password<>Server.configurations [b].password then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('password "'+Server.configurations [b].password+'"');
               Servers [a].configurations [b].password:=Server.configurations [b].password
            end;
            if Servers [a].configurations [b].adminpass<>Server.configurations [b].adminpass then
            begin
               if Server.configurations [b].name=Server.currentconfig then AddScriptLine ('adminpass "'+Server.configurations [b].adminpass+'"');
               Servers [a].configurations [b].adminpass:=Server.configurations [b].adminpass
            end;

            if Servers [a].configurations [b].sessionname<>Server.configurations [b].sessionname then
            begin
               Servers [a].configurations [b].sessionname:=Server.configurations [b].sessionname;
               if Server.configurations [b].name=Server.currentconfig then Result:=true;
            end;
            if Servers [a].configurations [b].style<>Server.configurations [b].style then
            begin
               Servers [a].configurations [b].style:=Server.configurations [b].style;
               if Server.configurations [b].name=Server.currentconfig then Result:=true;
            end;
            if Servers [a].configurations [b].voicechat<>Server.configurations [b].voicechat then
            begin
               Servers [a].configurations [b].voicechat:=Server.configurations [b].voicechat;
               if Server.configurations [b].name=Server.currentconfig then Result:=true;
            end;

            if (Server.configurations [b].name=Server.currentconfig) then
            begin
               if length (Servers [a].configurations [b].Maps)<>length (Server.configurations [b].maps) then
               begin
                  if Server.configurations [b].name=Server.currentconfig then Result:=true
               end else
                  for c:=0 to length (Server.configurations [b].maps)-1 do
                     if Servers [a].configurations [b].maps [c]<>Server.configurations [b].maps [c] then
                     begin
                        if Server.configurations [b].name=Server.currentconfig then Result:=true;
                        break;
                     end;
            end;

            SetLength (Servers [a].configurations [b].Maps,length (Server.configurations [b].maps));
            for c:=0 to length (Server.configurations [b].maps)-1 do
               Servers [a].configurations [b].maps [c]:=Server.configurations [b].maps [c];

            Servers [a].configurations [b].name:=Server.configurations [b].name;
            Servers [a].configurations [b].enableautokick:=Server.configurations [b].enableautokick;
            Servers [a].configurations [b].clanside:=Server.configurations [b].clanside;
            Servers [a].configurations [b].clanreserve:=Server.configurations [b].clanreserve;
            Servers [a].configurations [b].clantag:=Server.configurations [b].clantag;

            SetLength (Servers [a].configurations [b].messages,length (Server.configurations [b].messages));
            for c:=0 to length (Server.configurations [b].messages)-1 do
               Servers [a].configurations [b].messages [c]:=Server.configurations [b].messages [c];

            SetLength (Servers [a].configurations [b].banlist,length(Server.configurations [b].banlist));
            for c:=0 to length(Server.configurations [b].banlist)-1 do
               Servers [a].configurations [b].banlist [c]:=Server.configurations [b].banlist [c];

            Server.StartupInfo:=Servers [a].StartupInfo;
            Server.ProcessInfo:=Servers [a].ProcessInfo;
            Server.WindowHandle:=Servers [a].WindowHandle;
            Server.ProcessStarted:=Servers [a].ProcessStarted;
            Server.ProcessStarting:=Servers [a].ProcessStarting;
            Server.PermanentError:=Servers [a].PermanentError;
            Server.StartTime:=Servers [a].StartTime;
            Server.StartTries:=Servers [a].StartTries;
            Server.ConfigChanged:=Servers [a].ConfigChanged;
         end;

         if Servers [a].currentconfig<>Server.currentconfig then
         begin
            Result:=true;
            Servers [a].currentconfig:=Server.currentconfig;
         end;

         Servers [a].ConfigChanged:=Result;

         Break;
      end;

      if not Servers [a].running then Result:=false;
end;

var
   Handle:   HWND;

function EnumThreadWindowsCallback (Hndl: HWND; Param: LPARAM):boolean; stdcall;
begin
   Handle:=Hndl;
   Result:=false;
end;

procedure THD2ServerService.StartServer (Server: PServer);
var
   a:             Integer;

   CT,ET,KT,UT:   _FILETIME;

   Patcher:        Array [0..2] of byte;
   BytesReaded:    Cardinal;
begin
   Server.Initializing:=true;

   if Server.running and
        not (Server.ProcessStarted or Server.ProcessStarting) then
   begin
      Server.ProcessStarting:=true;
      Server.ProcessDown:=false;
      Server.InfoReceiveTime:=0;
      Server.StartTime:=GetTickCount;

      Server.KickedPlayers:=TStringList.Create;

      if Server.DataReader<>nil then
      begin
         Server.DataReader.Done;
         Server.DataReader:=nil;
         Sleep (1000);
      end;

      Server.DataReader:=THD2DS.Create (Server.name,'127.0.0.1',Server.Port);
      Server.DataReader.OnDataReceived:=ServerInfoReceived;

      if Server.ConfigChanged then
      begin
         Server.StartTries:=1;
         Server.ConfigChanged:=false;
         Server.PermanentError:=false;
      end else Inc (Server.StartTries);

      for a:=0 to 31 do
      begin
         Server.playerIPs[a] := '';
         Server.playerSlots[a] := '';
      end;

      AddToLogFile (true,'Starting server '+Server.name);
      AddToLogFile (false,'');

      for a:=0 to length (Threads)-1 do
         TTCPServerThread (Threads [a]).SendCommand (svcmd_PutServerStatus,Server.name);

      BuildScriptForServer (Server);

      ZeroMemory (@Server.StartupInfo,SizeOf (Server.StartupInfo));
      Server.StartupInfo.cb:=SizeOf (Server.StartupInfo);
      Server.StartupInfo.dwX:=CW_USEDEFAULT;
      Server.StartupInfo.dwY:=CW_USEDEFAULT;
      Server.StartupInfo.dwXSize:=CW_USEDEFAULT;
      Server.StartupInfo.dwYSize:=CW_USEDEFAULT;
      Server.StartupInfo.dwFlags:=STARTF_USESHOWWINDOW;
      Server.StartupInfo.wShowWindow:=SW_HIDE;

      if CreateProcess (PChar (ServerInfo.DedicatedServerPath),
                        nil,nil,nil,false,
                        CREATE_DEFAULT_ERROR_MODE or NORMAL_PRIORITY_CLASS,nil,
                        PChar (ExtractFilePath (ServerInfo.DedicatedServerPath)),
                        Server.StartupInfo,Server.ProcessInfo) then
      begin
         if WaitForInputIdle (Server.ProcessInfo.hProcess,10000)=0 then
         begin
            // If the server is the SQ DS, patch memory to allow 64 players
            ReadProcessMemory(Server.ProcessInfo.hProcess,
                              Pointer($007A2A0A),
                              @Patcher, 3, bytesReaded);

            if (Patcher[0] = $83) and (Patcher[1] = $F8) and (Patcher[2] = $06) then
            begin
               Patcher[2] := $40;
               WriteProcessMemory(Server.ProcessInfo.hProcess,
                                  Pointer($007A29F0),
                                  @Patcher,
                                  3,
                                  bytesReaded);

               WriteProcessMemory(Server.ProcessInfo.hProcess,
                                  Pointer($007A2A0A),
                                  @Patcher,
                                  3,
                                  bytesReaded);
            end;

            // If the server is HD2DS, patch memory to allow 64 players

            ReadProcessMemory(Server.ProcessInfo.hProcess,
                              Pointer($7A25C6),
                              @Patcher, 3, bytesReaded);

            if (Patcher[0] = $83) and (Patcher[1] = $F8) and (Patcher[2] = $20) then
            begin
               Patcher[2] := $40;
               WriteProcessMemory(Server.ProcessInfo.hProcess,
                                  Pointer($7A25C6),
                                  @Patcher,
                                  3,
                                  bytesReaded);
            end;

            Server.CPUCheckTime:=GetTickCount;
            GetProcessTimes (Server.ProcessInfo.hProcess,CT,ET,KT,UT);
            Server.CPULastUsage:=((KT.dwLowDateTime+65536*KT.dwHighDateTime)+
                                 (UT.dwLowDateTime+65536*KT.dwHighDateTime)) div 10000;
            Server.CPUUsageUpperTo80:=0;

            Handle:=0;
            EnumThreadWindows (Server.ProcessInfo.dwThreadId,@EnumThreadWindowsCallback,0);

            if Handle<>0 then
            begin
               Server.WindowHandle:=Handle;

               if HiddenWindows then ShowWindow (Server.WindowHandle,sw_hide);
               SetWindowText (Server.WindowHandle, PChar('HD2 Server Console - '+Server.Name));

               for a:=0 to length (Server.Script)-1 do
                  SendCommandToServer (Server,Server.Script [a]);

               SendCommandToServer (Server,'server');

               WaitForInputIdle (Server.ProcessInfo.hProcess,INFINITE);

            end else
               begin
                  TerminateProcess (Server.ProcessInfo.hProcess,0);
                  AddToLogFile (true,'Server '+Server.name+' window not found');
                  AddToLogFile (false,'');
               end;
         end else
            begin
               TerminateProcess (Server.ProcessInfo.hProcess,0);
               AddToLogFile (true,'Wait for server '+Server.name+' idle failed');
               AddToLogFile (false,'');
            end;

      end else
         begin
            AddToLogFile (true,'Starting server '+Server.name+' failed');
            AddToLogFile (false,'');
         end;
   end;

   Server.Initializing:=false;
end;

procedure THD2ServerService.StopServer (Server: PServer);
var
   a:        Integer;
   ExitCode: Cardinal;
begin
   if Server.ProcessStarted or Server.ProcessStarting then
   begin
      if Server.ProcessStarted then
      begin
         Server.StartTries:=0;
         Server.PermanentError:=false;
      end;

      Server.InfoReceiveTime:=0;
      Server.ProcessStarted:=false;
      Server.ProcessStarting:=false;
      if Server.DataReader<>nil then
      begin
         Server.DataReader.Done;
         Server.DataReader:=nil;
      end;

      Server.KickedPlayers.Free;

      AddToLogFile (true,'Stopping server '+Server.name);
      AddToLogFile (false,'');

      ShowWindow (Server.WindowHandle,sw_show);

      GetExitCodeProcess (Server.ProcessInfo.hProcess,ExitCode);
      if ExitCode=STILL_ACTIVE then
      begin
         SendCommandToServer (Server,'quit');

         for a:=0 to 100 do
         begin
            GetExitCodeProcess (Server.ProcessInfo.hProcess,ExitCode);
            if ExitCode=STILL_ACTIVE then sleep (100) else break;
         end;
      end;

      if ExitCode=STILL_ACTIVE then
         TerminateProcess (Server.ProcessInfo.hProcess,1);

      for a:=0 to length (Threads)-1 do
         TTCPServerThread (Threads [a]).SendCommand (svcmd_PutServerStatus,Server.name);
   end;
end;

procedure THD2ServerService.ServerInfoReceived (ServerName: string; ServerInfo: PHD2DS);
var
   a:   Integer;
begin
   for a:=0 to length (Servers)-1 do
      if Servers [a].name=ServerName then
      begin
         if (ServerInfo.Hostname<>'') and (ServerInfo.Gameversion<>'') and
            (ServerInfo.MapName<>'') and (ServerInfo.GameStyle<>'') then
               Servers [a].InfoReceiveTime:=GetTickCount;
         break;
      end;
end;

procedure THD2ServerService.SendCommandToServer (Server: PServer; Command: string);
var
   a:   Integer;
begin
   for a:=1 to length (Command) do
   begin
      PostMessage (Server.WindowHandle,wm_char,Ord (Command [a]),0);
      Sleep(10);
   end;

   PostMessage (Server.WindowHandle,wm_keydown,vk_return,0);
   Sleep(25);
end;

procedure THD2ServerService.ShowWindows;
var
   a:   Integer;
begin
   HiddenWindows:=false;
   for a:=0 to length (Servers)-1 do
      ShowWindow(Servers [a].WindowHandle,SW_SHOW);
end;

procedure THD2ServerService.HideWindows;
var
   a:   Integer;
begin
   HiddenWindows:=true;
   for a:=0 to length (Servers)-1 do
      ShowWindow(Servers [a].WindowHandle,SW_HIDE);
end;

initialization
   ServerService:=nil;

end.
