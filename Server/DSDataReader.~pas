unit DSDataReader;

interface

uses Windows,Classes,Sysutils,WSockets,WinSock;

type
  THD2DSPlayer = record
     Name:   string;
     Side:   integer;
     Score:  integer;
     Deaths: integer;
     Ping:   integer;
  end;

  PHD2DS = ^THD2DS;
  TDataReceivedProc = procedure (ServerName: string; ServerInfo: PHD2DS) of object;

  THD2DS = class
     private
        UDP:              TUDPClient;

        intServerName:    string;

        intHostname:      string;
        intGameversion:   string;
        intMapName:       string;
        intNumPlayers:    Byte;
        intMaxPlayers:    Byte;
        intGameMode:      Byte;
        intGameStyle:     string;
        intIsDedicated:   boolean;
        intPassword:      boolean;
        intVoicechat:     Byte;
        intExpansion:     boolean;
        intPlayers:       array of THD2DSPlayer;
        intAxisScore:     byte;
        intAlliedScore:   byte;

        DataReceivedProc: TDataReceivedProc;
        procedure SetDataRecvProc (Proc: TDataReceivedProc);

        function GetHostname: string;
        function GetGameVersion: string;
        function GetMapame: string;
        function GetNumPlayers: Byte;
        function GetMaxPlayers: Byte;
        function GetGameMode: Byte;
        function GetGameStyle: string;
        function GetIsDedicated: boolean;
        function GetPassword: boolean;
        function GetVoicechat: Byte;
        function GetExpansion: boolean;
        function GetNoPlayers: Byte;
        function GetPlayer (Index: Byte): THD2DSPlayer;
        function GetAxisScore: byte;
        function GetAlliedScore: byte;

        procedure ClearData;

        procedure UDPDataReceived (Sender: TObject; Socket: TSocket);
        procedure UDPError (Sender: TObject; Error: integer; Msg: string);

        
     public
        constructor Create (ServerName,Host: string; BasePort: Word);
        procedure Done;

        procedure ReceiveData;

        property OnDataReceived: TDataReceivedProc write SetDataRecvProc;

        property Hostname:     string read GetHostname;
        property Gameversion:  string read GetGameVersion;
        property MapName:      string read GetMapame;
        property NumPlayers:   Byte read GetNumPlayers;
        property MaxPlayers:   Byte read GetMaxPlayers;
        property GameMode:     Byte read GetGameMode;
        property GameStyle:    string read GetGameStyle;
        property IsDedicated:  boolean read GetIsDedicated;
        property Password:     boolean read GetPassword;
        property Voicechat:    Byte read GetVoicechat;
        property Expansion:    boolean read GetExpansion;

        property NoPlayers:   Byte read GetNoPlayers;

        property Player [Index: Byte]: THD2DSPlayer read GetPlayer;

        property AxisScore: byte read GetAxisScore;
        property AlliedScore: byte read GetAlliedScore;

     end;

implementation

constructor THD2DS.Create (ServerName,Host: string; BasePort: Word);
begin
   ClearData;

   DataReceivedProc:=nil;

   UDP:=TUDPClient.Create (nil);
   UDP.OnData:=UDPDataReceived;
   UDP.OnError:=UDPError;
   UDP.Host:=Host;
   UDP.Port:=IntToStr (BasePort+3);
   UDP.Open;

   intServerName:=ServerName;
end;

procedure THD2DS.Done;
begin
   UDP.Close;
   UDP.Free;
   Free;
end;

procedure THD2DS.SetDataRecvProc (Proc: TDataReceivedProc);
begin
   DataReceivedProc:=Proc;
end;

procedure THD2DS.ClearData;
begin
   intHostname:='';
   intGameVersion:='';
   intMapName:='';
   intNumPlayers:=0;
   intMaxPlayers:=0;
   intGameMode:=0;
   intGameStyle:='';
   intIsDedicated:=false;
   intPassword:=false;
   intVoicechat:=0;
   intExpansion:=false;
   intPlayers:=nil;
end;

procedure THD2DS.ReceiveData;
const
   Request:       array [0..9] of byte = ($FE,$FD,$00,$43,$4F,$52,$59,$FF,$FF,$FF);
var
   RequestBuffer: array [0..9] of char;
begin
   CopyMemory (@RequestBuffer,@Request,10);

   try
      if Assigned (UDP) then
         UDP.WriteBuffer (@RequestBuffer,10);
   except
      on E: Exception do
   end;
end;

procedure THD2DS.UDPError (Sender: TObject; Error: integer; Msg: string);
begin
   if (Error<>0) and (Msg<>'') then
   begin
   end;
end;

procedure THD2DS.UDPDataReceived (Sender: TObject; Socket: TSocket);
var
   ReceivedData:    array [0..32768] of char;
   ReceivedDataPtr: PChar;

   ParamCount:      byte;
   NoPlayers:       byte;

   NoTeamInfo:      byte;

   Tmp:             string;

   NumberBytes:     integer;

   a,b:             integer;
begin
   ClearData;

   NumberBytes:=32767;

   if NumberBytes<=32767 then
   begin
      ReceivedDataPtr:=ReceivedData;
      inc (ReceivedDataPtr,5);

      UDP.ReadBuffer (@ReceivedData,NumberBytes);

      Tmp:='';
      while StrLen (ReceivedDataPtr)<>0 do
      begin
         if Tmp='' then
            Tmp:=ReceivedDataPtr
         else
            begin
               if LowerCase (Tmp)='hostname' then intHostname:=ReceivedDataPtr
               else if LowerCase (Tmp)='gamever' then intGameVersion:=ReceivedDataPtr
               else if LowerCase (Tmp)='mapname' then intMapName:=ReceivedDataPtr
               else if LowerCase (Tmp)='numplayers' then intNumPlayers:=StrToInt (ReceivedDataPtr)
               else if LowerCase (Tmp)='maxplayers' then intMaxPlayers:=StrToInt (ReceivedDataPtr)
               else if LowerCase (Tmp)='gamemode' then intGameMode:=StrToInt (ReceivedDataPtr)
               else if LowerCase (Tmp)='gametype' then intGameStyle:=ReceivedDataPtr
               else if LowerCase (Tmp)='isdedicated' then intIsDedicated:=StrToInt (ReceivedDataPtr) = 1
               else if LowerCase (Tmp)='password' then intPassword:=StrToInt (ReceivedDataPtr) = 1
               else if LowerCase (Tmp)='voicechat' then intVoiceChat:=StrToInt (ReceivedDataPtr)
               else if LowerCase (Tmp)='expansion' then intExpansion:=StrToInt (ReceivedDataPtr) = 1;

               Tmp:='';
            end;
         inc (ReceivedDataPtr,strlen (ReceivedDataPtr)+1);
      end;

      inc (ReceivedDataPtr);

      if strlen (ReceivedDataPtr)=0 then
      begin
         inc (ReceivedDataPtr);

         NoPlayers:=Byte (ReceivedDataPtr^);
         inc (ReceivedDataPtr);

         ParamCount:=0;
         tmp:='';
         while strlen (ReceivedDataPtr)<>0 do
         begin
            tmp:=tmp+ReceivedDataPtr;
            inc (ReceivedDataPtr,strlen (ReceivedDataPtr)+1);
            inc (ParamCount);
         end;

         if strlen (ReceivedDataPtr)=0 then
         begin
            inc (ReceivedDataPtr);

            if (ParamCount=5) and (LowerCase (tmp)='player_score_deaths_ping_team_') then
            begin
               SetLength (intPlayers,NoPlayers);
               for a:=0 to NoPlayers-1 do
                  for b:=1 to 5 do
                  begin
                     case b of
                        1: intPlayers [a].Name:=ReceivedDataPtr;
                        2: intPlayers [a].Score:=StrToInt (ReceivedDataPtr);
                        3: intPlayers [a].Deaths:=StrToInt (ReceivedDataPtr);
                        4: intPlayers [a].Ping:=StrToInt (ReceivedDataPtr);
                        5: intPlayers [a].Side:=StrToInt (ReceivedDataPtr);
                     end;

                     inc (ReceivedDataPtr,strlen (ReceivedDataPtr)+1);
                  end;
            end;

            if strlen (ReceivedDataPtr)=0 then
            begin
               inc (ReceivedDataPtr);

               NoTeamInfo:=Byte (ReceivedDataPtr^);
               inc (ReceivedDataPtr);

               if NoTeamInfo=2 then
               begin
                  ParamCount:=0;
                  tmp:='';
                  while strlen (ReceivedDataPtr)<>0 do
                  begin
                     tmp:=tmp+ReceivedDataPtr;
                     inc (ReceivedDataPtr,strlen (ReceivedDataPtr)+1);
                     inc (ParamCount);
                  end;

                  inc (ReceivedDataPtr);

                  if (ParamCount=2) and (tmp='team_tscore_t') then
                  begin
                     Tmp:='';
                     while StrLen (ReceivedDataPtr)<>0 do
                     begin
                        if Tmp='' then
                           Tmp:=ReceivedDataPtr
                        else
                           begin
                              if LowerCase (Tmp)='axis' then intAxisScore:=StrToInt (ReceivedDataPtr)
                              else if LowerCase (Tmp)='allied' then intAlliedScore:=StrToInt (ReceivedDataPtr);
                              Tmp:='';
                           end;
                        inc (ReceivedDataPtr,strlen (ReceivedDataPtr)+1);
                     end;
                  end;
               end;
            end;
         end;

         if Assigned (DataReceivedProc) then DataReceivedProc (intServerName,@self);

      end;
   end;
end;

function THD2DS.GetHostname: string;
begin
   Result:=intHostname;
end;

function THD2DS.GetGameVersion: string;
begin
   Result:=intGameVersion;
end;

function THD2DS.GetMapame: string;
begin
   Result:=intMapName;
end;

function THD2DS.GetNumPlayers: Byte;
begin
   Result:=intNumPlayers;
end;

function THD2DS.GetMaxPlayers: Byte;
begin
   Result:=intMaxPlayers;
end;

function THD2DS.GetGameMode: Byte;
begin
   Result:=intGameMode;
end;

function THD2DS.GetGameStyle: string;
begin
   Result:=intGameStyle;
end;

function THD2DS.GetIsDedicated: boolean;
begin
   Result:=intIsDedicated;
end;

function THD2DS.GetPassword: boolean;
begin
   Result:=intPassword;
end;

function THD2DS.GetVoicechat: Byte;
begin
   Result:=intVoicechat;
end;

function THD2DS.GetExpansion: boolean;
begin
   Result:=intExpansion;
end;

function THD2DS.GetNoPlayers: Byte;
begin
   Result:=length (intPlayers);
end;

function THD2DS.GetPlayer (Index: Byte): THD2DSPlayer;
begin
   if Index<GetNoPlayers then Result:=intPlayers [Index];
end;

function THD2DS.GetAxisScore: byte;
begin
   Result:=intAxisScore;
end;

function THD2DS.GetAlliedScore: byte;
begin
   Result:=intAlliedScore;
end;

end.
