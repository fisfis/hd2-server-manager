program Tester;

uses
  Forms,
  TestMain in 'TestMain.pas' {Form1},
  common in '..\common.pas',
  HD2ServerService in 'HD2ServerService.pas',
  DSDataReader in 'DSDataReader.pas',
  WSockets in 'WSockets.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'HD2 Server Manager';
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
