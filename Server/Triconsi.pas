
{***********************************************************}
{                                                           }
{     VCL komponenty SINEA                 verze 4.x        }
{                                                           }
{     Copyright(c) 1992,2000    Ing. Jaroslav Skerik        }
{                               RNDr. Vaclav Krmela         }
{                                                           }
{***********************************************************}
{  Minimalizace aplikace do Tray ikony                      }
{***********************************************************}

unit TrIconSi;

interface

uses
  SysUtils, Windows, ShellApi, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Menus;

const
  WM_IconEvent = wm_User + 10;

type
  TTrayIconSinea = class(TComponent)
  private
    { Private declarations }
    FVisible : boolean;
    FAppVisible : boolean;
    FIcon : TIcon;
    FHint : string;
    FHandle : THandle;
    FNotifyIconData : TNotifyIconData;
    FPopupMenu: TPopupMenu;
    FOnMouseMove: TNotifyEvent;
    FOnMouseDown: TNotifyEvent;
    FOnMouseUp: TNotifyEvent;
    FOnDblClick: TNotifyEvent;
    procedure WndProc(var Msg: TMessage);
    procedure SetAppVisible(Value: boolean);
    procedure SetVisible(Value: boolean);
    procedure SetIcon(Value: TIcon);
    procedure SetHint(const Value: string);
    procedure SetPopupMenu(Value: TPopupMenu);
    procedure CheckMenuPopup;
    procedure SetNotifyIconData;
    function GetFileVersion: string;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure Show;
    procedure Modify;
    procedure Hide;
    procedure AppShow;
    procedure AppHide;
    property FileVersion: string read GetFileVersion;
  published
    { Published declarations }
    property AppVisible : boolean read FAppVisible write SetAppVisible;
    property Visible : boolean read FVisible write SetVisible stored true default true;
    property Icon : TIcon read FIcon write SetIcon;
    property Hint : string read FHint write SetHint;
    property PopupMenu: TPopupMenu read FPopupMenu write SetPopupMenu;
    property OnMouseMove: TNotifyEvent read FOnMouseMove write FOnMouseMove;
    property OnMouseDown: TNotifyEvent read FOnMouseDown write FOnMouseDown;
    property OnMouseUp: TNotifyEvent read FOnMouseUp write FOnMouseUp;
    property OnDblClick: TNotifyEvent read FOnDblClick write FOnDblClick;
  end;

procedure Register;

implementation

constructor TTrayIconSinea.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FHandle := AllocateHWnd(WndProc);
  FIcon := TIcon.Create;
  FVisible := true; FAppVisible := true;
  with FNotifyIconData do
  begin
    cbSize := SizeOf(FNotifyIconData);
    Wnd    := FHandle;
    uID    := 0;
    uCallBackMessage := WM_IconEvent;
  end;
end;

destructor TTrayIconSinea.Destroy;
begin
  if FVisible then Hide;
  FIcon.Free;
  DeallocateHWnd(FHandle);
  inherited Destroy;
end;

procedure TTrayIconSinea.WndProc(var Msg: TMessage);
begin
  with Msg do
  if Msg = WM_IconEvent then
    try
      case LParam of
      WM_MouseMove: if Assigned(FOnMouseMove) then FOnMouseMove(Self);
      WM_LButtonDown: if Assigned(FOnMouseDown) then FOnMouseDown(Self);
      WM_LButtonUp: if Assigned(FOnMouseUp) then FOnMouseUp(Self);
      WM_LButtonDblClk: if Assigned(OnDblClick) then OnDblClick(Self)
           else if not FAppVisible then SetAppVisible(true);
      WM_RButtonUp: CheckMenuPopup;
      end;
    except
      Application.HandleException(Self);
    end
  else
  begin
    Result := DefWindowProc(FHandle, Msg, wParam, lParam);
  end;
end;

procedure TTrayIconSinea.Loaded;
begin
  inherited Loaded;
  SetAppVisible(FAppVisible);
  SetVisible(FVisible);
end;

procedure TTrayIconSinea.SetAppVisible(Value: boolean);
begin
  if not Assigned(Owner) then Exit;
  if not (csDesigning in ComponentState) then
  begin
    if Value then
      ShowWindow(Application.Handle,SW_SHOW)
    else
    begin
      ShowWindow(Application.Handle,SW_HIDE);
      Application.ShowMainForm := False;
    end;
    (Owner as TForm).Visible := Value;
  end;
  FAppVisible := Value;
end;

procedure TTrayIconSinea.SetVisible(Value: boolean);
begin
  if not (csDesigning in ComponentState) then
  begin
    if Value then
    begin
      SetNotifyIconData;
      FVisible := Shell_NotifyIcon(NIM_ADD, @FNotifyIconData);
    end else
    begin
      if not FVisible then Exit;
      FVisible := not Shell_NotifyIcon(NIM_DELETE, @FNotifyIconData);
    end;
  end;
  FVisible := Value;
end;

procedure TTrayIconSinea.SetIcon(Value: TIcon);
begin
  FIcon.Assign(Value);
  Modify;
end;

procedure TTrayIconSinea.SetHint(const Value: string);
begin
  FHint := Value;
  Modify;
end;

procedure TTrayIconSinea.SetPopupMenu(Value: TPopupMenu);
begin
  FPopupMenu := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TTrayIconSinea.CheckMenuPopup;
var Pos: TPoint;
begin
  if csDesigning in ComponentState then Exit;
  if (FPopupMenu <> nil) and FPopupMenu.AutoPopup then
  begin
    GetCursorPos(Pos);
    FPopupMenu.Popup(Pos.X, Pos.Y);
  end;
end;

procedure TTrayIconSinea.SetNotifyIconData;
begin
  with FNotifyIconData do
  begin
    uFlags := 0;
    hIcon  := FIcon.Handle;
    StrPLCopy(szTip, FHint, 63);
    if FHandle <> 0 then uFlags := NIF_MESSAGE;
    if hIcon <> 0 then uFlags := uFlags or NIF_ICON;
    if StrLen(szTip) > 0 then uFlags := uFlags or NIF_TIP;
  end;
end;

procedure TTrayIconSinea.Show;
begin
  SetVisible(true);
end;

procedure TTrayIconSinea.Modify;
begin
{  if not FVisible then Exit;}
  SetNotifyIconData;
  Shell_NotifyIcon(NIM_MODIFY, @FNotifyIconData);
end;

procedure TTrayIconSinea.Hide;
begin
  SetVisible(false);
end;

procedure TTrayIconSinea.AppShow;
begin
  SetAppVisible(true);
end;

procedure TTrayIconSinea.AppHide;
begin
  SetAppVisible(false);
end;

function TTrayIconSinea.GetFileVersion: string;
{ prevzato z Jine komponenty / FileVersion }
const
  VerInfoRoot: PChar = '\';
var
  SizeVer: Integer;
  SBSize: UInt;
  dwHandle: DWord;
  FData: String;
  FVer: pointer;
  FileInfo: PVSFixedFileInfo;
begin
  SizeVer := GetFileVersionInfoSize(PChar(Application.ExeName), dwHandle);
  if SizeVer > 0 then
    begin
      SetLength(FData, SizeVer);
      try
        if GetFileVersionInfo(PChar(Application.ExeName), dwHandle, SizeVer, PChar(FData)) then
          if VerQueryValue(PChar(FData), VerInfoRoot, FVer, SBSize) then
            begin
              FileInfo := PVSFixedFileInfo(FVer);
{              FStructureVersion := IntToStr(HIWORD(FileInfo^.dwStrucVersion)) + '.' +
                                   IntToStr(LOWORD(FileInfo^.dwStrucVersion));}
              Result {FFileVersion} :=
                    IntToStr(HIWORD(FileInfo^.dwFileVersionMS)) + '.' +
                    IntToStr(LOWORD(FileInfo^.dwFileVersionMS)) + '.' +
                    IntToStr(HIWORD(FileInfo^.dwFileVersionLS)) + '.' +
                    IntToStr(LOWORD(FileInfo^.dwFileVersionLS));
{              FProductVersion := IntToStr(HIWORD(FileInfo^.dwProductVersionMS)) + '.' +
                                 IntToStr(LOWORD(FileInfo^.dwProductVersionMS)) + '.' +
                                 IntToStr(HIWORD(FileInfo^.dwProductVersionLS)) + '.' +
                                 IntToStr(LOWORD(FileInfo^.dwProductVersionLS));
              case FileInfo^.dwFileOS of
                VOS_UNKNOWN: FFileOS := 'Nezn�m�';
                VOS_DOS: FFileOS := 'MS-DOS';
                VOS_OS216: FFileOS := '16-bit OS2';
                VOS_OS232: FFileOS := '32-bit OS2';
                VOS_NT: FFileOS := 'Windows NT';
                VOS__WINDOWS16: FFileOS := '16-bit Windows';
                VOS__PM16: FFileOS := '16-bit Presentation Manager';
                VOS__PM32: FFileOS := '32-bit Presentation Manager';
                VOS__WINDOWS32: FFileOS := 'Win32 API';
                VOS_DOS_WINDOWS16: FFileOS := '16-bit Windows pod MS-DOS';
                VOS_DOS_WINDOWS32: FFileOS := 'Win32 API pod MS-DOS';
                VOS_OS216_PM16: FFileOS := '16-bit Presentation Manager pod 16-bit OS2';
                VOS_OS232_PM32: FFileOS := '32-bit Presentation Manager pod 32-bit OS2';
                VOS_NT_WINDOWS32: FFileOS := 'Win32 API pod Windows NT';
              end;
              case FileInfo^.dwFileType of
                VFT_UNKNOWN: FFileType := 'Nezn�m�';
                VFT_APP: FFileType := 'Aplikace';
                VFT_DLL: FFileType := 'Dynamick� knihovna';
                VFT_DRV: FFileType := 'Ovlada�';
                VFT_FONT: FFileType := 'Font';
                VFT_VXD: FFileType := 'Virtu�ln� ovlada�';
                VFT_STATIC_LIB: FFileType := 'Statick� knihovna';
              end;
              FFileDate := IntToStr(HIWORD(FileInfo^.dwFileDateMS)) + '.' +
                           IntToStr(LOWORD(FileInfo^.dwFileDateMS)) + '.' +
                           IntToStr(HIWORD(FileInfo^.dwFileDateLS)) + '.' +
                           IntToStr(LOWORD(FileInfo^.dwFileDateLS));
              FCompanyName := GetInfo('\CompanyName');
              FComments := GetInfo('\Comments');
              FFileDescription := GetInfo('\FileDescription');
              FFileVersion2 := GetInfo('\FileVersion');
              FInternalName := GetInfo('\InternalName');
              FLegalCopyright := GetInfo('\LegalCopyright');
              FLegalTrademarks := GetInfo('\LegalTrademarks');
              FOriginalFileName := GetInfo('\OriginalFilename');
              FProductName := GetInfo('\ProductName');
              FProductVersion2 := GetInfo('\ProductVersion');
              FPrivateBuild := GetInfo('\PrivateBuild');
              FSpecialBuild := GetInfo('\SpecialBuild');}
            end;
      finally
//
      end;
    end;
end;

procedure Register;
begin
  RegisterComponents('Sinea', [TTrayIconSinea]);
end;

end.


