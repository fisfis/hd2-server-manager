unit ServerUser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TEditUser = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Username: TEdit;
    Label2: TLabel;
    Password: TEdit;
    Ok: TButton;
    Cancel: TButton;
    GroupBox1: TGroupBox;
    Admin: TRadioButton;
    Manager: TRadioButton;
    procedure OkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditUser: TEditUser;

implementation

{$R *.DFM}

procedure TEditUser.OkClick(Sender: TObject);
begin
   if Trim (Username.Text)<>'' then ModalResult:=mrOK;
end;

procedure TEditUser.FormShow(Sender: TObject);
begin
   Username.SetFocus;
end;

end.
