object ServerProperties: TServerProperties
  Left = 816
  Top = 595
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Server properties'
  ClientHeight = 350
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 304
    Height = 344
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Server name:'
    end
    object Label2: TLabel
      Left = 232
      Top = 8
      Width = 55
      Height = 13
      Caption = 'Server port:'
    end
    object Bevel1: TBevel
      Left = 8
      Top = 303
      Width = 289
      Height = 9
      Shape = bsTopLine
    end
    object ServerName: TEdit
      Left = 8
      Top = 24
      Width = 217
      Height = 21
      TabOrder = 0
    end
    object ServerPort: TEdit
      Left = 232
      Top = 24
      Width = 65
      Height = 21
      TabOrder = 1
    end
    object AddUser: TButton
      Left = 144
      Top = 272
      Width = 75
      Height = 25
      Caption = 'Add'
      TabOrder = 2
      OnClick = AddUserClick
    end
    object RemoveUser: TButton
      Left = 222
      Top = 272
      Width = 75
      Height = 25
      Caption = 'Remove'
      TabOrder = 3
      OnClick = RemoveUserClick
    end
    object Ok: TButton
      Left = 144
      Top = 311
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 4
      OnClick = OkClick
    end
    object Cancel: TButton
      Left = 222
      Top = 311
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 5
    end
    object GrantedUsers: TListView
      Left = 8
      Top = 51
      Width = 289
      Height = 214
      Columns = <
        item
          AutoSize = True
          Caption = 'Granted users:'
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 6
      ViewStyle = vsReport
      OnSelectItem = GrantedUsersSelectItem
    end
  end
end
