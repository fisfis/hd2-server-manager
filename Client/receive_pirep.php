<?php
/*
 * You may not remove this header!
 *
 * -----------------------------------------------------------------------------------------------------------------------
 * 'receive_pirep.php' script code was made by Paulo Correia - FSAcars Team (�)2003
 * It's part of the FSAcars 2 for VA's package 
 *
 * This script assumes that the VA has PHP support and a MySQL database on it's site
 * A DDL file to create the following tables is provided: 'pilots', 'reports'
 *
 * The 'reports' table as the following columns:
 * pilot_id,date,time,callsign,origin_id,destination_id,registration,equipment,duration,fuel,distance,fsacars_rep_url
 * ----------------------------------------------------------------------------------------------------------------------
 *
 * FSACARS is distributed freely.
 *
 * Disclaimer
 * Although this product has been intensively tested, the authors accepts no responsibility for any damages caused by the use or misuse of this
 * software. This software is distributed 'as is' with no warranty expressed or implied. The authors will not be responsible for any losses incurred, either directly or indirectly, by the use of this software.
 * Use this software entirely at your own risk. If you do not agree to these terms then you must not use this software.
 *
 * Copyright
 * This software is a copyrighted program. AIBridge is a copyright of Jos� Oliveira. FSACARS coding is a copyright of Jos� Oliveira. FSACARS concept was idealized by Pedro Sousa.
 * It is protected by international copyright laws.
 */

/*
 * Change this constants according to your configuration  */
@define ("MYSQL_CONNECT_INCLUDE", "connect_db.php");		// MySQL database connection (a sample file is included)
@define ("REPORT_FILE_URL", "http://netairlines.ic.cz/reports/");				// URL where the complete FSAcars reports will be stored
@define ("REPORT_FILE_PATH", "/home/free/ic.cz/n/netairlines/root/www/fsacars/reports/");				// Folder where the complete FSAcars reports will be stored
@define ("ERROR_LOG_PATH", "/home/free/ic.cz/n/netairlines/root/www/fsacars/reports/NAL501/error.log");					// Folder and filename where the error log is located

/*
 * Error messages */
@define ("ERROR_OPENING_REPORT_FILE","Error opening report file");
@define ("ERROR_WRITING_REPORT_FILE","Error writing report file");
@define ("PILOT_NOT_FOUND","Pilot not found");
@define ("ERROR_IN_PILOT_QUERY","Pilot query error");
@define ("ERROR_INSERTING_PIREP","Error inserting report");



function CheckFSAcarsInfo() {   
	// Verify input
	if (!isset($_GET['pilot'])) { return 0; }
	
	// Request is not empty
	return 1;
}

function GetFSAcarsInfo() {
	/* ************************************************************************************************
	   @GetFSAcarsInfo
	   Receives inputs sent by FSAcars program and returns an array containing that information
	
	   Inputs: N/A
	   Outputs: string array 
	   ************************************************************************************************ */
	
	// DO NOT EDIT THIS FUNCTION - THIS FIELDS ARE SENT BY FSACARS
	$fsacars_pirep = array (
		"pilot" => $_GET['pilot'],
		"date" => $_GET['date'],
		"time" => $_GET['time'],
		"callsign" => $_GET['callsign'],
		"reg" => $_GET['reg'],
		"origin" => $_GET['origin'],
		"dest" => $_GET['dest'],
		"equipment" => $_GET['equipment'],
		"fuel" => $_GET['fuel'],
		"duration" => $_GET['duration'],
		"distance" => $_GET['distance'],
		"rep_url" => "Dummy",
		"more" => $_GET['more'],
		"fsacars_log" => $_GET['log']	// Get complete FSAcars log
	);
	
	/* DEBUG CODE - Write request to log file
	*/
	$fe = fopen (ERROR_LOG_PATH, "a");
	fwrite($fe, "[DEBUG ".date("d.m.y H:i:s")."] PILOT: ".$_GET['pilot']." DATE: ".$_GET['date']." TIME: ".$_GET['time']." CALLSIGN: ".$_GET['callsign']." REG: ".$_GET['reg']." ORIG: ".$_GET['origin']." DEST: ".$_GET['dest']." EQUIP: ".$_GET['equipment']." FUEL: ".$_GET['fuel']." DURATION: ".$_GET['duration']." DIST: ".$_GET['distance']." MORE: ".$_GET['more']." LOG: ".$_GET['log']."\n");
	fclose($fe);
	
	return $fsacars_pirep;
}

function SavePIREPFile($pirep_array) {
	/* ************************************************************************************************
	   @SavePIREPFile
	   Receives a string array with FSAcars pireps and creates or appends information to pirep file
	
	   Inputs: string array
	   Outputs: 1 sucess, 0 error
	   ************************************************************************************************ */
	
	/* Build report filename and URL */
	$filename=$pirep_array['pilot'].str_replace("/","",$pirep_array['date']).str_replace(":","",$pirep_array['time']).".txt";
	$pirep_array['rep_url']=REPORT_FILE_URL.$pirep_array['pilot']."/".$filename;
	
	/* Parse FsAcars log */
	$fsacars_log_lines_array = explode("*",$pirep_array['fsacars_log']);

	/* Create or Append FSAcars report file */
	$fp = fopen (REPORT_FILE_PATH.$pirep_array['pilot']."/".$filename, "a");

	if (!$fp) {
		/* Error opening file */
		$fe = fopen (ERROR_LOG_PATH, "a");
		fwrite($fe, "[ERROR ".date("d.m.y H:i:s")."] PILOT: ".$pirep_array['pilot']." - ".ERROR_OPENING_REPORT_FILE." - ".$filename."\n");
		fclose($fe);
		
		return 0;
	}

	/*
	* Write all log lines received from FSAcars */
	for($i=0;$i<count($fsacars_log_lines_array);$i++) {
    		if (!fwrite($fp, $fsacars_log_lines_array[$i] . "\n")) {
        		/* Error writing to file */
        		$fe = fopen (ERROR_LOG_PATH, "a");
			fwrite($fe, "[ERROR ".date("d.m.y H:i:s")."] PILOT".$pirep_array['pilot']." - ".ERROR_WRITING_REPORT_FILE." - ".$filename."\n");
			fclose($fe);
        		
        		return 0;
    		}
	}    

	/* Close file */
	fclose($fp);
	
	return 1;
}

function InsertReportIntoDB($pirep_array) {
	/* ************************************************************************************************
	   @InsertReportIntoDB
	   Receives a string array with FSAcars pireps and inserts summary into reports table
	
	   Inputs: string array
	   Outputs: 1 sucess, 0 error
	   ************************************************************************************************ */

	/* If this is the first chunk insert PIREP on database */
	if ($pirep_array['more']=="0") {
    		/* connect to database */
    		include(MYSQL_CONNECT_INCLUDE);

    		/*
    		 * Verify pilot identity (From VA Pilots table) */
    		$the_pilot = $pirep_array['pilot'];

    		$stmt = "select pilot_id from pilots where pilot_num='$the_pilot'";
    		$result = mysql_query($stmt);
    		
    		/* mysql error */
    		if (!$result) {
    			$fe = fopen (ERROR_LOG_PATH, "a");
			fwrite($fe, "[ERROR ".date("d.m.y H:i:s")."] ".ERROR_IN_PILOT_QUERY." - Pilot ".$pirep_array['pilot']." - ".mysql_error()." SQL: ".$stmt."\n");
			fclose($fe);
			
			return 0;
    		}
    		
    		if (mysql_num_rows($result) == 0) {
       			/* Pilot not found */
       			$fe = fopen (ERROR_LOG_PATH, "a");
			fwrite($fe, "[ERROR ".date("d.m.y H:i:s")."] ".PILOT_NOT_FOUND." - Pilot ".$pirep_array['pilot']."\n");
			fclose($fe);
			
			return 0;
    		} else {
          			/* Pilot found */
          			$pilot_id = mysql_result($result,0,"pilot_id");

          			/* Insert info on reports table */
          			$values = $pilot_id.",'".$pirep_array['date']."','".$pirep_array['time']."','".$pirep_array['callsign']."','".$pirep_array['origin']."','".$pirep_array['dest']."','".$pirep_array['reg']."','".$pirep_array['equipment']."','".$pirep_array['duration']."',".$pirep_array['fuel'].",".$pirep_array['distance'].",'".$pirep_array['rep_url']."'";
          			$stmt = "INSERT INTO reports (pilot_id,date,time,callsign,origin_id,destination_id,registration,equipment,duration,fuel,distance,fsacars_rep_url) VALUES ($values)";          			
          			$result = mysql_query($stmt);
    
          			if (!$result) {
    					$fe = fopen (ERROR_LOG_PATH, "a");
					fwrite($fe, "[ERROR ".date("d.m.y H:i:s")."] ".ERROR_INSERTING_PIREP." - Pilot ".$pirep_array['pilot']." - ".mysql_error()." SQL: ".$stmt."\n");
					fclose($fe);
			
					return 0;
    				}
                             
          			/* Close the database connection */
          			mysql_close();
    		}
    	}
    	
    	return 1;
}

function main() {
	/* ************************************************************************************************
	   @main
	   
	   Inputs: N/A
	   Outputs: "OK" sucess, "NOTOK" error
	   ************************************************************************************************ */
	$res = CheckFSAcarsInfo();
	if ($res == 0) {
		return "NOTOK";
	}
	
	$a = GetFSAcarsInfo();
	
	$res = SavePIREPFile(&$a);
	if ($res == 0) {
		return "NOTOK";
	}
	
	$res = InsertReportIntoDB($a);
	if ($res == 0) {
		return "NOTOK";
	}
	
	// Report sucessfully received
	return "OK";
}

/* receive_pirep.php return to FSACARS */
$out = main();
echo $out;

?>