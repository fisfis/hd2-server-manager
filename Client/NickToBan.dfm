object BanUser: TBanUser
  Left = 1711
  Top = 626
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Enter Nick'
  ClientHeight = 91
  ClientWidth = 263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 257
    Height = 85
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Nick name:'
    end
    object Nick: TEdit
      Left = 8
      Top = 24
      Width = 241
      Height = 21
      TabOrder = 0
    end
    object Button1: TButton
      Left = 94
      Top = 51
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 174
      Top = 51
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
    object Players: TButton
      Left = 230
      Top = 26
      Width = 17
      Height = 17
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = PlayersClick
    end
  end
end
