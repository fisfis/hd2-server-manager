unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, ScktComp, Common;

const
   ReplyTimeOut = 100;

type
  TMainForm = class(TForm)
    Image1: TImage;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ProviderHostname: TEdit;
    ProviderUsername: TEdit;
    ProviderPassword: TEdit;
    ConnectToProvider: TButton;
    StatusBar1: TStatusBar;
    GroupBox2: TGroupBox;
    PageControl1: TPageControl;
    ServerManagerSheet: TTabSheet;
    Label4: TLabel;
    Bevel1: TBevel;
    DedicatedServerPath: TEdit;
    Panel2: TPanel;
    EnableWatchdog: TCheckBox;
    WatchDogInterval: TEdit;
    WatchdogIntervalUpDown: TUpDown;
    Panel3: TPanel;
    EnableMessaging: TCheckBox;
    MessageInterval: TEdit;
    MessageIntervalUpDown: TUpDown;
    Panel4: TPanel;
    RebootServer: TCheckBox;
    RebootServerInterval: TEdit;
    RebootServerIntervalUpDown: TUpDown;
    RebootServerNow: TButton;
    SendMessagesToAllServers: TButton;
    AddNick: TButton;
    RemoveNick: TButton;
    SendForcedMessages: TCheckBox;
    UsersSheet: TTabSheet;
    UsersList: TListView;
    CreateUser: TButton;
    UserProperties: TButton;
    DeleteUser: TButton;
    ServersSheet: TTabSheet;
    ServersList: TListView;
    CreateServer: TButton;
    ServerProps: TButton;
    DeleteServer: TButton;
    RestartServer: TButton;
    StartServer: TButton;
    ConfigureServer: TButton;
    ForcedMessages: TMemo;
    MessagesForAllServers: TMemo;
    EditNick: TButton;
    Client: TClientSocket;
    UseForcedBanList: TCheckBox;
    GlobalBanList: TListView;
    ApplyChanges: TButton;
    Label5: TLabel;
    ServerPort: TEdit;
    Label6: TLabel;
    ServerIP: TEdit;

    // Status control
    procedure SetStatusText (Text: string);

    // Server connection contorl
    
    procedure ConnectToProviderClick(Sender: TObject);

    function SendCommandToServer (Command: Integer; Data: string): boolean;

    procedure ProcessReceivedBuffer;
    procedure ClientRead(Sender: TObject; Socket: TCustomWinSocket);

    procedure ClientError(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);

    // Update form from memory values
    procedure UpdateServerManagerInfo;
    procedure UpdateUsersInfo;
    procedure UpdateServersInfo;

    // sdfk

    procedure ConfigureServerClick(Sender: TObject);
    procedure ClientDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure FormCreate(Sender: TObject);
    procedure ClientConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure FormResize(Sender: TObject);
    procedure UsersListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ServersListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure GlobalBanListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ServersListDblClick(Sender: TObject);
    procedure StartServerClick(Sender: TObject);
    procedure RestartServerClick(Sender: TObject);
    procedure CreateServerClick(Sender: TObject);
    procedure ServerPropsClick(Sender: TObject);
    procedure DeleteServerClick(Sender: TObject);
    procedure CreateUserClick(Sender: TObject);
    procedure UserPropertiesClick(Sender: TObject);
    procedure DeleteUserClick(Sender: TObject);
    procedure UsersListDblClick(Sender: TObject);
    procedure AddNickClick(Sender: TObject);
    procedure EditNickClick(Sender: TObject);
    procedure RemoveNickClick(Sender: TObject);
    procedure GlobalBanListDblClick(Sender: TObject);
    procedure ApplyChangesClick(Sender: TObject);
    procedure Image1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    Resize:                     integer;

    ClientEncryptionKey:        array [0..15] of byte;

    ServerInfo:                 TServerInfo;
    ServerUsers:                array of TUserInfo;
    CurrentUserInfo:            TUserInfo;
    Servers:                    array of TServer;
    CurrentServer:              string;

    WaitingToServerReply:       boolean;
    ServerAccessEnabled:        boolean;

    BytesToReceive:             Word;
    ReceivedBytes:              Word;
    RecvBuffer:                 Pointer;
    RecvBufferPtr:              ^Byte;

    LoginFailed:                boolean;

  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses ServerSettings, ServerProps, SelectUser, ServerUser, NickToBan,
  CurrentPlayers;

{$R *.DFM}

procedure TMainForm.FormCreate(Sender: TObject);
var
   F:   TextFile;
   S:   String;
begin
   RecvBuffer:=nil;

   Resize:=2;

   GroupBox2.Visible:=false;
   Height:=207;

   ProviderHostname.Text:='';
   ProviderUsername.Text:='';
   ProviderPassword.Text:='';

   S:=ExtractFilePath(Application.ExeName)+'client.cfg';
   if FileExists(S) then
   begin
      AssignFile(F,'client.cfg');
      Reset(F);
      ReadLn(F,S);
      ProviderHostname.Text:=S;
      ReadLn(F,S);
      ProviderUsername.Text:=S;
      CloseFile(F);
   end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Client.Active:=false;
   Application.ProcessMessages;
end;


procedure TMainForm.FormResize(Sender: TObject);
begin
   if Resize>0 then
   begin
      Dec (Resize);
      exit;
   end;

   if Height=207 then Top:=Top+171
   else Top:=Top-171;
end;

procedure TMainForm.SetStatusText(Text: string);
begin
   if Text='' then
      StatusBar1.SimpleText:='Copyright (c) 1992-2011 Atom Software Studios supported by Mirco from PANICOS clan'
   else
      StatusBar1.SimpleText:=Text;

   Application.ProcessMessages;
end;

// *****************************************************************************
// *  Server connection and operations
// *****************************************************************************

procedure TMainForm.ConnectToProviderClick(Sender: TObject);
var
   MSG:    String;
   Port:   String;
begin
   if ConnectToProvider.Caption='Connect' then
   begin
      ConnectToProvider.Enabled:=false;
//      ConnectToProvider.Default:=false;

      SetStatusText ('Connecting to '+ProviderHostname.Text+' server');

      try
         WaitingToServerReply:=false;

         if Pos (':',ProviderHostname.Text)<>0 then
         begin
            Client.Host:=Copy (ProviderHostname.Text,1,Pos (':',ProviderHostname.Text)-1);
            Port:=Copy (ProviderHostname.Text,Pos (':',ProviderHostname.Text)+1,length (ProviderHostname.Text));
            if Port='' then Port:='2332';

            try
               Client.Port:=StrToInt (Port);
            except
               Port:='2332';
               Client.Port:=2332;
            end;

            if (Client.Port<1) or (Client.Port>65535) then
            begin
               Port:='2332';
               Client.Port:=2332;
            end;

            ProviderHostname.Text:=Client.Host+':'+Port;
         end else
            begin
               Client.Host:=ProviderHostname.Text;
               Client.Port:=2332;
            end;

         Client.Tag:=0;
         Client.Active:=true;

      except
         on E: Exception do
         begin
            ConnectToProvider.Enabled:=true;
            
            MSG:='Can''t connect to provider.'+#13#10#13#10+E.Message;
            MessageBox (Handle,PChar (MSG),
                               'Hidden and dangerous 2 - Server configurator',MB_OK or MB_ICONEXCLAMATION);
         end;
      end;
   end else
      begin
         Client.Active:=false;
         ConnectToProvider.Caption:='Connect';
      end;
end;

procedure TMainForm.ClientConnect(Sender: TObject; Socket: TCustomWinSocket);
var
   TimeOut: Cardinal;
begin
   GetMem (RecvBuffer,65535);
   BytesToReceive:=0;

   TimeOut:=GetTickCount;
   WaitingToServerReply:=true;
   while WaitingToServerReply and not (GetTickCount-TimeOut>ReplyTimeOut*1000) do
   begin
      SetStatusText ('Waiting to server reply... '+IntToStr (ReplyTimeOut-((GetTickCount-TimeOut) div 1000)));
      Application.ProcessMessages;
      if Application.Terminated then break;
      Sleep (0);
   end;

   if WaitingToServerReply then
   begin
      Socket.Close;
      FreeMem (RecvBuffer);
   end;

   ConnectToProvider.Enabled:=true;
   try
      if not LoginFailed then
         ConnectToProvider.SetFocus
      else
         ProviderPassword.SetFocus
   except
   end;
end;

procedure TMainForm.ClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
   WaitingToServerReply:=false;
   Client.Active:=false;

   GroupBox2.Visible:=false;
   Height:=207;

   ConnectToProvider.Caption:='Connect';
   ConnectToProvider.Enabled:=true;
   ConnectToProvider.Default:=true;

   SetStatusText ('');

   Screen.Cursor:=crDefault;
   GroupBox1.Enabled:=true;
   GroupBox2.Enabled:=true;

   if RecvBuffer<>nil then FreeMem (RecvBuffer);
   RecvBuffer:=nil;
end;

procedure TMainForm.ClientError(Sender: TObject; Socket: TCustomWinSocket;
  ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
   if ErrorEvent = eeConnect then
      MessageBox (Handle,'Can''t connect to specified server!',
                         'Hidden and dangerous 2 - Server configurator',MB_OK or MB_ICONEXCLAMATION)
   else
      MessageBox (Handle,'Connection to server lost!',
                         'Hidden and dangerous 2 - Server configurator',MB_OK or MB_ICONEXCLAMATION);

   ClientDisconnect (Sender,Socket);

   ErrorCode:=0;
end;


function TMainForm.SendCommandToServer (Command: Integer; Data: string): boolean;
var
   Buffer:      Pointer;
   BufferPtr:   ^Byte;
   BufferSize:  Cardinal;
   SendRV:      Integer;

   TimeOut:     Cardinal;

   a,b:         Integer;
begin
   GetMem (Buffer,65535);
   BufferPtr:=Buffer;
   Inc (BufferPtr,2);

   case Command of
      // login to server
      svcmd_Login:
         begin
            BufferPtr^:=svcmd_Login;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),ProviderUsername.Text);
            inc (BufferPtr,length (ProviderUsername.Text)+1);

            StrPCopy (PChar (BufferPtr),ProviderPassword.Text);
            inc (BufferPtr,length (ProviderPassword.Text)+1);

            Client.Tag:=1;
         end;

      svcmd_GetConfig:
         begin
            BufferPtr^:=svcmd_GetConfig;
            Inc (BufferPtr);
         end;

      svcmd_PutConfig:
         begin
            BufferPtr^:=svcmd_PutConfig;
            Inc (BufferPtr);

            CopyMemory (BufferPtr,@ServerInfo.WatchDogEnable,17);
            Inc (BufferPtr,17);

            BufferPtr^:=ForcedMessages.Lines.Count;
            Inc (BufferPtr);
            for a:=0 to ForcedMessages.Lines.Count-1 do
            begin
               StrPCopy (PChar (BufferPtr),ForcedMessages.Lines [a]);
               Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);
            end;

            BufferPtr^:=GlobalBanList.Items.Count;
            Inc (BufferPtr);
            for a:=0 to GlobalBanList.Items.Count-1 do
            begin
               StrPCopy (PChar (BufferPtr),GlobalBanList.Items [a].Caption);
               Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);
            end;
         end;

      svcmd_ListUsers:
         begin
            BufferPtr^:=svcmd_ListUsers;
            Inc (BufferPtr);
         end;

      svcmd_ListServers:
         begin
            BufferPtr^:=svcmd_ListServers;
            Inc (BufferPtr);
         end;

      svcmd_GetServerConfig:
         begin
            BufferPtr^:=svcmd_GetServerConfig;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;

      svcmd_PutServerConfig:
         begin
            BufferPtr^:=svcmd_PutServerConfig;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);

            CopyMemory (BufferPtr,@ServerSettingsForm.Server.watchdogenable,2);
            Inc (BufferPtr,2);
            StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.currentconfig);
            Inc (BufferPtr,length (ServerSettingsForm.Server.currentconfig)+1);
            BufferPtr^:=length (ServerSettingsForm.Server.configurations);
            Inc (BufferPtr);

            for a:=0 to length (ServerSettingsForm.Server.configurations)-1 do
            begin
               CopyMemory (BufferPtr,@ServerSettingsForm.Server.configurations [a],31);
               Inc (BufferPtr,31);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].name);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].name)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].domain);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].domain)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].style);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].style)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].sessionname);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].sessionname)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].dificulty);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].dificulty)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].password);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].password)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].adminpass);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].adminpass)+1);
               StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].clantag);
               Inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].clantag)+1);

               BufferPtr^:=length (ServerSettingsForm.Server.configurations [a].maps);
               Inc (BufferPtr);
               for b:=0 to length (ServerSettingsForm.Server.configurations [a].maps)-1 do
               begin
                  StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].maps [b]);
                  inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].maps [b])+1);
               end;

               BufferPtr^:=length (ServerSettingsForm.Server.configurations [a].messages);
               Inc (BufferPtr);
               for b:=0 to length (ServerSettingsForm.Server.configurations [a].messages)-1 do
               begin
                  StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].messages [b]);
                  inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].messages [b])+1);
               end;

               BufferPtr^:=length (ServerSettingsForm.Server.configurations [a].banlist);
               Inc (BufferPtr);
               for b:=0 to length (ServerSettingsForm.Server.configurations [a].banlist)-1 do
               begin
                  StrPCopy (PChar (BufferPtr),ServerSettingsForm.Server.configurations [a].banlist [b]);
                  inc (BufferPtr,length (ServerSettingsForm.Server.configurations [a].banlist [b])+1);
               end;
            end;
         end;

      svcmd_AddServer:
         begin
            BufferPtr^:=svcmd_AddServer;
            Inc (BufferPtr);

            for a:=0 to length (Servers)-1 do
               if Servers [a].name=Data then
               begin
                  CopyMemory (BufferPtr,@Servers [a],5);
                  Inc (BufferPtr,5);

                  StrPCopy (PChar (BufferPtr),Servers [a].name);
                  Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);

                  StrPCopy (PChar (BufferPtr),Servers [a].currentconfig);
                  Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);

                  BufferPtr^:=length (Servers [a].grantedusers);
                  Inc (BufferPtr);
                  for b:=0 to length (Servers [a].grantedusers)-1 do
                  begin
                     StrPCopy (PChar (BufferPtr),Servers [a].grantedusers [b]);
                     Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);
                  end;

                  break;
               end;
         end;

      svcmd_UpdateServer:
         begin
            BufferPtr^:=svcmd_UpdateServer;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);

            for a:=0 to length (Servers)-1 do
               if Servers [a].name=CurrentServer then
               begin
                  CopyMemory (BufferPtr,@Servers [a],2);
                  Inc (BufferPtr,2);

                  StrPCopy (PChar (BufferPtr),Servers [a].name);
                  Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);

                  BufferPtr^:=length (Servers [a].grantedusers);
                  Inc (BufferPtr);
                  for b:=0 to length (Servers [a].grantedusers)-1 do
                  begin
                     StrPCopy (PChar (BufferPtr),Servers [a].grantedusers [b]);
                     Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);
                  end;

                  break;
               end;
         end;

      svcmd_DeleteServer:
         begin
            BufferPtr^:=svcmd_DeleteServer;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,StrLen (PChar (BufferPtr))+1);
         end;

      svcmd_AddUser:
         begin
            BufferPtr^:=svcmd_AddUser;
            Inc (BufferPtr);

            BufferPtr^:=CurrentUserInfo.PrivilegeLevel;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),CurrentUserInfo.Username);
            Inc (BufferPtr,length (CurrentUserInfo.Username)+1);

            StrPCopy (PChar (BufferPtr),CurrentUserInfo.Password);
            Inc (BufferPtr,length (CurrentUserInfo.Password)+1);
         end;

      svcmd_UpdateUser:
         begin
            BufferPtr^:=svcmd_Updateuser;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);

            BufferPtr^:=CurrentUserInfo.PrivilegeLevel;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),CurrentUserInfo.Username);
            Inc (BufferPtr,length (CurrentUserInfo.Username)+1);

            StrPCopy (PChar (BufferPtr),CurrentUserInfo.Password);
            Inc (BufferPtr,length (CurrentUserInfo.Password)+1);
         end;

      svcmd_DeleteUser:
         begin
            BufferPtr^:=svcmd_DeleteUser;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;

      svcmd_StartServer:
         begin
            BufferPtr^:=svcmd_StartServer;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;

      svcmd_StopServer:
         begin
            BufferPtr^:=svcmd_StopServer;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;

      svcmd_RestartServer:
         begin
            BufferPtr^:=svcmd_RestartServer;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;

      svcmd_ListServerPlayers:
         begin
            BufferPtr^:=svcmd_ListServerPlayers;
            Inc (BufferPtr);

            StrPCopy (PChar (BufferPtr),Data);
            Inc (BufferPtr,length (Data)+1);
         end;
   end;

   BufferSize:=Integer (BufferPtr)-Integer(Buffer);

   // EncryptBuffer (Buffer,BufferSize);

   if BufferSize<>0 then
   begin
      BufferPtr:=Buffer;
      BufferPtr^:=Lo (BufferSize+2); Inc (BufferPtr);
      BufferPtr^:=Hi (BufferSize+2);
      SendRV:=Client.Socket.SendBuf (Buffer^,BufferSize+2);
      if SendRV<>(BufferSize+2) then
      begin
      end;
      FreeMem (Buffer);
   end;

   WaitingToServerReply:=true;
   TimeOut:=GetTickCount;

   GroupBox1.Enabled:=false;
   GroupBox2.Enabled:=false;

   Screen.Cursor:=crHourglass;

   while WaitingToServerReply and not (GetTickCount-TimeOut>ReplyTimeout*1000) do
   begin
      SetStatusText ('Waiting to server reply... '+IntToStr (ReplyTimeout-((GetTickCount-TimeOut) div 1000)));
      Application.ProcessMessages;
      if Application.Terminated then break;
      Sleep (0);
   end;

   if Client.Active then
   begin
      SetStatusText ('Ready');
      GroupBox1.Enabled:=true;
      GroupBox2.Enabled:=true;

      Screen.Cursor:=crDefault;

      Result:=not WaitingToServerReply;

   end else Result:=false;
end;

procedure TMainForm.ProcessReceivedBuffer;
var
   a,b:           Integer;
   i,j:           Integer;
   tmp:           string;
   item:          TTreeNode;

   F:             TextFile;
begin
   Inc (RecvBufferPtr);
   if not ((Client.Tag=2) and (WaitingToServerReply) and (RecvBufferPtr^=svcmd_PutServerStatus)) then
      WaitingToServerReply:=false;
   Dec (RecvBufferPtr);

   ServerAccessEnabled:=false;

   // if Client.Tag>0 then DecryptBuffer (Buffer,BufferSize);

   case Client.Tag of
      // Receive encryption tag
      0: if ReceivedBytes=18 then
         begin
            CopyMemory (@ClientEncryptionKey,RecvBufferPtr,16);
            // Send login informations
            LoginFailed:=false;
            if not SendCommandToServer (svcmd_Login,'') and not LoginFailed then
            begin
               Client.Close;
               MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                  'Hidden and dangerous 2 - Server configurator',
                                   MB_OK or MB_ICONEXCLAMATION);
            end;

            if not LoginFailed then
            begin
               tmp:=ExtractFilePath(Application.ExeName)+'client.cfg';
               AssignFile(F,'client.cfg');
               Rewrite(F);
               WriteLn(F,ProviderHostname.Text);
               WriteLn(F,ProviderUsername.Text);
               CloseFile(F);
            end else
               begin
                  SetStatusText('');
                  MessageBox (Handle,'Login to server failed!'+#13#10+'Bad usrename or password.',
                              'Hidden and dangerous 2 - Server configurator',
                              MB_OK or MB_ICONEXCLAMATION);

                  ClientDisconnect (self,Client.Socket);
                  LoginFailed:=true;
               end;

            ProviderPassword.Text:='';
         end;

      // Receive login information
      1: if RecvBufferPtr^=svcmd_Login then
         begin
            Client.Tag:=2;
            Inc (RecvBufferPtr);
            case RecvBufferPtr^ of
               0: begin
                     SetStatusText('');
                     MessageBox (Handle,'Login to server failed!'+#13#10+'Bad usrename or password.',
                                 'Hidden and dangerous 2 - Server configurator',
                                 MB_OK or MB_ICONEXCLAMATION);

                     ClientDisconnect (self,Client.Socket);
                     LoginFailed:=true;
                  end;

               1: begin
                     GroupBox2.Visible:=true;
                     Height:=550;

                     ConnectToProvider.Caption:='Disconnect';

                     ServerManagerSheet.TabVisible:=false;
                     UsersSheet.TabVisible:=false;

                     ServersList.Height:=250;
                     CreateServer.Visible:=false;
                     ServerProps.Visible:=false;
                     DeleteServer.Visible:=false;
                     PageControl1.ActivePage:=ServersSheet;

                     if not SendCommandToServer (svcmd_ListServers,'') then
                     begin
                        Client.Close;
                        MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                           'Hidden and dangerous 2 - Server configurator',
                                           MB_OK or MB_ICONEXCLAMATION);
                     end;
                  end;

               2: begin
                     GroupBox2.Visible:=true;
                     Height:=550;

                     ConnectToProvider.Caption:='Disconnect';

                     ServerManagerSheet.TabVisible:=true;
                     UsersSheet.TabVisible:=true;

                     ServersList.Height:=221;
                     CreateServer.Visible:=true;
                     ServerProps.Visible:=true;
                     DeleteServer.Visible:=true;
                     PageControl1.ActivePage:=ServerManagerSheet;

                     if not SendCommandToServer (svcmd_GetConfig,'') then
                     begin
                        Client.Close;
                        MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                           'Hidden and dangerous 2 - Server configurator',
                                           MB_OK or MB_ICONEXCLAMATION);
                     end else
                     if not SendCommandToServer (svcmd_ListUsers,'') then
                     begin
                        Client.Close;
                        MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                           'Hidden and dangerous 2 - Server configurator',
                                           MB_OK or MB_ICONEXCLAMATION);
                     end else
                     if not SendCommandToServer (svcmd_ListServers,'') then
                     begin
                        Client.Close;
                        MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                           'Hidden and dangerous 2 - Server configurator',
                                           MB_OK or MB_ICONEXCLAMATION);
                     end;
                  end;

            end;
         end;

      // Receive Data
      2: if RecvBufferPtr^=0 then
         begin
            Inc (RecvBufferPtr);
               case RecvBufferPtr^ of
                  // Receive server configuration
                  svcmd_GetConfig:
                  begin
                     Inc (RecvBufferPtr);
                     CopyMemory (@ServerInfo,RecvBufferPtr,19);
                     Inc (RecvBufferPtr,19);
                     ServerInfo.DedicatedServerPath:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (ServerInfo.DedicatedServerPath)+1);
                     ServerInfo.ServerIP:=StrPas (PChar (RecvBufferPtr));
                     Inc (RecvBufferPtr,length (ServerInfo.ServerIP)+1);
                     SetLength (ServerInfo.ForcedMessages,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for a:=0 to length (ServerInfo.ForcedMessages)-1 do
                     begin
                        ServerInfo.ForcedMessages [a]:=StrPas (PChar (RecvBufferPtr));
                        inc (RecvBufferPtr,length (ServerInfo.ForcedMessages [a])+1);
                     end;
                     SetLength (ServerInfo.ForcedBanList,RecvBufferPtr^);
                     Inc (RecvBufferPtr);
                     for a:=0 to length (ServerInfo.ForcedBanList)-1 do
                     begin
                        ServerInfo.ForcedBanList [a]:=StrPas (PChar (RecvBufferPtr));
                        inc (RecvBufferPtr,length (ServerInfo.ForcedBanList [a])+1);
                     end;

                     UpdateServerManagerInfo;
                  end;

                  // Receive server users
                  svcmd_ListUsers:
                  begin
                     Inc (RecvBufferPtr);
                     SetLength (ServerUsers,RecvBufferPtr^);
                     Inc (RecvBufferPtr);

                     for a:=0 to length (ServerUsers)-1 do
                     begin
                        ServerUsers [a].PrivilegeLevel:=RecvBufferPtr^;
                        Inc (RecvBufferPtr);
                        ServerUsers [a].Username:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (ServerUsers [a].Username)+1);
                        ServerUsers [a].Password:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (ServerUsers [a].Password)+1);
                     end;

                     UpdateUsersInfo;
                  end;

                  // Receive list of HD2 servers
                  svcmd_ListServers:
                  begin
                     Inc (RecvBufferPtr);
                     SetLength (Servers,RecvBufferPtr^);
                     Inc (RecvBufferPtr);

                     for a:=0 to length (Servers)-1 do
                     begin
                        CopyMemory (@Servers [a],RecvBufferPtr,3);
                        Inc (RecvBufferPtr,3);
                        Servers [a].name:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (Servers [a].name)+1);
                        SetLength (Servers [a].GrantedUsers,RecvBufferPtr^);
                        Inc (RecvBufferPtr);
                        for b:=0 to length (Servers [a].GrantedUsers)-1 do
                        begin
                           Servers [a].GrantedUsers [b]:=StrPas (PChar (RecvBufferPtr));
                           inc (RecvBufferPtr,length (Servers [a].GrantedUsers [b])+1);
                        end;
                     end;

                     UpdateServersInfo;
                  end;

                  // Receive configuration of HD2 server
                  svcmd_GetServerConfig:
                  begin
                     Inc (RecvBufferPtr);
                     if RecvBufferPtr^<>0 then ServerAccessEnabled:=true;
                     Inc (RecvBufferPtr);
                     if ServerAccessEnabled then
                     begin
                        CopyMemory (@ServerSettingsForm.Server.watchdogenable,RecvBufferPtr,2);
                        Inc (RecvBufferPtr,2);
                        ServerSettingsForm.Server.currentconfig:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,length (ServerSettingsForm.Server.currentconfig)+1);
                        SetLength (ServerSettingsForm.Server.configurations,RecvBufferPtr^);
                        Inc (RecvBufferPtr);

                        for a:=0 to length (ServerSettingsForm.Server.configurations)-1 do
                        begin
                           CopyMemory (@ServerSettingsForm.Server.configurations [a],RecvBufferPtr,31);
                           Inc (RecvBufferPtr,31);

                           ServerSettingsForm.Server.configurations [a].name:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].name)+1);
                           ServerSettingsForm.Server.configurations [a].domain:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].domain)+1);
                           ServerSettingsForm.Server.configurations [a].style:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].style)+1);
                           ServerSettingsForm.Server.configurations [a].sessionname:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].sessionname)+1);
                           ServerSettingsForm.Server.configurations [a].dificulty:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].dificulty)+1);
                           ServerSettingsForm.Server.configurations [a].password:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].password)+1);
                           ServerSettingsForm.Server.configurations [a].adminpass:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].adminpass)+1);
                           ServerSettingsForm.Server.configurations [a].clantag:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].clantag)+1);

                           SetLength (ServerSettingsForm.Server.configurations [a].maps,RecvBufferPtr^);
                           Inc (RecvBufferPtr);
                           for b:=0 to length (ServerSettingsForm.Server.configurations [a].maps)-1 do
                           begin
                              ServerSettingsForm.Server.configurations [a].maps [b]:=StrPas (PChar (RecvBufferPtr));
                              Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].maps [b])+1);
                           end;

                           SetLength (ServerSettingsForm.Server.configurations [a].messages,RecvBufferPtr^);
                           Inc (RecvBufferPtr);
                           for b:=0 to length (ServerSettingsForm.Server.configurations [a].messages)-1 do
                           begin
                              ServerSettingsForm.Server.configurations [a].messages [b]:=StrPas (PChar (RecvBufferPtr));
                              Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].messages [b])+1);
                           end;

                           SetLength (ServerSettingsForm.Server.configurations [a].banlist,RecvBufferPtr^);
                           Inc (RecvBufferPtr);
                           for b:=0 to length (ServerSettingsForm.Server.configurations [a].banlist)-1 do
                           begin
                              ServerSettingsForm.Server.configurations [a].banlist [b]:=StrPas (PChar (RecvBufferPtr));
                              Inc (RecvBufferPtr,length (ServerSettingsForm.Server.configurations [a].banlist [b])+1);
                           end;

                        end;

                        SetLength (ServerSettingsForm.Maps,RecvBufferPtr^);
                        Inc (RecvBufferPtr);
                        for b:=0 to length (ServerSettingsForm.Maps)-1 do
                        begin
                           ServerSettingsForm.Maps [b]:=StrPas (PChar (RecvBufferPtr));
                           Inc (RecvBufferPtr,length (ServerSettingsForm.Maps [b])+1);
                        end;
                     end;
                  end;

                  svcmd_PutServerConfig:
                     begin
                        Inc (RecvBufferPtr);
                        tmp:=StrPas (PChar (RecvBufferPtr));
                        Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
                        if (RecvBufferPtr^=255) then
                        begin
                           if MessageBox (Handle,PChar ('Server '+tmp+' needs restart to apply changes.'+#13#10+'Do you want to restart server now?'),
                                                 'Hidden and dangerous 2 - Server configurator',MB_YESNO or MB_ICONASTERISK)=IDYES then
                           RestartServerClick (self);
                        end;
                     end;

                  svcmd_PutServerStatus:
                     begin
                        Inc (RecvBufferPtr);
                        for a:=0 to ServersList.Items.Count-1 do
                           if ServersList.Items [a].Caption=StrPas (PChar (RecvBufferPtr)) then
                           begin
                              Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);
                              if ServersList.Items [a].SubItems.Count=4 then
                                 case RecvBufferPtr^ of
                                    0: ServersList.Items [a].SubItems [3]:='stopped';
                                    1: ServersList.Items [a].SubItems [3]:='starting';
                                    2: ServersList.Items [a].SubItems [3]:='running';
                                    3: ServersList.Items [a].SubItems [3]:='error';
                                    4: ServersList.Items [a].SubItems [3]:='wd wait';
                                 end;
                                 if ServersList.Selected<>nil then
                                    ServersListSelectItem (self,ServersList.Selected,true);
                              Break;
                           end;

                     end;

                  // Receive current statistics from server

                  // Receive player statistics from server

                  // Receive on-line players from servers
                  svcmd_ListServerPlayers:
                     begin
                        Inc (RecvBufferPtr);

                        CurrentPlayersForm.CurrentPlayers.Items.Clear;

                        i:=RecvBufferPtr^; // pocet serveru
                        Inc (RecvBufferPtr);

                        for a:=1 to i do
                        begin
                           tmp:=StrPas (PChar (RecvBufferPtr)); //nazev serveru
                           Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

                           j:=RecvBufferPtr^; // pocet hracu nebo neopravneny pristup (255)
                           Inc (RecvBufferPtr);
                           if j=255 then j:=0;

                           if j>0 then
                              item:=CurrentPlayersForm.CurrentPlayers.Items.Add(nil,tmp+' ['+IntToStr(j)+']')
                           else
                              item:=nil;

                           for b:=1 to j do
                           begin
                              tmp:=StrPas (PChar (RecvBufferPtr)); //jmeno hrace
                              Inc (RecvBufferPtr,StrLen (PChar (RecvBufferPtr))+1);

                              if item<>nil then CurrentPlayersForm.CurrentPlayers.Items.AddChild(item,tmp);
                           end;

                           if item<>nil then item.Expand(true);
                        end;

                     end;
               end;

         end else
            MessageBox (Handle,'You have not perissions to perform this action.',
                               'Hidden and dangerous 2 - Server configurator',MB_OK or MB_ICONEXCLAMATION);

   end;
end;

procedure TMainForm.ClientRead(Sender: TObject; Socket: TCustomWinSocket);
var
   BufferSize:    Word;
   Ctrl:          ^Word;
begin
   if BytesToReceive=0 then
   begin
      RecvBufferPtr:=RecvBuffer;
      ZeroMemory (RecvBuffer,65535);
   end;

   BufferSize:=Socket.ReceiveLength;
   Socket.ReceiveBuf (RecvBufferPtr^,BufferSize);

   if BytesToReceive=0 then
   begin
      Ctrl:=RecvBuffer;
      BytesToReceive:=Ctrl^;
   end;

   Inc (RecvBufferPtr,BufferSize);

   if Integer (RecvBufferPtr)-Integer (RecvBuffer)=BytesToReceive then
   begin
      ReceivedBytes:=BytesToReceive;
      BytesToReceive:=0;
      RecvBufferPtr:=RecvBuffer;
      Inc (RecvBufferPtr,2);
      ProcessReceivedBuffer;
   end;
end;

// *****************************************************************************
// * Update display values from memory values
// *****************************************************************************

procedure TMainForm.UpdateServerManagerInfo;
var
   a:   Integer;
   LI:  TListItem;
begin
   DedicatedServerPath.Text:=ServerInfo.DedicatedServerPath;
   ServerIP.Text:=ServerInfo.ServerIP;
   ServerPort.Text:=IntToStr (ServerInfo.ServerPort);
   EnableWatchdog.Checked:=ServerInfo.WatchDogEnable;
   WatchdogIntervalUpDown.Position:=ServerInfo.WatchDogInterval;
   EnableMessaging.Checked:=ServerInfo.MessagingEnable;
   MessageIntervalUpDown.Position:=ServerInfo.MessagingInterval;
   RebootServer.Checked:=ServerInfo.RebootEnable;
   RebootServerIntervalUpDown.Position:=ServerInfo.RebootInterval;
   SendForcedMessages.Checked:=ServerInfo.ForcedMessagesEnable;
   UseForcedBanList.Checked:=ServerInfo.ForcedBanListEnable;
   ForcedMessages.Clear;
   for a:=0 to length (ServerInfo.ForcedMessages)-1 do
      ForcedMessages.Lines.Add (ServerInfo.ForcedMessages [a]);
   GlobalBanList.Items.Clear;
   for a:=0 to length (ServerInfo.ForcedBanList)-1 do
   begin
      LI:=GlobalBanList.Items.Add;
      LI.Caption:=(ServerInfo.ForcedBanList [a]);
   end;

   EditNick.Enabled:=false;
   RemoveNick.Enabled:=false;
end;

procedure TMainForm.UpdateUsersInfo;
var
   a:   Integer;
   LI:  TListItem;
begin
   UsersList.Items.BeginUpdate;
   UsersList.Items.Clear;

   for a:=0 to length (ServerUsers)-1 do
   begin
      LI:=UsersList.Items.Add;

      LI.Caption:=ServerUsers [a].Username;
      LI.SubItems.Add (ServerUsers [a].Password);
      case ServerUsers [a].PrivilegeLevel of
         1: LI.SubItems.Add ('Server manager');
         2: LI.SubItems.Add ('Administrator');
      end;
   end;

   UsersList.Items.EndUpdate;

   UserProperties.Enabled:=false;
   DeleteUser.Enabled:=false;
end;

procedure TMainForm.UpdateServersInfo;
var
   a,b:   Integer;
   tmp:   string;
   LI:    TListItem;
begin
   ServersList.Items.BeginUpdate;
   ServersList.Items.Clear;

   for a:=0 to length (Servers)-1 do
   begin
      LI:=ServersList.Items.Add;

      LI.Caption:=Servers [a].name;
      LI.Checked:=false;
      tmp:='';
      for b:=0 to length (Servers [a].GrantedUsers)-1 do
      begin
         if LowerCase (Servers [a].GrantedUsers [b])=LowerCase (ProviderUsername.Text) then
            LI.Checked:=true;
         tmp:=tmp+Servers [a].GrantedUsers [b]+', ';
      end;
      
      Delete (tmp,length (tmp)-1,2);
      LI.SubItems.Add (tmp);
      LI.SubItems.Add (IntToStr (Servers [a].port));
      if Servers [a].running then
         LI.SubItems.Add ('Yes')
      else
         LI.SubItems.Add ('No');

      LI.SubItems.Add ('none');
   end;

   ServersList.Items.EndUpdate;

   ServerProps.Enabled:=false;
   DeleteServer.Enabled:=false;
   ConfigureServer.Enabled:=false;
   StartServer.Enabled:=false;
   RestartServer.Enabled:=false;
end;

// *****************************************************************************
// *
// *****************************************************************************

procedure TMainForm.AddNickClick(Sender: TObject);
begin
   BanUser.Nick.Text:='';

   BanUser.Players.Enabled:=false;
   if SendCommandToServer (svcmd_ListServerPlayers,'') then
      BanUser.Players.Enabled:=true;

   if BanUser.ShowModal=mrOK then
   begin
      SetLength (ServerInfo.ForcedBanList,length (ServerInfo.ForcedBanList)+1);
      ServerInfo.ForcedBanList [length (ServerInfo.ForcedBanList)-1]:=BanUser.Nick.Text;
      GlobalBanList.Items.Add.Selected:=true;
      GlobalBanList.Selected.Caption:=BanUser.Nick.Text;
   end;
end;

procedure TMainForm.EditNickClick(Sender: TObject);
begin
   BanUser.Nick.Text:=GlobalBanList.Selected.Caption;

   BanUser.Players.Enabled:=false;
   if SendCommandToServer (svcmd_ListServerPlayers,'') then
      BanUser.Players.Enabled:=true;

   if BanUser.ShowModal=mrOK then
   begin
      GlobalBanList.Selected.Caption:=BanUser.Nick.Text;
      ServerInfo.ForcedBanList [GlobalBanList.Selected.Index]:=BanUser.Nick.Text;
   end;
end;

procedure TMainForm.RemoveNickClick(Sender: TObject);
var
   a:   Integer;
begin
   if MessageBox (Handle,'Are you sure to delete player from ban listM?'+#13#10+'NOTE: Remove user access from servers manually.',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
   begin
      if GlobalBanList.Selected.Index<length (ServerInfo.ForcedBanList)-1 then
         for a:=GlobalBanList.Selected.Index to length (ServerInfo.ForcedBanList)-2 do
            ServerInfo.ForcedBanList [a]:=ServerInfo.ForcedBanList [a+1];
      SetLength (ServerInfo.ForcedBanList,length (ServerInfo.ForcedBanList)-1);
      GlobalBanList.Selected.Delete;
   end;
end;

procedure TMainForm.GlobalBanListDblClick(Sender: TObject);
begin
   if EditNick.Enabled then EditNickClick (Sender);
end;

procedure TMainForm.CreateUserClick(Sender: TObject);
var
   LI:  TListItem;
begin
   EditUser.Username.Text:='';
   EditUser.Password.Text:='';
   EditUser.Manager.Checked:=true;
   if EditUser.ShowModal=mrOK then
   begin
      SetLength (ServerUsers,length (ServerUsers)+1);

      ServerUsers [length (ServerUsers)-1].Username:=EditUser.Username.Text;
      ServerUsers [length (ServerUsers)-1].Password:=EditUser.Password.Text;

      LI:=UsersList.Items.Add;
      LI.Selected:=true;
      LI.Caption:=EditUser.Username.Text;
      LI.SubItems.Add (EditUser.Password.Text);
      if EditUser.Manager.Checked then
      begin
         ServerUsers [length (ServerUsers)-1].PrivilegeLevel:=1;
         LI.SubItems.Add ('Server manager')
      end else
         begin
            ServerUsers [length (ServerUsers)-1].PrivilegeLevel:=2;
            LI.SubItems.Add ('Administrator');
         end;

      CurrentUserInfo:=ServerUsers [length (ServerUsers)-1];
      if not SendCommandToServer (svcmd_AddUser,'') then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;
   end;
end;

procedure TMainForm.UserPropertiesClick(Sender: TObject);
var
   tmp:    string;
begin
   EditUser.Username.Text:=UsersList.Selected.Caption;
   EditUser.Password.Text:=UsersList.Selected.SubItems [0];
   if ServerUsers [UsersList.Selected.Index].PrivilegeLevel=2 then
      EditUser.Admin.Checked:=true else EditUser.Manager.Checked:=true;

   if EditUser.ShowModal=mrOK then
   begin
      tmp:=ServerUsers [UsersList.Selected.Index].Username;
      ServerUsers [UsersList.Selected.Index].Username:=EditUser.Username.Text;
      ServerUsers [UsersList.Selected.Index].Password:=EditUser.Password.Text;

      UsersList.Selected.Selected:=true;
      UsersList.Selected.Caption:=EditUser.Username.Text;
      UsersList.Selected.SubItems.Clear;
      UsersList.Selected.SubItems.Add (EditUser.Password.Text);
      if EditUser.Manager.Checked then
      begin
         ServerUsers [UsersList.Selected.Index].PrivilegeLevel:=1;
         UsersList.Selected.SubItems.Add ('Server manager')
      end else
         begin
            ServerUsers [UsersList.Selected.Index].PrivilegeLevel:=2;
            UsersList.Selected.SubItems.Add ('Administrator');
         end;

      CurrentUserInfo:=ServerUsers [UsersList.Selected.Index];
      if not SendCommandToServer (svcmd_UpdateUser,tmp) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;
   end;
end;

procedure TMainForm.DeleteUserClick(Sender: TObject);
var
   a:   integer;
begin
   if MessageBox (Handle,'Are you sure to delete selected user?'+#13#10+'NOTE: Remove user access from servers manually.',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
   begin
      if not SendCommandToServer (svcmd_DeleteUser,UsersList.Selected.Caption) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;

      if UsersList.Selected.Index<length (ServerUsers)-1 then
         for a:=UsersList.Selected.Index to length (ServerUsers)-2 do
            ServerUsers [a]:=ServerUsers [a+1];
      SetLength (ServerUsers,length (ServerUsers)-1);
      UsersList.Selected.Delete;
   end;
end;

procedure TMainForm.UsersListDblClick(Sender: TObject);
begin
   if UserProperties.Enabled then UserPropertiesClick (Sender);
end;


procedure TMainForm.CreateServerClick(Sender: TObject);
var
   LI:  TListItem;
   Tmp: string;
   a,b: integer;
begin
   ServerProperties.ServerName.Text:='';
   ServerProperties.ServerPort.Text:='11001';
   ServerProperties.GrantedUsers.Items.Clear;

   GrantUser.UserList.Items.Clear;
   for a:=0 to UsersList.Items.Count-1 do
      GrantUser.UserList.Items.Add (UsersList.Items [a].Caption);

   if (ServerProperties.ShowModal=mrOk) then
   begin
      b:=length (Servers);
      SetLength (Servers,b+1);

      Servers [b].port:=StrToInt (ServerProperties.ServerPort.Text);
      Servers [b].running:=false;
      Servers [b].watchdogenable:=false;
      Servers [b].messagesenable:=false;
      Servers [b].name:=ServerProperties.ServerName.Text;
      Servers [b].currentconfig:='';
      Servers [b].configurations:=nil;

      LI:=ServersList.Items.Add;
      LI.Caption:=ServerProperties.ServerName.Text;
      LI.Checked:=false;
      tmp:='';
      SetLength (Servers [b].grantedusers,ServerProperties.GrantedUsers.Items.Count);
      for a:=0 to ServerProperties.GrantedUsers.Items.Count-1 do
      begin
         Servers [b].grantedusers [a]:=ServerProperties.GrantedUsers.Items [a].Caption;
         tmp:=tmp+ServerProperties.GrantedUsers.Items [a].Caption+', ';
         if LowerCase (ProviderUsername.Text)=LowerCase (ServerProperties.GrantedUsers.Items [a].Caption) then LI.Checked:=true;
      end;

      Delete (tmp,length (tmp)-1,2);
      LI.SubItems.Add (Tmp);

      LI.SubItems.Add (ServerProperties.ServerPort.Text);

      LI.SubItems.Add ('No');

      LI.SubItems.Add ('stopped');

      LI.Selected:=true;
      ServersListSelectItem (self,LI,true);

      if not SendCommandToServer (svcmd_AddServer,ServerProperties.ServerName.Text) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;
   end;
end;

procedure TMainForm.ServerPropsClick(Sender: TObject);
var
   Tmp,tmp1: string;
   a,b: integer;
   LI:  TListItem;
   tmpbool: boolean;
begin
   ServerProperties.ServerName.Text:=ServersList.Selected.Caption;
   tmp:=ServersList.Selected.SubItems [0];
   ServerProperties.GrantedUsers.Items.Clear;
   while length (tmp)>0 do
      if Pos (',',tmp)<>0 then
      begin
         ServerProperties.GrantedUsers.Items.Add.Caption:=Trim (Copy (tmp,1,Pos (',',tmp)-1));
         Delete (tmp,1,Pos (',',tmp)+1);
      end else
         begin
            ServerProperties.GrantedUsers.Items.Add.Caption:=Trim (tmp);
            tmp:='';
         end;
   ServerProperties.ServerPort.Text:=ServersList.Selected.SubItems [1];

   GrantUser.UserList.Items.Clear;
   for a:=0 to UsersList.Items.Count-1 do
      GrantUser.UserList.Items.Add (UsersList.Items [a].Caption);

   if ServerProperties.ShowModal=mrOk then
   begin
      b:=ServersList.Selected.Index;
      tmp1:=Servers [b].name;
      if Servers [b].port<>StrToInt (ServerProperties.ServerPort.Text) then tmpbool:=true
      else tmpbool:=false;
      Servers [b].port:=StrToInt (ServerProperties.ServerPort.Text);
      Servers [b].name:=ServerProperties.ServerName.Text;
      CurrentServer:=ServerProperties.ServerName.Text;

      LI:=ServersList.Selected;
      LI.SubItems.Clear;
      LI.Caption:=ServerProperties.ServerName.Text;
      LI.Checked:=false;
      tmp:='';
      SetLength (Servers [b].grantedusers,ServerProperties.GrantedUsers.Items.Count);
      for a:=0 to ServerProperties.GrantedUsers.Items.Count-1 do
      begin
         Servers [b].grantedusers [a]:=ServerProperties.GrantedUsers.Items [a].Caption;
         tmp:=tmp+ServerProperties.GrantedUsers.Items [a].Caption+', ';
         if LowerCase (ProviderUsername.Text)=LowerCase (ServerProperties.GrantedUsers.Items [a].Caption) then LI.Checked:=true;
      end;

      Delete (tmp,length (tmp)-1,2);
      LI.SubItems.Add (Tmp);

      LI.SubItems.Add (ServerProperties.ServerPort.Text);


      if Servers [b].running then LI.SubItems.Add ('Yes') else LI.SubItems.Add ('No');

      LI.SubItems.Add ('none');

      ServersListSelectItem (self,LI,true);

      if not SendCommandToServer (svcmd_UpdateServer,tmp1) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;

      if tmpbool and Servers [b].running then 
         if MessageBox (Handle,PChar ('Server '+Servers [b].name+' needs restart to apply changes.'+#13#10+'Do you want to restart server now?'),
                               'Hidden and dangerous 2 - Server configurator',MB_YESNO or MB_ICONASTERISK)=IDYES then
            RestartServerClick (self);

   end;

end;

procedure TMainForm.DeleteServerClick(Sender: TObject);
var
   a: Integer;
begin
   if MessageBox (Handle,'Configuration of server will be lost.'+#13#10+'Are you sure to delete selected server?',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
   begin
      if not SendCommandToServer (svcmd_DeleteServer,ServersList.Selected.Caption) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
      end;

      if ServersList.Selected.Index<length (Servers)-1 then
         for a:=ServersList.Selected.Index to length (Servers)-2 do
            Servers [a]:=Servers [a+1];
      SetLength (Servers,length (Servers)-1);
      ServersList.Selected.Delete;
   end;
end;

procedure TMainForm.ConfigureServerClick(Sender: TObject);
begin
   if SendCommandToServer (svcmd_GetServerConfig,ServersList.Selected.Caption) then
   begin
      if ServerAccessEnabled then
      begin
         SendCommandToServer (svcmd_ListServerPlayers,'');
         if ServerSettingsForm.ShowModal=mrOk then
            if not SendCommandToServer (svcmd_PutServerConfig,ServersList.Selected.Caption) then
            begin
               Client.Close;
               MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                                  'Hidden and dangerous 2 - Server configurator',
                                  MB_OK or MB_ICONEXCLAMATION);
            end;

      end else
         MessageBox (Handle,'Access denied!'+#10#13+'You Are not permited to configure this serever',
                            'Hidden and dangerous 2 - Server configurator',MB_OK or MB_ICONEXCLAMATION);
   end else
      begin
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                            MB_OK or MB_ICONEXCLAMATION);
         Client.Close;
      end;
end;

procedure TMainForm.StartServerClick(Sender: TObject);
begin
   if StartServer.Caption='Start server' then
   begin
      StartServer.Caption:='Stop server';
      RestartServer.Enabled:=true;
      ServersList.Selected.SubItems [2]:='Yes';
      Servers [ServersList.Selected.Index].running:=true;
      if not SendCommandToServer (svcmd_StartServer,ServersList.Selected.Caption) then
      begin
         Client.Close;
         MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                            'Hidden and dangerous 2 - Server configurator',
                             MB_OK or MB_ICONEXCLAMATION);
      end;
   end else
      begin
         StartServer.Caption:='Start server';
         RestartServer.Enabled:=false;
         ServersList.Selected.SubItems [2]:='No';
         Servers [ServersList.Selected.Index].running:=false;
         if not SendCommandToServer (svcmd_StopServer,ServersList.Selected.Caption) then
         begin
            Client.Close;
            MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                               'Hidden and dangerous 2 - Server configurator',
                                MB_OK or MB_ICONEXCLAMATION);
         end;
      end;
end;

procedure TMainForm.RestartServerClick(Sender: TObject);
begin
   StartServer.Caption:='Stop server';
   RestartServer.Enabled:=true;
   ServersList.Selected.SubItems [2]:='Yes';
   Servers [ServersList.Selected.Index].running:=true;
   if not SendCommandToServer (svcmd_RestartServer,ServersList.Selected.Caption) then
   begin
      Client.Close;
      MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_OK or MB_ICONEXCLAMATION);
   end;
end;

procedure TMainForm.ServersListDblClick(Sender: TObject);
begin
   if ConfigureServer.Enabled then ConfigureServerClick (Sender);
end;


procedure TMainForm.UsersListSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
   if Selected then
   begin
      UserProperties.Enabled:=true;
      DeleteUser.Enabled:=true;
   end else
      begin
         UserProperties.Enabled:=false;
         DeleteUser.Enabled:=false;
      end;
end;

procedure TMainForm.ServersListSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
   if selected then
   begin
      ServerProps.Enabled:=true;
      DeleteServer.Enabled:=true;
      if Item.Checked then
      begin
         ConfigureServer.Enabled:=true;
         StartServer.Enabled:=true;
         if Item.SubItems [2]='Yes' then
         begin
            StartServer.Caption:='Stop server';
            RestartServer.Enabled:=true
         end else
            begin
               StartServer.Caption:='Start server';
               RestartServer.Enabled:=false;
            end;
      end else
         begin
            ConfigureServer.Enabled:=false;
            StartServer.Enabled:=false;
            RestartServer.Enabled:=false;
         end;

      if Item.SubItems [3]='error' then
      begin
         StartServer.Caption:='Stop server';
         RestartServer.Enabled:=false;
      end;
      
   end else
      begin
         ServerProps.Enabled:=false;
         DeleteServer.Enabled:=false;
         ConfigureServer.Enabled:=false;
         StartServer.Enabled:=false;
         RestartServer.Enabled:=false;
      end;
end;

procedure TMainForm.GlobalBanListSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then
   begin
      EditNick.Enabled:=true;
      RemoveNick.Enabled:=true;
   end else
      begin
         EditNick.Enabled:=false;
         RemoveNick.Enabled:=false;
      end;
end;


procedure TMainForm.ApplyChangesClick(Sender: TObject);
begin
   ServerInfo.WatchDogEnable:=EnableWatchDog.Checked;
   ServerInfo.WatchDogInterval:=WatchdogIntervalUpDown.Position;
   ServerInfo.MessagingEnable:=EnableMessaging.Checked;
   ServerInfo.MessagingInterval:=MessageIntervalUpDown.Position;
   ServerInfo.RebootEnable:=RebootServer.Enabled;
   ServerInfo.RebootInterval:=RebootServerIntervalUpDown.Position;
   ServerInfo.ForcedMessagesEnable:=SendForcedMessages.Checked;
   ServerInfo.ForcedBanListEnable:=UseForcedBanList.Checked;

   if not SendCommandToServer (svcmd_PutConfig,'') then
   begin
      Client.Close;
      MessageBox (Handle,'Data transfer error!'+#13#10+'Please reconnect to server and try again.',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_OK or MB_ICONEXCLAMATION);
   end;
end;

procedure TMainForm.Image1DblClick(Sender: TObject);
begin
   Client.Active:=false;
   Screen.Cursor:=crDefault;
   GroupBox1.Enabled:=true;
   GroupBox2.Enabled:=true;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
   if (ConnectToProvider.Caption='Connect') and (ProviderHostname.Text<>'') then
      ProviderPassword.SetFocus;
end;

end.
