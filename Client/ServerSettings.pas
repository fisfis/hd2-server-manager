unit ServerSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, Common;

type
  TServerSettingsForm = class(TForm)
    Panel1: TPanel;
    ServerSettings: TPageControl;
    TabSheet4: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    SessionName: TEdit;
    GroupBox3: TGroupBox;
    DomainLocal: TRadioButton;
    DomainInternet: TRadioButton;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    MaxPlayersTxt: TLabel;
    PointLimitTxt: TLabel;
    RoundTimeLimitTxt: TLabel;
    RoundCOuntTxt: TLabel;
    RespawnTimeTxt: TLabel;
    SpawnProtectionTxt: TLabel;
    WarmUpTxt: TLabel;
    InverseDamageTxt: TLabel;
    MaxPlayers: TTrackBar;
    PointLimit: TTrackBar;
    RoundTimeLimit: TTrackBar;
    RoundCount: TTrackBar;
    RespawnTime: TTrackBar;
    SpawnProtection: TTrackBar;
    WarmUp: TTrackBar;
    InverseDamage: TTrackBar;
    GroupBox6: TGroupBox;
    AllowFriendlyFire: TCheckBox;
    AutoteamBalance: TCheckBox;
    Allow3dPerson: TCheckBox;
    AllowCrossHair: TCheckBox;
    FallingDamage: TCheckBox;
    AllowRespawn: TCheckBox;
    AllowVehicles: TCheckBox;
    GroupBox7: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    RespawnNumberTxt: TLabel;
    ServerCoopDificulty: TComboBox;
    TeamRespawn: TCheckBox;
    RespawnNumber: TTrackBar;
    GameStyle: TComboBox;
    TabSheet5: TTabSheet;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label29: TLabel;
    Password: TEdit;
    MaxPing: TEdit;
    MaxFrequency: TEdit;
    AdminPassword: TEdit;
    VoiceChat: TComboBox;
    MaxInactivity: TEdit;
    Messages: TMemo;
    RemoveNick: TButton;
    AddNick: TButton;
    TabSheet7: TTabSheet;
    TabSheet1: TTabSheet;
    Label24: TLabel;
    Label26: TLabel;
    Allied: TSpeedButton;
    Axis: TSpeedButton;
    ClanTAG: TEdit;
    EnableAutokick: TCheckBox;
    ApplySettings: TButton;
    CancelSettings: TButton;
    Panel2: TPanel;
    ServerConfigs: TListView;
    CreateConfig: TButton;
    EditConfig: TButton;
    DeleteConfig: TButton;
    RenameConfig: TButton;
    Bevel1: TBevel;
    Close: TButton;
    TimersGroupBox: TGroupBox;
    EnableWatchdog: TCheckBox;
    EnableMessages: TCheckBox;
    Banlist: TListView;
    AddMap: TButton;
    RemoveMap: TButton;
    MoveMapUp: TButton;
    MoveMapDown: TButton;
    Label22: TLabel;
    ReserveClanSidePlayers: TTrackBar;
    ReserveClanSidePlayersTxt: TLabel;
    AviabileMaps: TListView;
    SelectedMaps: TListView;
    EditNick: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ServerConfigsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ServerConfigsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ServerConfigsKeyPress(Sender: TObject; var Key: Char);
    procedure ServerConfigsDblClick(Sender: TObject);
    procedure EditConfigClick(Sender: TObject);
    procedure ApplySettingsClick(Sender: TObject);
    procedure CancelSettingsClick(Sender: TObject);
    procedure MaxPlayersChange(Sender: TObject);
    procedure PointLimitChange(Sender: TObject);
    procedure RoundTimeLimitChange(Sender: TObject);
    procedure RoundCountChange(Sender: TObject);
    procedure RespawnTimeChange(Sender: TObject);
    procedure SpawnProtectionChange(Sender: TObject);
    procedure WarmUpChange(Sender: TObject);
    procedure InverseDamageChange(Sender: TObject);
    procedure RespawnNumberChange(Sender: TObject);
    procedure GameStyleChange(Sender: TObject);
    procedure ReserveClanSidePlayersChange(Sender: TObject);
    procedure AviabileMapsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure SelectedMapsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure AviabileMapsDblClick(Sender: TObject);
    procedure SelectedMapsDblClick(Sender: TObject);
    procedure AddMapClick(Sender: TObject);
    procedure RemoveMapClick(Sender: TObject);
    procedure MoveMapUpClick(Sender: TObject);
    procedure MoveMapDownClick(Sender: TObject);
    procedure BanlistSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure AddNickClick(Sender: TObject);
    procedure EditNickClick(Sender: TObject);
    procedure BanlistDblClick(Sender: TObject);
    procedure RemoveNickClick(Sender: TObject);
    procedure CloseClick(Sender: TObject);
    procedure EnableWatchdogClick(Sender: TObject);
    procedure CreateConfigClick(Sender: TObject);
    procedure RenameConfigClick(Sender: TObject);
    procedure DeleteConfigClick(Sender: TObject);
  private
    { Private declarations }
    CurrentConfig:   TListItem;

    procedure CheckCurrentConfigCheckBoxes;
    procedure UpdateComponentsStatus;
    procedure UpdateMaplists;

  public
    { Public declarations }
    Server:          TServer;
    Maps:            array of string;
    SettingsChanged: boolean;
  end;

var
  ServerSettingsForm: TServerSettingsForm;

implementation

uses NickToBan, EditConfigName;

{$R *.DFM}

procedure TServerSettingsForm.FormCreate(Sender: TObject);
begin
   GameStyle.ItemIndex:=0;
   VoiceChat.ItemIndex:=0;
end;

procedure TServerSettingsForm.FormShow(Sender: TObject);
var
   a:   Integer;
   LI:  TListItem;
begin
   SettingsChanged:=false;

   Panel1.BringToFront;
   Panel1.Visible:=false;

   RenameConfig.Enabled:=false;
   EditConfig.Enabled:=false;
   DeleteConfig.Enabled:=false;

   EnableWatchdog.Checked:=Server.watchdogenable;
   EnableMessages.Checked:=Server.messagesenable;

   ServerConfigs.Items.BeginUpdate;
   ServerConfigs.Items.Clear;

   for a:=0 to length (Server.configurations)-1 do
   begin
      LI:=ServerConfigs.Items.Add;

      LI.Caption:=Server.configurations [a].name;
      if Server.configurations [a].name=Server.currentconfig then
      begin
         LI.Checked:=true;
         CurrentConfig:=LI;
      end else LI.Checked:=false;
   end;

   ServerConfigs.Items.EndUpdate;

   TimersGroupBox.SetFocus;
   EnableWatchdog.SetFocus;
end;

procedure TServerSettingsForm.EditConfigClick(Sender: TObject);
var
   a,c:   integer;
   LI:    TListItem;
begin
   ServerSettings.ActivePage:=TabSheet4;
   Panel2.Visible:=false;
   Panel1.Visible:=true;
   Caption:='Server settings - '+ServerConfigs.Selected.Caption;

   c:=ServerConfigs.Selected.Index;

   GameStyle.ItemIndex:=0;
   for a:=0 to GameStyle.Items.Count-1 do
      if GameStyle.Items [a]=Server.configurations [c].style then
         GameStyle.ItemIndex:=a;

   SessionName.Text:=Server.configurations [c].sessionname;

   if Server.configurations [c].domain='Internet' then
      DomainInternet.Checked:=true else DomainLocal.Checked:=true;

   MaxPlayers.Position:=Server.configurations [c].maxclients;
   PointLimit.Position:=Server.configurations [c].pointlimit;
   RoundTimeLimit.Position:=Server.configurations [c].roundlimit;
   RoundCount.Position:=Server.configurations [c].roundcount;
   RespawnTime.Position:=Server.configurations [c].respawntime;
   SpawnProtection.Position:=Server.configurations [c].spawnprotection;
   WarmUp.Position:=Server.configurations [c].warmup;
   InverseDamage.Position:=Server.configurations [c].inversedamage;

   ServerCoopDificulty.ItemIndex:=2;
   for a:=0 to ServerCoopDificulty.Items.Count-1 do
      if ServerCoopDificulty.Items [a]=Server.configurations [c].dificulty then
         ServerCoopDificulty.ItemIndex:=a;

   RespawnNumber.Position:=Server.configurations [c].respawnnumber;

   TeamRespawn.Checked:=Server.configurations [c].teamrespawn;

   AllowFriendlyFire.Checked:=Server.configurations [c].friendlyfire;
   AutoteamBalance.Checked:=Server.configurations [c].autoteambalance;
   Allow3dPerson.Checked:=Server.configurations [c]._3rdpersonview;
   AllowCrossHair.Checked:=Server.configurations [c].allowcrosshair;
   FallingDamage.Checked:=Server.configurations [c].fallingdmg;
   AllowRespawn.Checked:=Server.configurations [c].allowrespawn;
   AllowVehicles.Checked:=Server.configurations [c].allowvehicles;

   Password.Text:=Server.configurations [c].password;
   MaxPing.Text:=IntToStr (Server.configurations [c].maxping);
   MaxFrequency.Text:=IntToStr (Server.configurations [c].maxfreq);
   MaxInactivity.Text:=IntToStr (Server.configurations [c].maxinactivity);
   AdminPassword.Text:=Server.configurations [c].adminpass;

   VoiceChat.ItemIndex:=Server.configurations [c].voicechat;

   Messages.Clear;
   for a:=0 to length (Server.configurations [c].messages)-1 do
      Messages.Lines.Add (Server.configurations [c].messages [a]);

   BanList.Items.Clear;
   for a:=0 to length (Server.configurations [c].banlist)-1 do
   begin
      LI:=BanList.Items.Add;
      LI.Caption:=Server.configurations [c].banlist [a];
   end;

   SelectedMaps.Items.Clear;
   for a:=0 to length (Server.configurations [c].maps)-1 do
   begin
      LI:=SelectedMaps.Items.Add;
      LI.Caption:=Server.configurations [c].maps [a];
   end;

   EnableAutokick.Checked:=Server.configurations [c].enableautokick;

   ClanTAG.Text:=Server.configurations [c].clantag;

   if Server.configurations [c].clanside=0 then
      Allied.Down:=true else Axis.Down:=true;

   ReserveClanSidePlayers.Position:=Server.configurations [c].clanreserve;

   MaxPlayersChange (Sender);
   PointLimitChange (Sender);
   RoundTimeLimitChange (Sender);
   RoundCountChange (Sender);
   RespawnTimeChange (Sender);
   SpawnProtectionChange (Sender);
   WarmUpChange (Sender);
   InverseDamageChange (Sender);
   RespawnNumberChange (Sender);
   ReserveClanSidePlayersChange (Sender);

   UpdateComponentsStatus;
   UpdateMapLists;

   EditNick.Enabled:=false;
   RemoveNick.Enabled:=false;
   AddMap.Enabled:=false;
   RemoveMap.Enabled:=false;
   MoveMapUp.Enabled:=false;
   MoveMapDown.Enabled:=false;
end;



procedure TServerSettingsForm.ServerConfigsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then
   begin
      RenameConfig.Enabled:=true;
      EditConfig.Enabled:=true;
      DeleteConfig.Enabled:=true;
   end else
      begin
         RenameConfig.Enabled:=false;
         EditConfig.Enabled:=false;
         DeleteConfig.Enabled:=false;
      end;
end;

procedure TServerSettingsForm.CheckCurrentConfigCheckBoxes;
var
   a:   Integer;
   Ch:  Integer;
begin
   Ch:=0;
   for a:=0 to ServerConfigs.Items.Count-1 do
      if ServerConfigs.Items [a].Checked then Inc (Ch);

   if CurrentConfig<>nil then
   begin
      if Ch<2 then CurrentConfig.Checked:=true else
         if Ch>1 then
         begin
            if CurrentConfig<>ServerConfigs.Selected then SettingsChanged:=true;
            CurrentConfig.Checked:=false;
            CurrentConfig:=ServerConfigs.Selected;
            CurrentConfig.Checked:=true;
         end;
   end;
end;

procedure TServerSettingsForm.ServerConfigsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if (ServerConfigs.GetItemAt (X,Y))<>nil then
   begin
      ServerConfigs.GetItemAt (X,Y).Selected:=true;
      ServerConfigs.GetItemAt (X,Y).Focused:=true;
   end;

   CheckCurrentConfigCheckBoxes;
end;

procedure TServerSettingsForm.ServerConfigsKeyPress(Sender: TObject;
  var Key: Char);
begin
   CheckCurrentConfigCheckBoxes;
end;

procedure TServerSettingsForm.ServerConfigsDblClick(Sender: TObject);
begin
   if EditConfig.Enabled then EditConfigClick (Sender);
end;

procedure TServerSettingsForm.ApplySettingsClick(Sender: TObject);
var
   a,c:   integer;
begin
   Caption:='Server settings';
   Panel2.Visible:=true;
   Panel1.Visible:=false;

   SettingsChanged:=true;

   c:=ServerConfigs.Selected.Index;

   Server.configurations [c].style:=GameStyle.Text;

   Server.configurations [c].sessionname:=SessionName.Text;

   if DomainInternet.Checked then
      Server.configurations [c].domain:='Internet'
   else
      Server.configurations [c].domain:='Local';

   Server.configurations [c].maxclients:=MaxPlayers.Position;
   Server.configurations [c].pointlimit:=PointLimit.Position;
   Server.configurations [c].roundlimit:=RoundTimeLimit.Position;
   Server.configurations [c].roundcount:=RoundCount.Position;
   Server.configurations [c].respawntime:=RespawnTime.Position;
   Server.configurations [c].spawnprotection:=SpawnProtection.Position;
   Server.configurations [c].warmup:=WarmUp.Position;
   Server.configurations [c].inversedamage:=InverseDamage.Position;

   Server.configurations [c].dificulty:=ServerCoopDificulty.Text;

   Server.configurations [c].respawnnumber:=RespawnNumber.Position;

   Server.configurations [c].teamrespawn:=TeamRespawn.Checked;

   Server.configurations [c].friendlyfire:=AllowFriendlyFire.Checked;
   Server.configurations [c].autoteambalance:=AutoteamBalance.Checked;
   Server.configurations [c]._3rdpersonview:=Allow3dPerson.Checked;
   Server.configurations [c].allowcrosshair:=AllowCrossHair.Checked;
   Server.configurations [c].fallingdmg:=FallingDamage.Checked;
   Server.configurations [c].allowrespawn:=AllowRespawn.Checked;
   Server.configurations [c].allowvehicles:=AllowVehicles.Checked;

   Server.configurations [c].password:=Password.Text;
   Server.configurations [c].maxping:=StrToInt (MaxPing.Text);
   Server.configurations [c].maxfreq:=StrToInt (MaxFrequency.Text);
   Server.configurations [c].maxinactivity:=StrToInt (MaxInactivity.Text);
   Server.configurations [c].adminpass:=AdminPassword.Text;

   Server.configurations [c].voicechat:=VoiceChat.ItemIndex;

   SetLength (Server.configurations [c].messages,Messages.Lines.Count);
   for a:=0 to length (Server.configurations [c].messages)-1 do
      Server.configurations [c].messages [a]:=Messages.Lines [a];

   SetLength (Server.configurations [c].banlist,BanList.Items.Count);
   for a:=0 to length (Server.configurations [c].banlist)-1 do
      Server.configurations [c].banlist [a]:=BanList.Items [a].Caption;

   SetLength (Server.configurations [c].maps,SelectedMaps.Items.Count);
   for a:=0 to length (Server.configurations [c].maps)-1 do
      Server.configurations [c].maps [a]:=SelectedMaps.Items [a].Caption;

   Server.configurations [c].enableautokick:=EnableAutokick.Checked;

   Server.configurations [c].clantag:=ClanTAG.Text;

   if Axis.Down then
      Server.configurations [c].clanside:=1
   else
      Server.configurations [c].clanside:=0;

   Server.configurations [c].clanreserve:=ReserveClanSidePlayers.Position;
end;

procedure TServerSettingsForm.CancelSettingsClick(Sender: TObject);
begin
   Caption:='Server settings';
   Panel2.Visible:=true;
   Panel1.Visible:=false;
end;

procedure TServerSettingsForm.MaxPlayersChange(Sender: TObject);
begin
   MaxPlayersTxt.Caption:=IntToStr (MaxPlayers.Position);

   ReserveClanSidePlayers.Max:=MaxPlayers.Position;
end;

procedure TServerSettingsForm.PointLimitChange(Sender: TObject);
begin
   if PointLimit.Position>0 then
      PointLimitTxt.Caption:=IntToStr (PointLimit.Position)
   else
      PointLimitTxt.Caption:='no limit';
end;

procedure TServerSettingsForm.RoundTimeLimitChange(Sender: TObject);
begin
   if RoundTimeLimit.Position>0 then
      RoundTimeLimitTxt.Caption:=IntToStr (RoundTimeLimit.Position)+' min'
   else
      RoundTimeLimitTxt.Caption:='no limit'
end;

procedure TServerSettingsForm.RoundCountChange(Sender: TObject);
begin
   if RoundCount.Position>0 then
      RoundCountTxt.Caption:=IntToStr (RoundCount.Position)
   else
      RoundCountTxt.Caption:='no limit'
end;

procedure TServerSettingsForm.RespawnTimeChange(Sender: TObject);
begin
   RespawnTimeTxt.Caption:=IntToStr (RespawnTime.Position)+' sec';
end;

procedure TServerSettingsForm.SpawnProtectionChange(Sender: TObject);
begin
   SpawnProtectionTxt.Caption:=IntToStr (SpawnProtection.Position)+' sec';
end;

procedure TServerSettingsForm.WarmUpChange(Sender: TObject);
begin
   WarmUpTxt.Caption:=IntToStr (WarmUp.Position)+' sec';
end;

procedure TServerSettingsForm.InverseDamageChange(Sender: TObject);
begin
   InverseDamageTxt.Caption:=IntToStr (InverseDamage.Position)+' %';
end;

procedure TServerSettingsForm.RespawnNumberChange(Sender: TObject);
begin
   if RespawnNumber.Position=0 then
      RespawnNumberTxt.Caption:='no limit'
   else
      RespawnNumberTxt.Caption:=IntToStr (RespawnNumber.Position);
end;

procedure TServerSettingsForm.ReserveClanSidePlayersChange(
  Sender: TObject);
begin
   ReserveClanSidePlayersTxt.Caption:=IntToStr (ReserveClanSidePlayers.Position);
end;

procedure TServerSettingsForm.UpdateComponentsStatus;
begin
   PointLimit.Enabled:=true;
   PointLimitTxt.Enabled:=true;
   RespawnTime.Enabled:=true;
   RespawnTimeTxt.Enabled:=true;
   SpawnProtection.Enabled:=true;
   SpawnProtectionTxt.Enabled:=true;
   RespawnNumber.Enabled:=true;
   RespawnNumberTxt.Enabled:=true;
   InverseDamage.Enabled:=true;
   InverseDamageTxt.Enabled:=true;
   AutoteamBalance.Enabled:=true;
   ServerCoopDificulty.Enabled:=true;
   ServerCoopDificulty.Color:=clWindow;
   TeamRespawn.Enabled:=true;
   AllowRespawn.Enabled:=true;
   TeamRespawn.Enabled:=true;
   AllowFriendlyFire.Enabled:=true;


   if GameStyle.Text='Objectives' then
   begin
      PointLimit.Enabled:=false;
      PointLimitTxt.Enabled:=false;
      RespawnTime.Enabled:=false;
      RespawnTimeTxt.Enabled:=false;
      SpawnProtection.Enabled:=false;
      SpawnProtectionTxt.Enabled:=false;
      ServerCoopDificulty.Enabled:=false;
      ServerCoopDificulty.Color:=clBtnFace;
      RespawnNumber.Enabled:=false;
      RespawnNumberTxt.Enabled:=false;
      TeamRespawn.Enabled:=false;
   end;

   if GameStyle.Text='Occupation' then
   begin
      AllowRespawn.Checked:=true;
      AllowRespawn.Enabled:=false;
      ServerCoopDificulty.Enabled:=false;
      ServerCoopDificulty.Color:=clBtnFace;
      RespawnNumber.Enabled:=false;
      RespawnNumberTxt.Enabled:=false;
      TeamRespawn.Enabled:=false;
   end;

   if GameStyle.Text='Deathmatch' then
   begin
      AllowFriendlyFire.Enabled:=false;
      AutoteamBalance.Enabled:=false;
      AllowRespawn.Checked:=true;
      AllowRespawn.Enabled:=false;
      InverseDamage.Enabled:=false;
      InverseDamageTxt.Enabled:=false;
      ServerCoopDificulty.Enabled:=false;
      ServerCoopDificulty.Color:=clBtnFace;
      RespawnNumber.Enabled:=false;
      RespawnNumberTxt.Enabled:=false;
      TeamRespawn.Enabled:=false;
   end;

   if GameStyle.Text='Cooperative' then
   begin
      PointLimit.Enabled:=false;
      PointLimitTxt.Enabled:=false;
      AutoteamBalance.Enabled:=false;
      AllowRespawn.Checked:=true;
      AllowRespawn.Enabled:=false;
   end;
end;

procedure TServerSettingsForm.UpdateMaplists;
var
   a:   Integer;
begin
   AviabileMaps.Items.Clear;
   a:=0;

   if GameStyle.Text='Objectives' then
      while (a<length (Maps)) and (Maps [a]<>'<hd2multiplayer>') do inc (a);

   if GameStyle.Text='Occupation' then
      while (a<length (Maps)) and (Maps [a]<>'<teamplay>') do inc (a);

   if GameStyle.Text='Deathmatch' then
      while (a<length (Maps)) and (Maps [a]<>'<deathmatch>') do inc (a);

   if GameStyle.Text='Cooperative' then
      while (a<length (Maps)) and (Maps [a]<>'<cooperative>') do inc (a);

   Inc (a);

   if a<length (Maps) then
      while (a<length (Maps)) and (Maps [a][1]<>'<') do
      begin
         if SelectedMaps.FindCaption (0,Maps [a],false,true,false)=nil then
            AviabileMaps.Items.Add.Caption:=Maps [a];
         Inc (a);
      end;
end;

procedure TServerSettingsForm.GameStyleChange(Sender: TObject);
begin
   UpdateComponentsStatus;
   SelectedMaps.Items.Clear;
   UpdateMaplists;
end;

procedure TServerSettingsForm.AviabileMapsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then AddMap.Enabled:=true else AddMap.Enabled:=false;
end;

procedure TServerSettingsForm.SelectedMapsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then
   begin
      RemoveMap.Enabled:=true;
      MoveMapUp.Enabled:=true;
      MoveMapDown.Enabled:=true;
   end else
      begin
         RemoveMap.Enabled:=false;
         MoveMapUp.Enabled:=false;
         MoveMapDown.Enabled:=false;
      end;
end;

procedure TServerSettingsForm.AviabileMapsDblClick(Sender: TObject);
begin
   if AddMap.Enabled then AddMapClick (Sender);
end;

procedure TServerSettingsForm.SelectedMapsDblClick(Sender: TObject);
begin
   if RemoveMap.Enabled then RemoveMapClick (Sender);
end;

procedure TServerSettingsForm.AddMapClick(Sender: TObject);
begin
   SelectedMaps.Items.Add.Caption:=AviabileMaps.Selected.Caption;
   AviabileMaps.Selected.Delete;
end;

procedure TServerSettingsForm.RemoveMapClick(Sender: TObject);
begin
   AviabileMaps.Items.Add.Caption:=SelectedMaps.Selected.Caption;
   SelectedMaps.Selected.Delete;
end;

procedure TServerSettingsForm.MoveMapUpClick(Sender: TObject);
var
   tmp: string;
begin
   if SelectedMaps.Selected.Index>0 then
   begin
      tmp:=SelectedMaps.Items [SelectedMaps.Selected.Index-1].Caption;
      SelectedMaps.Items [SelectedMaps.Selected.Index-1].Caption:=SelectedMaps.Selected.Caption;
      SelectedMaps.Selected.Caption:=tmp;
      SelectedMaps.Items [SelectedMaps.Selected.Index-1].Selected:=true;
   end;
end;

procedure TServerSettingsForm.MoveMapDownClick(Sender: TObject);
var
   tmp: string;
begin
   if SelectedMaps.Selected.Index<SelectedMaps.Items.Count-1 then
   begin
      tmp:=SelectedMaps.Items [SelectedMaps.Selected.Index+1].Caption;
      SelectedMaps.Items [SelectedMaps.Selected.Index+1].Caption:=SelectedMaps.Selected.Caption;
      SelectedMaps.Selected.Caption:=tmp;
      SelectedMaps.Items [SelectedMaps.Selected.Index+1].Selected:=true;
   end;
end;

procedure TServerSettingsForm.BanlistSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then
   begin
      EditNick.Enabled:=true;
      RemoveNick.Enabled:=true
   end else
      begin
         EditNick.Enabled:=false;
         RemoveNick.Enabled:=false;
      end;
end;

procedure TServerSettingsForm.AddNickClick(Sender: TObject);
begin
   BanUser.Nick.Text:='';
   if BanUser.ShowModal=mrOk then
      if BanUser.Nick.Text<>'' then BanList.Items.Add.Caption:=BanUser.Nick.Text;
end;

procedure TServerSettingsForm.EditNickClick(Sender: TObject);
begin
   BanUser.Nick.Text:=BanList.Selected.Caption;
   if BanUser.ShowModal=mrOk then
      if BanUser.Nick.Text<>'' then BanList.Selected.Caption:=BanUser.Nick.Text;
end;

procedure TServerSettingsForm.BanlistDblClick(Sender: TObject);
begin
   if EditNick.Enabled then EditNick.Click;
end;

procedure TServerSettingsForm.RemoveNickClick(Sender: TObject);
begin
   if MessageBox (Handle,'Are you sure to remove player from ban list?',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
      BanList.Selected.Delete;
end;

procedure TServerSettingsForm.CloseClick(Sender: TObject);
var
   a:   integer;
begin
   Server.watchdogenable:=EnableWatchdog.Checked;
   Server.messagesenable:=EnableMessages.Checked;
   if CurrentConfig<>nil then Server.currentconfig:=CurrentConfig.Caption else
      Server.currentconfig:=''; 

   if not SettingsChanged then
      for a:=0 to ServerConfigs.Items.Count-1 do
         if ServerConfigs.Items [a].Checked and
               (ServerConfigs.Items [a].Caption<>Server.currentconfig) then
         begin
            SettingsChanged:=true;
            Break;
         end;

   if SettingsChanged then ModalResult:=mrOK else ModalResult:=mrCancel;
end;

procedure TServerSettingsForm.EnableWatchdogClick(Sender: TObject);
begin
   SettingsChanged:=true;
end;

procedure TServerSettingsForm.CreateConfigClick(Sender: TObject);
var
   LI:  TListItem;
begin
   ConfigurationName.ConfigName.Text:='';
   if ConfigurationName.ShowModal=mrOk then
   begin
      SettingsChanged:=true;
      SetLength (Server.configurations,length (Server.configurations)+1);
      Server.configurations [length (Server.configurations)-1].name:=ConfigurationName.ConfigName.Text;
      LI:=ServerConfigs.Items.Add;
      LI.Caption:=ConfigurationName.ConfigName.Text;
      LI.Selected:=true;
      if ServerConfigs.Items.Count=1 then
      begin
         LI.Checked:=true;
         CurrentConfig:=LI;
      end else LI.Checked:=false;
   end;
end;

procedure TServerSettingsForm.RenameConfigClick(Sender: TObject);
begin
   ConfigurationName.ConfigName.Text:=ServerConfigs.Selected.Caption;
   if ConfigurationName.ShowModal=mrOk then
   begin
      SettingsChanged:=true;
      ServerConfigs.Selected.Caption:=ConfigurationName.ConfigName.Text;
      Server.configurations [ServerConfigs.Selected.Index].name:=ConfigurationName.ConfigName.Text;
      if ServerConfigs.Selected.Checked then
         Server.currentconfig:=ConfigurationName.ConfigName.Text;
   end;
end;

procedure TServerSettingsForm.DeleteConfigClick(Sender: TObject);
var
   a:   integer;
   b:   boolean;
begin
   if MessageBox (Handle,'Are you sure to remove selected server configuration?',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
   begin
      SettingsChanged:=true;
      b:=ServerConfigs.Selected.Checked;
      if ServerConfigs.Selected.Index<length (Server.configurations)-1 then
         for a:=ServerConfigs.Selected.Index to length (Server.configurations)-2 do
            Server.configurations [a]:=Server.configurations [a+1];
      SetLength (Server.configurations,length (Server.configurations)-1);
      ServerConfigs.Selected.Delete;
      if b and (ServerConfigs.Items.Count>0) then
      begin
         CurrentConfig:=ServerConfigs.Items [0];
         CurrentConfig.Checked:=true;
      end;
   end;
end;

end.

