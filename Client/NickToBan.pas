unit NickToBan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TBanUser = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Nick: TEdit;
    Button1: TButton;
    Button2: TButton;
    Players: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PlayersClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BanUser: TBanUser;

implementation

uses CurrentPlayers;

{$R *.DFM}

procedure TBanUser.FormShow(Sender: TObject);
begin
   Nick.SetFocus;
end;

procedure TBanUser.Button1Click(Sender: TObject);
begin
   if Trim (Nick.Text)<>'' then ModalResult:=mrOK;
end;

procedure TBanUser.PlayersClick(Sender: TObject);
begin
   if CurrentPlayersForm.ShowModal = mrOk then
      Nick.Text:=CurrentPlayersForm.CurrentPlayers.Selected.Text;
end;

end.
