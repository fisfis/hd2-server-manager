unit ServerProps;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls;

type
  TServerProperties = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ServerName: TEdit;
    ServerPort: TEdit;
    AddUser: TButton;
    RemoveUser: TButton;
    Bevel1: TBevel;
    Ok: TButton;
    Cancel: TButton;
    GrantedUsers: TListView;
    procedure FormShow(Sender: TObject);
    procedure GrantedUsersSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure RemoveUserClick(Sender: TObject);
    procedure AddUserClick(Sender: TObject);
    procedure OkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ServerProperties: TServerProperties;

implementation

uses SelectUser;

{$R *.DFM}

procedure TServerProperties.FormShow(Sender: TObject);
begin
   RemoveUser.Enabled:=false;
   ServerName.SetFocus;
end;

procedure TServerProperties.GrantedUsersSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
   if Selected then RemoveUser.Enabled:=true else RemoveUser.Enabled:=false;
end;

procedure TServerProperties.RemoveUserClick(Sender: TObject);
begin
   if MessageBox (Handle,'Are you sure to remove selected granted user?',
                         'Hidden and dangerous 2 - Server configurator',
                         MB_YESNO or MB_ICONASTERISK) = IDYES then
   begin
      GrantedUsers.Selected.Delete;
   end;

end;

procedure TServerProperties.AddUserClick(Sender: TObject);
var
   a:   Integer;
begin
   if GrantUser.ShowModal=mrOK then
      for a:=0 to GrantUser.UserList.Items.Count-1 do
         if GrantUser.UserList.Selected [a] then
            if GrantedUsers.FindCaption (0,GrantUser.UserList.Items [a],false,true,false)=nil then
               GrantedUsers.Items.Add.Caption:=GrantUser.UserList.Items [a];
end;

procedure TServerProperties.OkClick(Sender: TObject);
begin
   if Trim (ServerName.Text)<>'' then
   try
      StrToInt (ServerPort.Text);
      ModalResult:=mrOk;
   except
   end;
end;

end.
