object ServerStats: TServerStats
  Left = 760
  Top = 541
  Width = 493
  Height = 432
  Caption = 'Statistics'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 478
    Height = 398
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 8
      Top = 8
      Width = 466
      Height = 352
      ActivePage = TabSheet1
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Current statistics'
        object Label30: TLabel
          Left = 8
          Top = 0
          Width = 71
          Height = 13
          Caption = 'Server version:'
        end
        object Label31: TLabel
          Left = 160
          Top = 0
          Width = 54
          Height = 13
          Caption = 'Game type:'
        end
        object Label28: TLabel
          Left = 304
          Top = 0
          Width = 60
          Height = 13
          Caption = 'Current map:'
        end
        object Label25: TLabel
          Left = 8
          Top = 13
          Width = 70
          Height = 24
          Caption = 'ALLIED'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 8
          Top = 165
          Width = 48
          Height = 24
          Caption = 'AXIS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label32: TLabel
          Left = 380
          Top = 165
          Width = 70
          Height = 24
          Alignment = taRightJustify
          AutoSize = False
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 376
          Top = 13
          Width = 74
          Height = 25
          Alignment = taRightJustify
          AutoSize = False
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CurrentStatsAllied: TListView
          Left = 8
          Top = 35
          Width = 441
          Height = 130
          Columns = <
            item
              AutoSize = True
              Caption = 'Player'
            end
            item
              Alignment = taCenter
              Caption = 'Score'
            end
            item
              Alignment = taCenter
              Caption = 'Deaths'
            end
            item
              Alignment = taCenter
              Caption = 'Ping'
            end>
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
        object CurrentStatsAxis: TListView
          Left = 8
          Top = 187
          Width = 441
          Height = 130
          Columns = <
            item
              AutoSize = True
              Caption = 'Player'
            end
            item
              Alignment = taCenter
              Caption = 'Score'
            end
            item
              Alignment = taCenter
              Caption = 'Deaths'
            end
            item
              Alignment = taCenter
              Caption = 'Ping'
            end>
          ReadOnly = True
          RowSelect = True
          TabOrder = 1
          ViewStyle = vsReport
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Player statistics'
        ImageIndex = 1
        object PlayerStats: TListView
          Left = 8
          Top = 7
          Width = 441
          Height = 310
          Columns = <
            item
              AutoSize = True
              Caption = 'Player'
            end
            item
              Alignment = taCenter
              Caption = 'Score'
            end
            item
              Alignment = taCenter
              Caption = 'Deaths'
            end
            item
              Alignment = taCenter
              Caption = 'Time'
            end>
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
    end
    object Button1: TButton
      Left = 399
      Top = 366
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Close'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
  end
end
