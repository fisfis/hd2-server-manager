object ServerSettingsForm: TServerSettingsForm
  Left = 768
  Top = 111
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Server settings'
  ClientHeight = 400
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 3
    Top = 3
    Width = 478
    Height = 390
    TabOrder = 1
    object Bevel1: TBevel
      Left = 8
      Top = 352
      Width = 461
      Height = 9
      Shape = bsTopLine
    end
    object ServerConfigs: TListView
      Left = 8
      Top = 48
      Width = 460
      Height = 265
      Checkboxes = True
      Columns = <
        item
          AutoSize = True
          Caption = 'Server configurations (checked is currently in use)'
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      ViewStyle = vsReport
      OnDblClick = ServerConfigsDblClick
      OnKeyPress = ServerConfigsKeyPress
      OnMouseDown = ServerConfigsMouseDown
      OnSelectItem = ServerConfigsSelectItem
    end
    object CreateConfig: TButton
      Left = 92
      Top = 320
      Width = 89
      Height = 25
      Caption = 'Create config'
      TabOrder = 2
      OnClick = CreateConfigClick
    end
    object EditConfig: TButton
      Left = 380
      Top = 320
      Width = 89
      Height = 25
      Caption = 'Configure'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = EditConfigClick
    end
    object DeleteConfig: TButton
      Left = 284
      Top = 320
      Width = 89
      Height = 25
      Caption = 'Delete config'
      TabOrder = 4
      OnClick = DeleteConfigClick
    end
    object RenameConfig: TButton
      Left = 188
      Top = 320
      Width = 89
      Height = 25
      Caption = 'Rename config'
      TabOrder = 3
      OnClick = RenameConfigClick
    end
    object Close: TButton
      Left = 380
      Top = 359
      Width = 89
      Height = 25
      Cancel = True
      Caption = 'Close'
      Default = True
      TabOrder = 6
      OnClick = CloseClick
    end
    object TimersGroupBox: TGroupBox
      Left = 8
      Top = 8
      Width = 457
      Height = 33
      Caption = ' Timers '
      TabOrder = 0
      OnClick = EnableWatchdogClick
      object EnableWatchdog: TCheckBox
        Left = 101
        Top = 11
        Width = 105
        Height = 17
        Caption = 'Enable watchdog'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = EnableWatchdogClick
      end
      object EnableMessages: TCheckBox
        Left = 269
        Top = 11
        Width = 105
        Height = 17
        Caption = 'Enable messages'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = EnableWatchdogClick
      end
    end
  end
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 478
    Height = 390
    TabOrder = 0
    object ServerSettings: TPageControl
      Left = 7
      Top = 5
      Width = 466
      Height = 350
      ActivePage = TabSheet4
      TabOrder = 0
      object TabSheet4: TTabSheet
        Caption = 'General'
        object Label2: TLabel
          Left = 8
          Top = 36
          Width = 69
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'Session name:'
          ParentBiDiMode = False
        end
        object Label3: TLabel
          Left = 8
          Top = 0
          Width = 55
          Height = 13
          Caption = 'Game style:'
        end
        object SessionName: TEdit
          Left = 8
          Top = 51
          Width = 353
          Height = 21
          TabOrder = 0
        end
        object GroupBox3: TGroupBox
          Left = 368
          Top = 8
          Width = 81
          Height = 65
          Caption = ' Domain '
          TabOrder = 1
          object DomainLocal: TRadioButton
            Left = 8
            Top = 19
            Width = 58
            Height = 16
            Caption = 'Local'
            TabOrder = 0
          end
          object DomainInternet: TRadioButton
            Left = 8
            Top = 40
            Width = 57
            Height = 17
            Caption = 'Internet'
            Checked = True
            TabOrder = 1
            TabStop = True
          end
        end
        object GroupBox5: TGroupBox
          Left = 8
          Top = 80
          Width = 313
          Height = 145
          TabOrder = 2
          object Label4: TLabel
            Left = 8
            Top = 12
            Width = 56
            Height = 13
            Caption = 'Max players'
          end
          object Label5: TLabel
            Left = 8
            Top = 28
            Width = 44
            Height = 13
            Caption = 'Point limit'
          end
          object Label6: TLabel
            Left = 8
            Top = 44
            Width = 74
            Height = 13
            Caption = 'Round time limit'
          end
          object Label7: TLabel
            Left = 8
            Top = 60
            Width = 62
            Height = 13
            Caption = 'Round count'
          end
          object Label8: TLabel
            Left = 8
            Top = 76
            Width = 67
            Height = 13
            Caption = 'Respawn time'
          end
          object Label9: TLabel
            Left = 8
            Top = 92
            Width = 83
            Height = 13
            Caption = 'Spawn protection'
          end
          object Label10: TLabel
            Left = 8
            Top = 108
            Width = 40
            Height = 13
            Caption = 'Warmup'
          end
          object Label11: TLabel
            Left = 8
            Top = 124
            Width = 76
            Height = 13
            Caption = 'Inverse damage'
          end
          object MaxPlayersTxt: TLabel
            Left = 272
            Top = 12
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object PointLimitTxt: TLabel
            Left = 272
            Top = 28
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object RoundTimeLimitTxt: TLabel
            Left = 272
            Top = 44
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object RoundCOuntTxt: TLabel
            Left = 272
            Top = 60
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object RespawnTimeTxt: TLabel
            Left = 272
            Top = 76
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object SpawnProtectionTxt: TLabel
            Left = 272
            Top = 92
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object WarmUpTxt: TLabel
            Left = 272
            Top = 108
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object InverseDamageTxt: TLabel
            Left = 272
            Top = 124
            Width = 38
            Height = 13
            Alignment = taCenter
            AutoSize = False
          end
          object MaxPlayers: TTrackBar
            Left = 94
            Top = 10
            Width = 179
            Height = 21
            Max = 64
            Min = 2
            Orientation = trHorizontal
            Frequency = 1
            Position = 64
            SelEnd = 0
            SelStart = 0
            TabOrder = 0
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = MaxPlayersChange
          end
          object PointLimit: TTrackBar
            Left = 94
            Top = 26
            Width = 179
            Height = 21
            Max = 200
            Orientation = trHorizontal
            Frequency = 1
            Position = 0
            SelEnd = 0
            SelStart = 0
            TabOrder = 1
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = PointLimitChange
          end
          object RoundTimeLimit: TTrackBar
            Left = 94
            Top = 42
            Width = 179
            Height = 21
            Max = 180
            Orientation = trHorizontal
            Frequency = 1
            Position = 30
            SelEnd = 0
            SelStart = 0
            TabOrder = 2
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = RoundTimeLimitChange
          end
          object RoundCount: TTrackBar
            Left = 94
            Top = 58
            Width = 179
            Height = 21
            Max = 20
            Orientation = trHorizontal
            Frequency = 1
            Position = 1
            SelEnd = 0
            SelStart = 0
            TabOrder = 3
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = RoundCountChange
          end
          object RespawnTime: TTrackBar
            Left = 94
            Top = 74
            Width = 179
            Height = 21
            Max = 300
            Orientation = trHorizontal
            Frequency = 1
            Position = 2
            SelEnd = 0
            SelStart = 0
            TabOrder = 4
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = RespawnTimeChange
          end
          object SpawnProtection: TTrackBar
            Left = 94
            Top = 90
            Width = 179
            Height = 21
            Max = 30
            Orientation = trHorizontal
            Frequency = 1
            Position = 0
            SelEnd = 0
            SelStart = 0
            TabOrder = 5
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = SpawnProtectionChange
          end
          object WarmUp: TTrackBar
            Left = 94
            Top = 106
            Width = 179
            Height = 21
            Max = 60
            Orientation = trHorizontal
            Frequency = 1
            Position = 0
            SelEnd = 0
            SelStart = 0
            TabOrder = 6
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = WarmUpChange
          end
          object InverseDamage: TTrackBar
            Left = 94
            Top = 122
            Width = 179
            Height = 21
            Max = 200
            Orientation = trHorizontal
            Frequency = 1
            Position = 0
            SelEnd = 0
            SelStart = 0
            TabOrder = 7
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = InverseDamageChange
          end
        end
        object GroupBox6: TGroupBox
          Left = 8
          Top = 224
          Width = 441
          Height = 89
          TabOrder = 3
          object AllowFriendlyFire: TCheckBox
            Left = 8
            Top = 16
            Width = 81
            Height = 17
            Caption = 'Friendly fire'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object AutoteamBalance: TCheckBox
            Left = 8
            Top = 32
            Width = 121
            Height = 17
            Caption = 'Auto team balance'
            TabOrder = 1
          end
          object Allow3dPerson: TCheckBox
            Left = 8
            Top = 48
            Width = 129
            Height = 17
            Caption = 'Allow 3rd person view'
            TabOrder = 2
          end
          object AllowCrossHair: TCheckBox
            Left = 8
            Top = 64
            Width = 97
            Height = 17
            Caption = 'Allow crosshair'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object FallingDamage: TCheckBox
            Left = 224
            Top = 16
            Width = 97
            Height = 17
            Caption = 'Falling damage'
            Checked = True
            State = cbChecked
            TabOrder = 4
          end
          object AllowRespawn: TCheckBox
            Left = 224
            Top = 32
            Width = 97
            Height = 17
            Caption = 'Allow respawn'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 5
          end
          object AllowVehicles: TCheckBox
            Left = 224
            Top = 48
            Width = 97
            Height = 17
            Caption = 'Allow vehicles'
            Checked = True
            State = cbChecked
            TabOrder = 6
          end
        end
        object GroupBox7: TGroupBox
          Left = 328
          Top = 80
          Width = 121
          Height = 145
          Caption = ' Cooperation '
          TabOrder = 4
          object Label12: TLabel
            Left = 8
            Top = 16
            Width = 40
            Height = 13
            Caption = 'Dificulty:'
          end
          object Label13: TLabel
            Left = 8
            Top = 56
            Width = 86
            Height = 13
            Caption = 'Respawn number:'
          end
          object RespawnNumberTxt: TLabel
            Left = 8
            Top = 93
            Width = 105
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'no limit'
          end
          object ServerCoopDificulty: TComboBox
            Left = 8
            Top = 32
            Width = 105
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            Items.Strings = (
              'Easy'
              'Normal'
              'Hard'
              'Very hard')
          end
          object TeamRespawn: TCheckBox
            Left = 15
            Top = 120
            Width = 97
            Height = 17
            Caption = 'Team respawn'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 1
          end
          object RespawnNumber: TTrackBar
            Left = 6
            Top = 74
            Width = 107
            Height = 21
            Enabled = False
            Max = 99
            Orientation = trHorizontal
            Frequency = 1
            Position = 0
            SelEnd = 0
            SelStart = 0
            TabOrder = 2
            ThumbLength = 11
            TickMarks = tmTopLeft
            TickStyle = tsNone
            OnChange = RespawnNumberChange
          end
        end
        object GameStyle: TComboBox
          Left = 8
          Top = 14
          Width = 353
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
          OnChange = GameStyleChange
          Items.Strings = (
            'Objectives'
            'Occupation'
            'Deathmatch'
            'Cooperative')
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Advanced'
        ImageIndex = 1
        object Label14: TLabel
          Left = 8
          Top = 8
          Width = 105
          Height = 13
          Caption = 'Connection password:'
        end
        object Label15: TLabel
          Left = 8
          Top = 56
          Width = 46
          Height = 13
          Caption = 'Max ping:'
        end
        object Label16: TLabel
          Left = 8
          Top = 96
          Width = 73
          Height = 13
          Caption = 'Max frequency:'
        end
        object Label17: TLabel
          Left = 8
          Top = 136
          Width = 67
          Height = 13
          Caption = 'Max inactivity:'
        end
        object Label19: TLabel
          Left = 8
          Top = 191
          Width = 80
          Height = 13
          Caption = 'Admin password:'
        end
        object Label20: TLabel
          Left = 8
          Top = 244
          Width = 51
          Height = 13
          Caption = 'Voicechat:'
        end
        object Label21: TLabel
          Left = 160
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Messages:'
        end
        object Label29: TLabel
          Left = 160
          Top = 104
          Width = 37
          Height = 13
          Caption = 'Ban list:'
          Enabled = False
        end
        object Password: TEdit
          Left = 8
          Top = 24
          Width = 121
          Height = 21
          TabOrder = 0
        end
        object MaxPing: TEdit
          Left = 8
          Top = 72
          Width = 121
          Height = 21
          TabOrder = 1
          Text = '0'
        end
        object MaxFrequency: TEdit
          Left = 8
          Top = 112
          Width = 121
          Height = 21
          TabOrder = 2
          Text = '0'
        end
        object AdminPassword: TEdit
          Left = 8
          Top = 207
          Width = 121
          Height = 21
          TabOrder = 4
        end
        object VoiceChat: TComboBox
          Left = 8
          Top = 260
          Width = 121
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
          Items.Strings = (
            'Not used'
            'Codec 1.2 kbps'
            'Codec 3.2 kbps'
            'Codec 6.4 kbps'
            'Codec 8.0 kbps'
            'Codec 13.0 kbps'
            'Codec 32.0 kbps')
        end
        object MaxInactivity: TEdit
          Left = 8
          Top = 152
          Width = 121
          Height = 21
          TabOrder = 3
          Text = '300'
        end
        object Messages: TMemo
          Left = 160
          Top = 24
          Width = 289
          Height = 73
          TabOrder = 6
        end
        object RemoveNick: TButton
          Left = 362
          Top = 290
          Width = 88
          Height = 25
          Caption = 'Remove nick'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          OnClick = RemoveNickClick
        end
        object AddNick: TButton
          Left = 170
          Top = 290
          Width = 88
          Height = 25
          Caption = 'Add nick'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          OnClick = AddNickClick
        end
        object Banlist: TListView
          Left = 160
          Top = 120
          Width = 289
          Height = 161
          Columns = <
            item
              AutoSize = True
              Caption = 'Banned players'
            end>
          ColumnClick = False
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 7
          ViewStyle = vsReport
          OnDblClick = BanlistDblClick
          OnSelectItem = BanlistSelectItem
        end
        object EditNick: TButton
          Left = 266
          Top = 290
          Width = 88
          Height = 25
          Caption = 'Edit nick'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          OnClick = EditNickClick
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Maps'
        ImageIndex = 2
        object AddMap: TButton
          Left = 208
          Top = 120
          Width = 41
          Height = 25
          Caption = '>>>'
          TabOrder = 0
          OnClick = AddMapClick
        end
        object RemoveMap: TButton
          Left = 208
          Top = 152
          Width = 41
          Height = 25
          Caption = '<<<'
          TabOrder = 1
          OnClick = RemoveMapClick
        end
        object MoveMapUp: TButton
          Left = 296
          Top = 288
          Width = 75
          Height = 25
          Caption = 'Up'
          TabOrder = 2
          OnClick = MoveMapUpClick
        end
        object MoveMapDown: TButton
          Left = 376
          Top = 288
          Width = 75
          Height = 25
          Caption = 'Down'
          TabOrder = 3
          OnClick = MoveMapDownClick
        end
        object AviabileMaps: TListView
          Left = 8
          Top = 8
          Width = 193
          Height = 305
          Columns = <
            item
              AutoSize = True
              Caption = 'Aviabile maps'
            end>
          ColumnClick = False
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          SortType = stText
          TabOrder = 4
          ViewStyle = vsReport
          OnDblClick = AviabileMapsDblClick
          OnSelectItem = AviabileMapsSelectItem
        end
        object SelectedMaps: TListView
          Left = 256
          Top = 8
          Width = 193
          Height = 273
          Columns = <
            item
              AutoSize = True
              Caption = 'Selected maps'
            end>
          ColumnClick = False
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 5
          ViewStyle = vsReport
          OnDblClick = SelectedMapsDblClick
          OnSelectItem = SelectedMapsSelectItem
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Clan settings'
        ImageIndex = 3
        object Label24: TLabel
          Left = 8
          Top = 32
          Width = 42
          Height = 13
          Caption = 'Clan tag:'
        end
        object Label26: TLabel
          Left = 296
          Top = 32
          Width = 46
          Height = 13
          Caption = 'Clan side:'
        end
        object Allied: TSpeedButton
          Left = 296
          Top = 48
          Width = 73
          Height = 22
          GroupIndex = 1
          Down = True
          Caption = 'ALLIED'
        end
        object Axis: TSpeedButton
          Left = 376
          Top = 48
          Width = 73
          Height = 22
          GroupIndex = 1
          Caption = 'AXIS'
        end
        object Label22: TLabel
          Left = 8
          Top = 80
          Width = 200
          Height = 13
          Caption = 'Reserve connections for clan-side players:'
        end
        object ReserveClanSidePlayersTxt: TLabel
          Left = 400
          Top = 105
          Width = 49
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = '0'
        end
        object ClanTAG: TEdit
          Left = 8
          Top = 48
          Width = 281
          Height = 21
          TabOrder = 0
        end
        object EnableAutokick: TCheckBox
          Left = 8
          Top = 8
          Width = 97
          Height = 17
          Caption = 'Enable autokick'
          TabOrder = 1
        end
        object ReserveClanSidePlayers: TTrackBar
          Left = 6
          Top = 98
          Width = 387
          Height = 21
          Max = 32
          Orientation = trHorizontal
          Frequency = 1
          Position = 0
          SelEnd = 0
          SelStart = 0
          TabOrder = 2
          ThumbLength = 11
          TickMarks = tmTopLeft
          TickStyle = tsAuto
          OnChange = ReserveClanSidePlayersChange
        end
      end
    end
    object ApplySettings: TButton
      Left = 296
      Top = 360
      Width = 89
      Height = 25
      Caption = 'Apply'
      Default = True
      TabOrder = 1
      OnClick = ApplySettingsClick
    end
    object CancelSettings: TButton
      Left = 387
      Top = 360
      Width = 86
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      TabOrder = 2
      OnClick = CancelSettingsClick
    end
  end
end
