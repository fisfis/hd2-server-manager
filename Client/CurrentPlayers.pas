unit CurrentPlayers;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls;

type
  TCurrentPlayersForm = class(TForm)
    CurrentPlayers: TTreeView;
    procedure CurrentPlayersDblClick(Sender: TObject);
    procedure CurrentPlayersKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CurrentPlayersForm: TCurrentPlayersForm;

implementation

{$R *.DFM}

procedure TCurrentPlayersForm.CurrentPlayersDblClick(Sender: TObject);
begin
   if (CurrentPlayers.Selected<>nil) and (CurrentPlayers.Selected.GetPrev<>nil) then modalResult:=mrOk;
end;

procedure TCurrentPlayersForm.CurrentPlayersKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if Key=vk_escape then modalResult:=mrCancel;
end;

end.
