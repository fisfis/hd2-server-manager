unit SelectUser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TGrantUser = class(TForm)
    Panel1: TPanel;
    UserList: TListBox;
    Add: TButton;
    Cancel: TButton;
    procedure AddClick(Sender: TObject);
    procedure UserListDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GrantUser: TGrantUser;

implementation

{$R *.DFM}

procedure TGrantUser.AddClick(Sender: TObject);
begin
   if UserList.SelCount>0 then ModalResult:=mrOK;
end;

procedure TGrantUser.UserListDblClick(Sender: TObject);
begin
   AddClick (Sender);
end;

end.
