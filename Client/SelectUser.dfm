object GrantUser: TGrantUser
  Left = 240
  Top = 125
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Select user to grant acess'
  ClientHeight = 313
  ClientWidth = 248
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 241
    Height = 306
    TabOrder = 0
    object UserList: TListBox
      Left = 8
      Top = 8
      Width = 224
      Height = 257
      ItemHeight = 13
      MultiSelect = True
      Sorted = True
      TabOrder = 0
      OnDblClick = UserListDblClick
    end
    object Add: TButton
      Left = 77
      Top = 272
      Width = 75
      Height = 25
      Caption = 'Add'
      Default = True
      TabOrder = 1
      OnClick = AddClick
    end
    object Cancel: TButton
      Left = 157
      Top = 272
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
end
