object CurrentPlayersForm: TCurrentPlayersForm
  Left = 1699
  Top = 483
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Now are playing'
  ClientHeight = 375
  ClientWidth = 245
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object CurrentPlayers: TTreeView
    Left = 0
    Top = 0
    Width = 245
    Height = 375
    Align = alClient
    Indent = 19
    TabOrder = 0
    OnDblClick = CurrentPlayersDblClick
    OnKeyDown = CurrentPlayersKeyDown
    Items.Data = {
      020000001E0000000000000000000000FFFFFFFFFFFFFFFF0000000001000000
      056164617364220000000000000000000000FFFFFFFFFFFFFFFF000000000000
      0000096173646173646173641C0000000000000000000000FFFFFFFFFFFFFFFF
      000000000000000003617364}
  end
end
