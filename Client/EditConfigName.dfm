object ConfigurationName: TConfigurationName
  Left = 355
  Top = 251
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'ConfigurationName'
  ClientHeight = 90
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 361
    Height = 83
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 127
      Height = 13
      Caption = 'Server configuration name:'
    end
    object ConfigName: TEdit
      Left = 8
      Top = 24
      Width = 345
      Height = 21
      TabOrder = 0
    end
    object Ok: TButton
      Left = 200
      Top = 51
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 1
      OnClick = OkClick
    end
    object Cancel: TButton
      Left = 280
      Top = 51
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
end
