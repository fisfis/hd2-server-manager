program HD2ServerManagerCilent;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  ServerSettings in 'ServerSettings.pas' {ServerSettingsForm},
  NickToBan in 'NickToBan.pas' {BanUser},
  ServerUser in 'ServerUser.pas' {EditUser},
  ServerProps in 'ServerProps.pas' {ServerProperties},
  common in '..\common.pas',
  Stats in 'Stats.pas' {ServerStats},
  SelectUser in 'SelectUser.pas' {GrantUser},
  EditConfigName in 'EditConfigName.pas' {ConfigurationName},
  DSDataReader in '..\..\DS Server Data Reader\DSDataReader.pas',
  CurrentPlayers in 'CurrentPlayers.pas' {CurrentPlayersForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Hidden and dangerous remote server configurator';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TServerSettingsForm, ServerSettingsForm);
  Application.CreateForm(TBanUser, BanUser);
  Application.CreateForm(TEditUser, EditUser);
  Application.CreateForm(TServerProperties, ServerProperties);
  Application.CreateForm(TServerStats, ServerStats);
  Application.CreateForm(TGrantUser, GrantUser);
  Application.CreateForm(TConfigurationName, ConfigurationName);
  Application.CreateForm(TCurrentPlayersForm, CurrentPlayersForm);
  Application.Run;
end.
