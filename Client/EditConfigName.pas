unit EditConfigName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TConfigurationName = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ConfigName: TEdit;
    Ok: TButton;
    Cancel: TButton;
    procedure OkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfigurationName: TConfigurationName;

implementation

{$R *.DFM}

procedure TConfigurationName.OkClick(Sender: TObject);
begin
   if Trim (ConfigName.Text)<>'' then ModalResult:=mrOK;
end;

procedure TConfigurationName.FormShow(Sender: TObject);
begin
   ConfigName.SetFocus;
end;

end.
