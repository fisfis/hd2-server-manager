object EditUser: TEditUser
  Left = 471
  Top = 824
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'User properties'
  ClientHeight = 133
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 3
    Width = 329
    Height = 127
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 58
      Height = 13
      Caption = 'Login name:'
    end
    object Label2: TLabel
      Left = 168
      Top = 8
      Width = 49
      Height = 13
      Caption = 'Password:'
    end
    object Username: TEdit
      Left = 8
      Top = 24
      Width = 153
      Height = 21
      TabOrder = 0
    end
    object Password: TEdit
      Left = 168
      Top = 24
      Width = 153
      Height = 21
      TabOrder = 1
    end
    object Ok: TButton
      Left = 166
      Top = 94
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 3
      OnClick = OkClick
    end
    object Cancel: TButton
      Left = 246
      Top = 94
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 4
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 48
      Width = 313
      Height = 41
      Caption = ' User prmissions:'
      TabOrder = 2
      object Admin: TRadioButton
        Left = 48
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Administrator'
        TabOrder = 0
      end
      object Manager: TRadioButton
        Left = 168
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Server Manager'
        Checked = True
        TabOrder = 1
        TabStop = True
      end
    end
  end
end
