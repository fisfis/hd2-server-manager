unit common;

interface

uses
   Windows, Classes, DSDataReader;

const
   svcmd_Login           = 0;
   svcmd_GetConfig       = 1;
   svcmd_PutConfig       = 2;

   svcmd_ListUsers       = 3;
   svcmd_AddUser         = 4;
   svcmd_UpdateUser      = 5;
   svcmd_DeleteUser      = 6;

   svcmd_ListServers     = 7;
   svcmd_AddServer       = 8;
   svcmd_UpdateServer    = 9;
   svcmd_DeleteServer    = 10;

   svcmd_GetServerConfig = 11;
   svcmd_PutServerConfig = 12;

   svcmd_StartServer     = 13;
   svcmd_StopServer      = 14;
   svcmd_RestartServer   = 15;

   svcmd_GetStatistic    = 16;

   svcmd_PutServerStatus = 17;


   svcmd_ListServerPlayers = 20;

type
   TServerInfo = packed record
      ServerPort:            Word;
      WatchDogEnable:        boolean;
      WatchDogInterval:      Cardinal;
      MessagingEnable:       boolean;
      MessagingInterval:     Cardinal;
      RebootEnable:          boolean;
      RebootInterval:        Integer;
      ForcedMessagesEnable:  boolean;
      ForcedBanListEnable:   boolean;

      DedicatedServerPath:   string;
      ServerIP:              string;
      ForcedMessages:        array of string;
      ForcedBanList:         array of string;
   end;

   TUserInfo = packed record
      PrivilegeLevel:        byte;
      Username:              string;
      Password:              string;
   end;

   PServerConfig = ^TServerConfig;
   TServerConfig = packed record
      maxclients:            byte;
      pointlimit:            byte;
      roundlimit:            byte;
      roundcount:            byte;
      respawntime:           word;
      spawnprotection:       byte;
      warmup:                byte;
      inversedamage:         byte;
      friendlyfire:          boolean;
      autoteambalance:       boolean;
      _3rdpersonview:        boolean;
      allowcrosshair:        boolean;
      fallingdmg:            boolean;
      allowrespawn:          boolean;
      allowvehicles:         boolean;
      respawnnumber:         integer;
      teamrespawn:           boolean;
      maxping:               word;
      maxfreq:               word;
      maxinactivity:         word;
      voicechat:             byte;
      enableautokick:        boolean;
      clanside:              byte;
      clanreserve:           byte;
      name:                  string;
      domain:                string;
      style:                 string;
      sessionname:           string;
      dificulty:             string;
      password:              string;
      adminpass:             string;
      clantag:               string;
      maps:                  array of string;
      messages:              array of string;
      banlist:               array of string;
   end;

   PServer = ^TServer;
   TServer = packed record
      port:                  word;
      running:               boolean;
      watchdogenable:        boolean;
      messagesenable:        boolean;
      name:                  string;
      currentconfig:         string;
      grantedusers:          array of string;
      configurations:        array of TServerConfig;

      Script:                array of string;

      StartupInfo:           TStartupInfo;
      ProcessInfo:           TProcessInformation;
      WindowHandle:          HWND;

      Initializing:          boolean;
      ProcessStarting:       boolean;
      StartTries:            Byte;
      ProcessStarted:        boolean;
      StartTime:             Cardinal;
      PermanentError:        boolean;
      ConfigChanged:         boolean;
      ProcessDown:           boolean;

      InfoReceiveTime:       Cardinal;
      LastProcessedInfo:     Cardinal;

      CPUCheckTime:          Cardinal;
      CPULastUsage:          Cardinal;
      CPUUsageUpperTo80:     Cardinal;

      playerIPs:             array[0..31] of string;
      playerSlots:           array[0..31] of string;

      KickedPlayers:         TStringList;

      DataReader:            THD2DS;
   end;

   function BoolToStr (Data: boolean): string;

implementation

function BoolToStr (Data: boolean): string;
begin
   if Data then Result:='true' else Result:='false';
end;

end.
